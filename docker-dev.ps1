
#docker kill $(docker ps -a -f "names=doctrack" -f "status=running" -q)
#docker kill $(docker ps -a -f "names=doctrack" -f "status=paused" -q)
docker rm -f $(docker ps -a -f "names=doctrack" -q)

docker run `
  --name doctrack-db-dev `
  -d `
  -e "MYSQL_PASSWORD=password1" `
  -p "3306:3306" `
  durenworks/mysql-dev:0.1

sleep 2

docker run `
  --name doctrack-web-dev `
  --rm `
  -e "LARAVEL_DB_DRIVER=MYSQL" `
  -e "LARAVEL_DB_NAME=dev_doctrack" `
  -e "LARAVEL_DB_PASSWORD=password1" `
  -e "LARAVEL_MIGRATE=1" `
  -e "LARAVEL_MIGRATE_SEED=1" `
  -e "DOCKER_BEANSTALKD=1" `
  -p "80:80" `
  --link "doctrack-db-dev:db" `
  --hostname "trusty-server" `
  -v "/mnt/host/doctrack:/srv/web" `
  -w "/srv/web" `
  durenworks/laravel-dev:0.3
