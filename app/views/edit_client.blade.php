@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
	
@endsection
@section('content')
	<div class="content-title">
		<a href="{{{ action('ClientController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Klien {{{ $client->name }}}</span>
	</div>
	
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<form class="document-form" method="post" action="{{{ action('ClientController@updateRow', [$client->id]) }}}">

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
							Nama Klien
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="name" value="{{{ Input::old('name', $client->name) }}}" size="30">
						@if ($errors->has('name'))
							<br><span class="text-danger">{{{ $errors->first('name') }}}</span>
						@endif
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Alamat
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="address" value="{{{ Input::old('address', $client->address) }}}" size="30">
						@if ($errors->has('address'))
							<br><span class="text-danger">{{{ $errors->first('address') }}}</span>
						@endif
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Email
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="email" value="{{{ Input::old('email', $client->email) }}}" size="30">
						@if ($errors->has('email'))
							<br><span class="text-danger">{{{ $errors->first('email') }}}</span>
						@endif
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Telepon
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="phone" value="{{{ Input::old('phone', $client->phone) }}}" size="30">
						@if ($errors->has('phone'))
							<br><span class="text-danger">{{{ $errors->first('phone') }}}</span>
						@endif
					</div>
					</div>
				</div>
				<button type="Submit" class="btn btn-xs btn-warning">
					UBAH
					<span class="glyphicon glyphicon-edit"></span>
				</button>
			</form>
		</div>
	</div>
@endsection