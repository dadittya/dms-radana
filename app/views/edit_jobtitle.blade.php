@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
	
@endsection
@section('content')
	<div class="content-title">
		<a href="{{{ action('JobTitleController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Jabatan {{{ $jobtitle->name }}}</span>
	</div>
	
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<form class="document-form" method="post" action="{{{ action('JobTitleController@updateRow',[$jobtitle->id]) }}}">

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
							Nama Jabatan
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="name" value="{{{ Input::old('name', $jobtitle->name) }}}" size="30">
						@if ($errors->has('name'))
							<br><span class="text-danger">{{{ $errors->first('name') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
							Hak Akses
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('level', $job_level, Input::old('level', $jobtitle->level), array(
							$jobtitle->system_jobtitle == 1 ? 'disabled' : ''
						)) }}
						@if ($errors->has('level'))
							<br><span class="text-danger">{{{ $errors->first('level') }}}</span>
						@endif
					</div>
					</div>
				</div>

				
				
				<button type="Submit" class="btn btn-xs btn-warning">
					UBAH
					<span class="glyphicon glyphicon-edit"></span>
				</button>
			</form>
		</div>
	</div>
@endsection