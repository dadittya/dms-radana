@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title document-form">
		APT Search
	</div>
	<form action="{{{ action('DocumentController@showCSList') }}}" method="get" id="form-search">
		<div id="content-search" class="content-search content-search-no-margin">
			<div class="document-form content-advanced content-advanced-no-border content-search-no-margin">
				<div class="left-right">
					<div class="document-point-left">Perihal Surat</div>
					<div class="document-point-right">
						<input type="text" size="30" name="about" value="{{{ $about }}}">
					</div>
				</div>
				<div class="left-right">
					<div class="document-point-left">No Agenda</div>
					<div class="document-point-right">
						<input type="text" size="30" name="no_agenda" value="{{{ $no_agenda }}}">
					</div>
				</div>
				<div class="left-right">
					<div class="document-point-left">No Surat</div>
					<div class="document-point-right">
						<input type="text" size="30" name="no_surat" value="{{{ $no_surat }}}">
					</div>
				</div>
				<div class="left-right">
					<div class="document-point-left">Pemohon</div>
					<div class="document-point-right">
						<input type="text" size="30" name="pemohon" value="{{{ $pemohon }}}">
					</div>
				</div>
				<div class="left-right">
					<div class="document-point-left">SubBagian/Seksi</div>
					<div class="document-point-right">
						{{ Form::select('assigned_dept', $deptlist, $assigned_dept, array(
							'id' => 'dept'
						) ) }}
					</div>
				</div>
				<div class="button">
					<button type="submit" class="btn btn-xs btn-warning">Terapkan</button>
				</div>
			</div>
		</div>
	</form>
	<div class="content-column">
		<table class="table table-notif hover">
			<thead>
				<tr>
					<th>Sifat</th>
					<th>Klasifikasi</th>
					<th>No Agenda</th>
					<th>No Surat</th>
					<th>Hal</th>
					<th>Dari</th>
					<th>Tgl Surat</th>
					<th>Status</th>
					<th style="text-align: right">Tgl Selesai</th>
				</tr>
			</thead>
			@foreach ($documents as $document)
				<tbody class="content_cs">
					<tr>
						<td>
							@if ($document->urgency == 'KLT')
								Kilat
							@elseif ($document->urgency == 'SSG')
								Sangat Segera
							@elseif ($document->urgency == 'SGR')
								Segera
							@else
								Biasa
							@endif
						</td>
						<td>
							@if ($document->clasification == 'SRS')
								Sangat Rahasia
							@elseif ($document->clasification == 'RHS')
								Rahasia
							@elseif ($document->clasification == 'KFD')
								Konfidential
							@else
								Biasa
							@endif
						</td>
						<td>{{{ $document->agenda_number }}}</td>
						<td>{{{ $document->document_number }}}</td>
						<td>{{{ $document->about }}}</td>
						<td>{{{ $document->fromClient ? $document->fromClient->name : $document->received_from }}}</td>
						<td style="text-align:right; min-width: 130px">
							{{{ $document->received_at ? $document->document_date->format('d M Y') : '-' }}}
						</td>
						<td style="min-width: 150px">
							{{{ $document->getStatus() }}}
						</td>
						<td style="text-align:right; min-width: 130px">
							@if ($document->getStatusCode() == 'FNS')
								<?php $date = $document->getFinishDate() ?>
								{{{ $date ? $date->format('d M Y') : '-' }}}
							@endif
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<ul class="comma-list">
								Disposisi kepada :
								@if (count($document->docDisposition))
									@foreach ($document->docDisposition as $dept)
										<li>{{{ $dept->name }}}</li>
									@endforeach
								@else
									-
								@endif
							</ul>
						</td>
						<td colspan="4" style="text-align: right">
							Perubahan terakhir : {{{ $document->last_action()->created_at->format('d F Y') }}}
								oleh {{{ $document->last_action()->employee->name }}} {{{ $document->last_action()->employee->dept ? '(' . $document->last_action()->employee->dept->name . ')' : '' }}}
						</td>
					</tr>
				</tbody>
			@endforeach
			@if(empty($documents))
				<tbody>
					<tr>
						<td colspan="9">Hasil Pencarian Kosong
						</td>
					</tr>
				</tbody>
			@endif
		</table>
	</div>
	<div style="margin: 0 30px; text-align: right">
	@if($documents)
		{{ $documents->appends(array(
			'about' => $about,
			'no_agenda' => $no_agenda,
			'no_surat' => $no_surat,
			'assigned_dept' => $assigned_dept,
		))->links() }}
	@endif
	</div>

@endsection

@section('content.js')
@endsection
