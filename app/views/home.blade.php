@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title">Selamat Datang, {{{ Auth::user()->name }}}</div>
	<div class="content-notif">
		@include('top_notif')
	</div>
	<div class="content-column">
		<table class="table table-hover table-notif separate">
			<thead>
				<th>&nbsp;</th>
				<th>No Agenda</th>
				<th>No Surat</th>
				<th>Hal</th>
				<th>Dari</th>
				<th>Perubahan Terakhir</th>
				<th>Tgl Selesai</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				@foreach ($documents as $document)
					<tr onclick="document.location = '{{{ action('DocumentController@showDetail', array($document->id)) }}}'" style="background-color: {{{ $document->defineBgColor() }}}">
						<td width="65px">
							@if ($document->calcPercentageDeadline() <= 25)
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
							@elseif ($document->calcPercentageDeadline() <= 30)
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
							@elseif ($document->calcPercentageDeadline() <= 50)
								<span class="glyphicon glyphicon-fire"></span>
							@endif
						</td>
						<td>{{{ $document->agenda_number }}}</td>
						<td>{{{ $document->document_number }}}</td>
						<td>
							{{{ $document->about }}} <br>
							<span class="label label-default">
								@if ($document->is_copy)
									TEMBUSAN
								@else
									ASLI
								@endif
							</span>
						</td>
						<td>{{{ $document->received_from_id ? $document->fromClient->name : $document->received_from }}}</td>
						<td style="min-width: 115px">
							{{{ $document->last_action()->created_at->format('d/m/Y H:i') }}} <br>
							oleh {{{ $document->last_action()->employee->name }}}
						</td>
						<td style="min-width: 180px">
							@if ($document->target_date)
								{{{ $document->target_date->format('d/m/Y')}}} 
								<span class="badge" style="font-size: 11px">
									{{{ $document->calcPercentageDeadline() }}}%
								</span>
								<span class="badge" style="font-size: 11px">
									@if ($document->target_date < $now)
										0
									@else
										{{{ $document->target_date->diffInDays($now) + 1 }}}
									@endif
									hari
								</span>
							@else
								-
							@endif
						</td>
						<td style="min-width: 125px">
							@if ($document->is_product == 1)
								<a href="{{{ action('DocumentController@showList', array('is_product' => 'yes')) }}}">
									<span class="label label-success">PRODUK</span>
								</a>
							@else
								<form action="{{{ action('DocumentController@saveUserTrack', array($document->id)) }}}" method="post">
									<a href="{{{ action('DocumentController@showList', array('is_product' => 'no')) }}}">
										<span class="label label-primary">BUKAN PRODUK</span>
									</a>
									<!--button usertrack
										<button type="submit" class="btn btn-xs btn-warning pull-right">OK</button>
									-->
								</form>
							@endif
							@if ($document->target_date)
								<a href="{{{ action('DocumentController@showList', array('target' => '1')) }}}">
									<span class="label label-danger">TARGET</span>
								</a>
							@endif
						</td>
					</tr>
					<tr onclick="document.location = '{{{ action('DocumentController@showDetail', array($document->id)) }}}'">
						<td colspan="8" style="background-color: {{{ $document->defineBgColor() }}}; border-top: none">
							File terlampir :
							<?php $count = 0?>
							@foreach ($document->action as $action)
								@foreach ($action->file as $file)
									<div style="padding-left: 20px">
										<a href="{{{ action('DocumentController@downloadFile', array($file->id)) }}}">
											<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
											{{{ $file->originalname }}} 
										</a>
											@if ($file->type == 'INB')
												(Surat Masuk)
											@elseif ($file->type == 'PRO')
												(Produk)
											@elseif ($file->type == 'OTH')
												(Lain-Lain)
											@else
												({{{ $file->type }}})
											@endif
									</div>
									<?php $count += 1?>
								@endforeach
							@endforeach
							@if ($count <= 0)
								-
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection