@extends('master_layout')

@section('content')
	<div class="content-title">Daftar SubBagian/Seksi</div>
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<table class="table table-hover">
				<tr>
					<th>Nama SubBagian/Seksi</th>
					<th>
						<span class="pull-right"> 
							[Hapus]
						</span>
					</th>
				</tr>
				@foreach($deps as $dep)
					<tr>
						<td>
							<a href="{{{ action('DepartmentController@showEditForm', [$dep->id]) }}}" title="Ubah" style="color: #333">
								<span class="btn btn-default btn-xs glyphicon glyphicon-pencil"></span>
								<span>{{{ $dep->name }}}</span>
							</a>
						</td>
						<td>
							<form action="{{{ action('DepartmentController@deleteRow', [$dep->id]) }}}" method="post">
								<button class="btn btn-danger btn-xs pull-right" type="submit" title="Hapus">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
							</form>
						</td>
					</tr>
				@endforeach
			</table>
			<div style="text-align: right">
				{{ $deps->links() }}
			</div>
			<a href="{{{ action('DepartmentController@showNewForm') }}}" title="Tambah">
				<button class="btn btn-warning btn-sm">Tambah SubBagian/Seksi Baru</button>
			</a>
		</div>
	</div>
@endsection