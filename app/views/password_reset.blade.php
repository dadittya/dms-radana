<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIMOS</title>

    <link rel="icon" href="/static/img/logo-mikro.png" type="image/png" />
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/signin.css" rel="stylesheet">
    <link href="/static/css/dw.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
      <div style="text-align: center">
          <img src="/static/img/logo.png" height="80px">
          <h4> SIMOS</h4>
          <h5> KANTOR PELAYANAN KEKAYAAN NEGARA DAN LELANG Samarinda </h5>
      </div>
      <form class="form-signin" role="form" action="{{{ action('RemindersController@postReset') }}}" method="post">
        <h2 class="form-signin-heading">Reset Password</h2>
        @if (Session::has('error'))
          <div class="alert alert-danger">{{{ Session::get('error') }}}</div>
        @endif
        @if (Session::has('status'))
          <div class="alert alert-success">{{{ Session::get('status') }}}</div>
        @endif
        <input type="hidden" name="token" value="{{ $token }}">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus><br>
        <label for="inputPassword" class="sr-only">Password Baru</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password Baru" required>
        <label for="inputPasswordConfirmation" class="sr-only">Password Konfirmasi</label>
        <input type="password" name="password_confirmation" id="inputPasswordConfirmation" class="form-control" placeholder="Password Konfirmasi" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
      </form>

    </div> <!-- /container -->

  </body>
</html>
