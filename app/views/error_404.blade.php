@extends('master_layout')

@section('content')
	<div class="content-title">Page Not Found</div>
				
	<div class="detail-document">
		<div class="detail-document-form">
			Halaman tidak ditemukan.
		</div>
	</div>
@endsection