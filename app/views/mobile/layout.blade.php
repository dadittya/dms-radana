<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/static/css/bootstrap.css" rel="stylesheet">
		<link href="/static/css/dw.css" rel="stylesheet">
		<style>
			body {
				min-width: inherit;
			}
			.alert {
				margin-top: 20px;
			}
			.notification-button .btn-xs {
				display: block;
				width: 100%;
				min-height: 50px;
				margin-bottom: 20px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			@yield('content')
		</div>
	</body>
</html>