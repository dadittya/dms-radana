@extends('mobile.layout')

@section('content')
	@foreach ($errors->all() as $e)
		<div class="alert alert-danger">
			{{{ $e }}}
		</div>
	@endforeach
	<div class="notification" style="margin-top: 20px">
		<div class="notification-content">
			Perubahan status gagal dilakukan, mohon coba kembali.
		</div>
	</div>
@endsection
