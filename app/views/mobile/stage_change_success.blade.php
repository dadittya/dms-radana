@extends('mobile.layout')

@section('content')
	<div class="notification" style="margin-top: 20px">
		<div class="notification-content">
			Perubahan status berhasil.
		</div>
	</div>
	<script>
		app.reloadDocument();
	</script>
@endsection
