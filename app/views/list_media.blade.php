@extends('master_layout')

@section('content')
	<div class="content-title">Upload Media</div>
	<div class="detail-document">
		<form class="document-form" method="post" action="{{{ action('MediaController@createNew') }}}" enctype="multipart/form-data">
			<div class="detail-document-form">
			@include('top_notif')
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<span class="control-label">
								Gambar
						</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="document-point-right">
							<input type="file" name="media">
							@if ($errors->has('media'))
								<br><span class="text-danger">{{{ $errors->first('media') }}}</span>
							@endif
						</div>
					</div>
				</div>

				<button type="Submit" class="btn btn-xs btn-warning">
					SIMPAN
					<span class="glyphicon glyphicon-ok"></span>
				</button>
			</div>
		</form>
	</div>
	<div class="content-title">Daftar Media</div>
	<div class="detail-document">
		@if ($media->count() > 0)
		<div class="row">
			<div class="panel panel-default" style="margin-top: 10px">
	        <div class="panel-heading"></div>
			<div class="panel-body" style="padding-bottom: 0">
                 @foreach($media as $key => $med)
                    <div class="form-group col-md-4" style="{{ $key % 3 == 0 ? 'clear: both;' : '' }}" style="padding-bottom: 20px">
                        <div style="border: 1px solid #E9E9E9;">
                            <span class="img-main">
                                <img src="{{ $med->getMediaFullPath() }}" width="100%">
                            </span>
                        </div>
	                    <center>
	                    	<a href="#" data-toggle="modal" data-target="#mediaModal{{ $med->id }}">
	                    		<span style="word-wrap: break-word">{{ $med->name }}</span>
	                    	</a>
	                    </center>
                    </div>
                @endforeach
	        </div>
	        <div class="panel-heading" style="padding-top: 12px"></div>
		</div>
		@endif
	</div>
@endsection

@section('content.modal')
	@foreach ($media as $med)
		<div class="modal fade" id="mediaModal{{ $med->id }}" role="dialog">
		    <div class="modal-dialog modal-sm">
		      	<div class="modal-content">
			        <div class="modal-header">
						<form method="post" action="{{{ action('MediaController@deleteRow', $med->id) }}}" class="document-form" role="form">
				          	<h4 class="modal-title">
				          		Edit Media
								<button type="submit" class="btn btn-danger pull-right">Delete</button>
							</h4>
						</form>
			        </div>
					<form method="post" action="{{{ action('MediaController@updateRow', $med->id) }}}" class="form-inline" role="form">
			        <div class="modal-body container-fluid">
			        	<div class="form-group">
			        		<div class="col-md-12 col-sm-12 col-xs-12">
								<label for="name">Name</label>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="document-point-right">
									<input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{{ $med->name }}}">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<label for="name">File Name</label>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="document-point-right">
									<input type="text" name="filename" class="form-control" readonly="true" id="filename" placeholder="File Name" value="{{{ $med->filename }}}">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<label for="name">File Url</label>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="document-point-right">
									<input type="text" name="url" class="form-control" readonly="true" id="url" placeholder="File Url" value="{{{ $med->getMediaFullPath() }}}">
								</div>
							</div>
						</div>
					</div>
			        <div class="modal-footer">
			        	<button type="submit" class="btn btn-success">Save</button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
			        </form>
			    </div>
		    </div>
		</div>
	@endforeach
@endsection