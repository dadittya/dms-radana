<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIMOS</title>

    <link rel="icon" href="/static/img/radana_icon.png" type="image/png" />
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/dw.css" rel="stylesheet">
</head>
<body style="background-color: transparent; min-width: 0px">
	<div class="container-disp" id="container">
		<table class="table content-disp" id="table-top">
			<tr class="to-center">
				<td class="to-vmiddle" style="width: 190px;">
					<img src="/static/img/radana_icon_2.png" style="margin-left: -20px">
				</td>
				<td colspan="3">
					<div style="margin-left: -70px">
						<h4 class="no-margin"><b>
							PT Radana Bhaskara Finance Tbk
						</b></h4>
						<span style="font-size: 10pt"><b>
							CIBIS Nine Building 11th Floor Suite W-16<br>
							Jl. TB Simatupang No. 2 RT.001/RW.005 Kel Cilandak Timur,<br>
							Kec Pasar Minggu Jakarta 12560<br>
							Telp. 021 50991088 Fax. 021 50991089
						</b></span>
					</div>
				</td>
			</tr>
			<tr class="to-center">
				<td colspan="4">
					<b>LEMBAR DISPOSISI</b>
				</td>
			</tr>
			<tr class="to-center">
				<td colspan="4">
					<b>PERHATIAN : Dilarang memisahkan sehelai surat pun yang tergabung dalam berkas ini</b>
				</td>
			</tr>
			<tr>
				<td>
					No. Agenda
					<span class="pull-right">:</span>
				</td>
				<td>{{{ $document->agenda_number }}}</td>
				<td rowspan="2" class="merge to-vmiddle" width="25%">No. KK : </td>
				<td rowspan="2" class="merge to-vmiddle" width="200px">
					<span class="disp-icon to-vmiddle" style="width: 80px">
						@if ($document->is_copy == 1)
							<img src="/static/img/checkbox_unchecked.ico"> 
						@else 
							<img src="/static/img/checkbox_checked.ico"> 
						@endif
						Asli
					</span>
					<span class="disp-icon to-vmiddle">
						@if ($document->is_copy == 0)
							<img src="/static/img/checkbox_unchecked.ico"> 
						@else 
							<img src="/static/img/checkbox_checked.ico"> 
						@endif
						Tembusan
					</span>
				</td>
			</tr>
			<tr>
				<td>
					Tanggal Penerimaan Surat
					<span class="pull-right">:</span>
				</td>
				<td>{{{ $document->received_at->format('d/m/Y') }}}</td>
			</tr>
			<tr>
				<td>
					No./Tgl Surat
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">{{{ $document->document_number }}} / {{{ $document->document_date->format('d/m/Y') }}}</td>
			</tr>
			<tr class="group">
				<td>
					Dari
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">{{{ $document->fromClient ? $document->fromClient->name : $document->received_from }}}</td>
			</tr>
			<tr class="group">
				<td>
					Hal
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">{{{ $document->about }}}</td>
			</tr>
			<tr class="group">
				<td>
					Lampiran
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">{{{ $document->enclosure }}}</td>
			</tr>
			<tr class="group">
				<td>
					Sifat
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->urgency == "KLT")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Kilat
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->urgency == "SSG")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Sangat Segera
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->urgency == "SGR")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Segera
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->urgency == "BIA")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Biasa
					</span>
				</td>
			</tr>
			<tr class="group">
				<td>
					Klasifikasi
					<span class="pull-right">:</span>
				</td>
				<td colspan="3">
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->clasification == "SRS")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Sangat Rahasia
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->clasification == "RHS")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Rahasia
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->clasification == "KFD")
							<img src="/static/img/checkbox_checked.ico"> 
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Konfidensial
					</span>
					<span class="disp-icon to-vmiddle" style="width: 120px">
						@if ($document->clasification == "BIA")
							<img src="/static/img/checkbox_checked.ico">
						@else 
							<img src="/static/img/checkbox_unchecked.ico"> 
						@endif
						Biasa
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<b><u> DISPOSISI KEPALA KANTOR KEPADA :</u></b> <br>
					@foreach ($departments as $key => $department)
						<?php $match = 0; $row = $key%3; $col = ($key-$row)/3;?>
						@foreach ($document->docDisposition as $disposition)
							@if ($department->id == $disposition->id)
								<span class="disp-icon" style="width: 200px">
									<img src="/static/img/checkbox_checked.ico">
									{{{ $department->name }}}
								</span>
								<?php $match = 1; break;?>
							@endif
						@endforeach

						@if ($match == 0)
							<span class="disp-icon" style="width: 210px">
								<img src="/static/img/checkbox_unchecked.ico">
								{{{ $department->name }}}
							</span>
						@endif
					@endforeach
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<b><u> PETUNJUK </u></b> <br>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_0" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_0">Setuju</span>
                    </label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_1" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_1">Untuk perhatian</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_2" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_2">Perbaiki</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_3" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_3">Simpan</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_4" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_4">Tolak</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_5" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_5">Edarkan</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_6" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_6">Bicarakan dengan Saya</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_7" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_7">Sesuai Catatan</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_8" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_8">Teliti dan Pendapat</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_9" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_9">Jawab</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_10" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_10">Ingatkan</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_11" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_11">Perbanyak</span>
					</label>
					<label class="disp-icon" style="width: 175px">
						<img id="icon_12" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_12">Untuk diketahui</span>
					</label>
					<label class="disp-icon" style="width: 175px; margin-bottom: 10px">
						<img id="icon_13" src="/static/img/checkbox_unchecked.ico">
						<span id="directive_13">Selesaikan</span>
					</label>
				</td>
			</tr>
			<tr class="group">
				<td colspan="4">
					<b><u> CATATAN KEPALA KANTOR : </u></b> <br>
					<div style="min-height: 185px" id="disposition">
						@foreach ($document->docDisposition as $i)
							@if ($i->pivot->notes)
								<b>{{{ $i->name }}}</b><br>
								{{{ $i->pivot->notes }}}<br>
							@endif
						@endforeach
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					Tgl. Penyelesaian  :
				</td>
				<td colspan="2" class="merge">
					Diajukan kembali tgl  :
				</td>
			</tr>
			<tr>
				<td colspan="2">
					Penerima  :
				</td>
				<td colspan="2" class="merge">
					Penerima  :
				</td>
			</tr>
		</table>
		<div id="bottom">
			<table class="table content-disp-bottom" id="table-bottom">
				<tr>
					<td colspan="2" class="first" id="td-first">
						<b><u> DISPOSISI KASUBBAG/KASI :</u></b> <br>
						<div>
							SubBagian/Seksi : {{{ $document->assigned_dep ? $document->assignedDept->name : '-' }}} <br>
							Pelaksana Tugas : {{{ $document->assigned_emp ? $document->assigned->name : '-' }}} <br>
							@if ($document->substitute_emp)
								SubBagian/Seksi Pengganti : {{{ $document->substitute->department ? $document->substitute->dept->name : '-' }}} <br>
								Petugas Pengganti : {{{ $document->substitute->name }}} <br>							
							@endif
						</div>
					</td>
				</tr>
				<tr>
					<td>
						Tgl. Penyelesaian  :
					</td>
					<td>
						Diajukan kembali tgl  :
					</td>
				</tr>
				<tr>
					<td>
						Penerima  :
					</td>
					<td>
						Penerima  :
					</td>
				</tr>
			</table>
		</div>
	</div>

<!-- script js -->
<script src="/static/js/jquery.min.js"></script>
<script>
	var directive = {{ json_encode($directive) }};
	var height = $('#container').height();

	if (height > 1080) {
		$('#table-top').attr('style', 'page-break-after: always');
		$('#disposition').attr('style', 'min-height: 400px');
		$('#td-first').removeClass('first');
		$('#table-bottom').addClass('break');
		$('#bottom').attr('style', 'padding-top: 5mm');
	}

	$(function () {
		//checked directive
		for (var i = 0 in directive) {
			for (var x = 0; x < 14; x++)
				if ($('#directive_' + x).html() == directive[i].directive) {
					$('#icon_' + x).attr('src', '/static/img/checkbox_checked.ico');
					break;
				}
		}
	});
	window.print();
</script>
</body>
</html>