@extends('master_layout')

@section('content')
	<div class="content-title">
		<span>Detail Surat {{{ $document->document_number }}}</span>
	</div>
	
	<div class="content-column">
		<ul>
			<li class="border-right">{{{ $document->document_number }}}</li>
			<li class="border-right">{{{ $document->fromClient ? $document->fromClient->name : $document->received_from  }}}</li>
			<li class="border-right">{{{ $document->received_at->format('d F Y') }}}</li>
		</ul>
	</div>
	
	<div class="detail-content">
		<div class="col-xs-6 left form-horizontal">		
			<div class="form-group no-margin">		
				<label class="col-xs-4 bold-head">Nomor Surat</label>
				<label class="col-xs-8">{{{ $document->document_number }}}</label>
			</div>

			<div class="form-group no-margin">	
				<label class="col-xs-4 bold-head">Tanggal Surat</label>
				<label class="col-xs-8">{{{ $document->document_date->format('d F Y') }}}</label>
			</div>

			<div class="form-group no-margin">	
				<label class="col-xs-4 bold-head">Dari</label>
				<label class="col-xs-8">{{{ $document->fromClient ? $document->fromClient->name : $document->received_from }}}</label>
			</div>

			<div class="form-group no-margin">	
				<label class="col-xs-4 bold-head">Hal</label>
				<label class="col-xs-8">{{{ $document->about }}}</label>
			</div>
		</div>
		
		<div class="col-xs-6 form-horizontal">
			<div class="form-group no-margin">
				<label class="col-xs-4 bold-head">Tanggal Selesai</label>
				<label class="col-xs-8">{{{ $document->target_date ? $document->target_date->format('d F Y') : '-' }}}</label>
			</div>

			<div class="form-group no-margin">
				<label class="col-xs-4 bold-head">SubBagian/Seksi</label>
				<label class="col-xs-8">{{{ ($document->assignedDept ? $document->assignedDept->name : '') }}}</label>
			</div>
		</div>
	</div>	

	<div class="clearfix"></div>
	
	<div class="disposition full">
		<div class="disposition-divider"></div>
		<div class="action disposition-detail add-left-margin">
			<div class="disposition-info">
				<ul>
					<li class="border-right">KPKNL Samarinda</li>
				</ul>
			</div>
			
			<div class="disposition-content">
				Dokumen Telah Selesai Dikerjakan
			</div>

			<div class="disposition-attachment">
				<div class="attachment-file">
				@foreach ($document->action as $action)	
					@foreach ($action->file as $file)
						@if ($file->type != 'PRO')
							<?php continue; ?>
						@endif
						<div class="attachment-row">
							<a href="{{{ action('DocumentController@downloadPublicFile', [$document->id, $file->id, $file->getPublicKey()]) }}}">
								<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
								{{{ $file->originalname }}} 
							</a>
						</div>
					@endforeach
				@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
