@extends('master_layout')

@section('content')
	<div class="content-title">Daftar Jabatan</div>
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<table class="table table-hover">
				<tr>
					<th>Nama Jabatan</th>
					<th>Hak Akses</th>
					<th>
						<span class="pull-right"> 
							[Hapus]
						</span>
					</th>
				</tr>
				@foreach($jobtitles as $jobtitle)
					<tr>
						<td>
							<a href="{{{ action('JobTitleController@showEditForm', [$jobtitle->id]) }}}" title="Ubah" style="color: #333">
								<span class="btn btn-default btn-xs glyphicon glyphicon-pencil"></span>
								<span>{{{ $jobtitle->name }}}</span>
							</a>
						</td>
						<td>
							{{{ $jobtitle->jobLevel->name }}}
						</td>
						<td>
							@if ($jobtitle->system_jobtitle == 0)
								<form action="{{{ action('JobTitleController@deleteRow', [$jobtitle->id]) }}}" method="post">
									<button class="btn btn-danger btn-xs pull-right" type="submit" title="Hapus">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							@endif
						</td>
					</tr>
				@endforeach
			</table>

			<div style="text-align: right">
				{{ $jobtitles->links() }}
			</div>

			<a href="{{{ action('JobTitleController@showNewForm') }}}" title="Tambah">
				<button class="btn btn-warning btn-sm">Tambah Jabatan Baru</button>
			</a>
		</div>
	</div>
@endsection