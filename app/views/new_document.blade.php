@extends('master_layout')

@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:80%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:80%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
	
@endsection

@section('content')
	<div class="content-title">
		@if (Request::is('document/new'))
			Surat Baru
		@else
			Arsip Surat Baru
		@endif
	</div>
				
	<div class="detail-document">
		<form class="document-form" method="post" action="{{{ $form_url }}}">
			<div class="detail-document-form">
			@include('top_notif')
				
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
							Nomor Agenda
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="agenda_number" value="{{{ Input::old('agenda_number') }}}" size="30" class="form-mobile">
						<span class="control-label">
							/
						</span>
						<input type="text" name="agenda_year" value="{{ $year }}" class="form-mobile" size="6">
						@if ($errors->has('agenda_number'))
							<br><span class="text-danger">{{{ $errors->first('agenda_number') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Tanggal Masuk
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="received_at" value="{{{ Input::old('received_at') }}}" class="input-date">
						<span class="glyphicon glyphicon-calendar"></span>
						@if ($errors->has('received_at'))
							<br><span class="text-danger">{{{ $errors->first('received_at') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Referensi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" id="inputDocument" size="30" readonly>
						<input type="hidden" name="referenced_doc" id="sentDocument" value="{{{ Input::old('referenced_doc') }}}">
						<button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#reference">cari</button>
					</div>
					</div>
				</div>

				<div class="left-right">
					<div class="col-md-2 col-sm-2 col-xs-12">
						{{
							Form::radio('is_copy', '0', true, array(
								'class' => 'no-margin',
								'style' => 'position: absolute'
						)) }}
						<label>Asli</label> 
					</div>
					<div class="document-point-right">
						{{
							Form::radio('is_copy', '1', Input::old('is_copy'), array(
								'class' => 'no-margin',
								'style' => 'position: absolute'
						)) }}
						<label>Tembusan</label> 
					</div>
				</div>

				<div class="line"></div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Nomor Surat
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="document_number" value="{{{ Input::old('document_number') }}}" size="30">
						@if ($errors->has('document_number'))
							<br><span class="text-danger">{{{ $errors->first('document_number') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Tanggal Surat
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="document_date" class="input-date" value="{{{ Input::old('document_date') }}}">
						<span class="glyphicon glyphicon-calendar"></span>	
						@if ($errors->has('document_date'))
							<br><span class="text-danger">{{{ $errors->first('document_date') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Dari
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('received_from_id', $clientlist, Input::old('received_from_id'), array(
			        		'id' => 'fromClient_id'
			        	)) }}
			            <input type="text" id="fromClient" name="received_from" class="hidden" value="{{{ Input::old('received_from') }}}" size="30">
			            <button type="button" class="btn btn-xs btn-success" id="RefreshFromListButton">
							<span class="glyphicon glyphicon-refresh"></span>
						</button>
			            @if ($errors->has('received_from'))
							<br><span class="text-danger">{{{ $errors->first('received_from') }}}</span>
						@endif
					</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Kepada
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<table class="table table-condensed custom-table full-width" id="addressedToTable"></table>
						{{ Form::select('addressed_to_id', $clientlist, '', array(
				        		'id' => 'selectClient'
				        )) }}
				        <input type="text" size="30" id="inputClient">
						<button type="button" class="btn btn-xs btn-success" id="AddClientButton" onclick="add_client()">
							<span class="glyphicon glyphicon-plus"></span>
						</button>
						<input type="hidden" id="sentClient" name="clients">
						@if ($errors->has('clients'))
							<br><span class="text-danger">{{{ $errors->first('clients') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Hal
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<textarea name="about">{{{ Input::old('about') }}}</textarea>
						@if ($errors->has('about'))
							<br><span class="text-danger">{{{ $errors->first('about') }}}</span>
						@endif
					</div>
					</div>
				</div>
			    									
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Lampiran
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<textarea name="enclosure">{{{ Input::old('enclosure') }}}</textarea>
						@if ($errors->has('enclosure'))
							<br><span class="text-danger">{{{ $errors->first('enclosure') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Sifat
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('urgency', array(
							'KLT' => 'Kilat',
							'SSG' => 'Sangat Segera',
							'SGR' => 'Segera',
							'BIA' => 'Biasa'
						), Input::old('urgency') ) }}
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Klasifikasi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('clasification', array(
							'SRS' => 'Sangat Rahasia',
							'RHS' => 'Rahasia',
							'KFD' => 'Konfidensial',
							'BIA' => 'Biasa'
						), Input::old('clasification') ) }}
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Tanggal Selesai
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="target_date" class="input-date" value="{{{ Input::old('target_date') }}}">
						<span class="glyphicon glyphicon-calendar"></span>	
						@if ($errors->has('target_date'))
							<br><span class="text-danger">{{{ $errors->first('target_date') }}}</span>
						@endif
					</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Produk
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('is_product', array(
							null => '',
							'1' => 'Ya',
							'0' => 'Tidak',
						), Input::old('is_product') ) }}
						@if ($errors->has('is_product'))
							<br><span class="text-danger">{{{ $errors->first('is_product') }}}</span>
						@endif
					</div>
					</div>
				</div>
					
				<div class="line"></div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						SubBagian/Seksi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('assigned_dep', $deptlist, Input::old('assigned_dep') ? Input::old('assigned_dep') : '0', array(
							'id' => 'dept'
						) ) }}
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Petunjuk
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right checkbox no-margin">
						<label>{{ Form::checkbox('directive[0]', 'Setuju', Input::old('directive.0')) }} Setuju</label>
						<label>{{ Form::checkbox('directive[1]', 'Tolak', Input::old('directive.1')) }} Tolak</label>
						<label>{{ Form::checkbox('directive[2]', 'Teliti dan Pendapat', Input::old('directive.2')) }} Teliti &amp; Pendapat</label>
						<label>{{ Form::checkbox('directive[3]', 'Untuk diketahui', Input::old('directive.3')) }} Untuk diketahui</label>
						<label>{{ Form::checkbox('directive[4]', 'Untuk perhatian', Input::old('directive.4')) }} Untuk perhatian</label>
						<label>{{ Form::checkbox('directive[5]', 'Edarkan', Input::old('directive.5')) }} Edarkan</label>
						<label>{{ Form::checkbox('directive[6]', 'Jawab', Input::old('directive.6')) }} Jawab</label>
						<label>{{ Form::checkbox('directive[7]', 'Selesaikan', Input::old('directive.7')) }} Selesaikan</label>
						<label>{{ Form::checkbox('directive[8]', 'Perbaiki', Input::old('directive.8')) }} Perbaiki</label>
						<label>{{ Form::checkbox('directive[9]', 'Bicarakan dengan Saya', Input::old('directive.9')) }} Bicarakan dengan Saya</label>
						<label>{{ Form::checkbox('directive[10]', 'Ingatkan', Input::old('directive.10')) }} Ingatkan</label>
						<label>{{ Form::checkbox('directive[11]', 'Simpan', Input::old('directive.11')) }} Simpan</label>
						<label>{{ Form::checkbox('directive[12]', 'Sesuai Catatan', Input::old('directive.12')) }} Sesuai Catatan</label>
						<label>{{ Form::checkbox('directive[13]', 'Perbanyak', Input::old('directive.13')) }} Perbanyak</label>
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Disposisi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right checkbox no-margin">
						{{ count($departments) == 0 ? '&nbsp;':''}}
						@foreach ($departments as $key => $department)
							<label style="width:100%">
							{{ Form::checkbox('department[' . $key . ']', $department->id, Input::old('department.' . $key), array(
								'id' => "department_" . $department->id,
								'onclick' => "show_notes(" . $department->id . ")" 
							)) }}
							{{{ $department->name }}}</label><br>
							<textarea name="disp_notes[{{{ $department->id }}}]" id="notes_{{{ $department->id }}}" class="hidden" style="margin-bottom: 10px">{{{ Input::old('disp_notes.' . $department->id) }}}</textarea><br>
						@endforeach
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Pejabat Disposisi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<table class="table table-condensed custom-table" id="carbonCopyTable"></table>
						<input type="hidden" id="sentDispEmployee" name="carbonCopy" value="{{{ Input::old('carbonCopy') }}}">
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Petugas Pengganti
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" id="inputSubsEmployee" size="30" readonly>
						<input type="hidden" name="substitute_emp" id="sentSubsEmployee" value="{{{ Input::old('substitute_emp') }}}">
						<button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#subsEmployee">cari</button>
						@if ($errors->has('substitute_emp'))
							<br><span class="text-danger">{{{ $errors->first('substitute_emp') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="line"></div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Pelaksana Tugas
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" id="inputEmployee" size="30" readonly>
						<input type="hidden" name="assigned_emp" id="sentEmployee" value="{{{ Input::old('assigned_emp') }}}">
						<button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#employee">cari</button>
						@if ($errors->has('assigned_emp'))
							<br><span class="text-danger">{{{ $errors->first('assigned_emp') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Nota Dinas
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="official_memo" value="{{{ Input::old('official_memo') }}}" size="30">
						@if ($errors->has('official_memo'))
							<br><span class="text-danger">{{{ $errors->first('official_memo') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Tanggal Nota Dinas
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="memo_date" value="{{{ Input::old('memo_date') }}}" class="input-date">
						<span class="glyphicon glyphicon-calendar"></span>
						@if ($errors->has('memo_date'))
							<br><span class="text-danger">{{{ $errors->first('memo_date') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="line"></div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Catatan
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<textarea name="notes">{{{ Input::old('notes') }}}</textarea>
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						File
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="file" id="one_file">
						<div class="row">
							<div class="col-md-6">
								<div class="box-upload pull-left" id="dropbox">
									<span>Letakkan file di sini</span>
								</div>
							</div>
						</div>
						<table class="table table-condensed" id="fileListTable" style="margin-top: 5px"></table>
						<input type="hidden" name="file" id="inputFile">
					</div>
					</div>
				</div>
				
				<div class="line"></div>
				
				<button type="Submit" class="btn btn-xs btn-warning" id="btn_submit">
					SUBMIT
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				
			</div>
		</form>
	</div>
		
	<!--modal -->
	<div class="modal fade" id="employee">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Data Pegawai</h4>
				</div>
				<div class="modal-body">
					<div id="emp_modal" class="shade-screen hidden">
						<img src="/static/img/ajax-loader.gif">
					</div>
					<div id="divisi" class="input-group">
						<input class="form-control" type="text" placeholder="cari berdasarkan nama" id="inputSearch">		
						<span class="input-group-btn">
							<button class="btn btn-warning" type="button" id="search_emp">cari</button>
						</span>
					</div>

					<table class="table table-striped" id="employeeTable"></table>
					<span id="emp_page" class="employee">
						{{ $non_dept->links() }}
					</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="subsEmployee">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Data Pegawai</h4>
				</div>
				<div class="modal-body">
					<div id="subs_emp_modal" class="shade-screen hidden">
						<img src="/static/img/ajax-loader.gif">
					</div>
					<div id="divisi" class="input-group">
						<input class="form-control" type="text" placeholder="cari berdasarkan nama" id="inputSearchSubsEmp">		
						<span class="input-group-btn">
							<button class="btn btn-warning" type="button" id="search_subs_emp">cari</button>
						</span>
					</div>

					<table class="table table-striped" id="subsEmployeeTable"></table>
					<span id="subs_emp_page" class="subs_employee"></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="dispEmployee">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Data Pegawai</h4>
				</div>
				<div class="modal-body">
					<div id="disp_emp_modal" class="shade-screen hidden">
						<img src="/static/img/ajax-loader.gif">
					</div>
					<div id="divisi" class="input-group">
						<input class="form-control" type="text" placeholder="cari berdasarkan nama" id="inputSearchDispEmp">		
						<span class="input-group-btn">
							<button class="btn btn-warning" type="button" id="search_disp_emp">cari</button>
						</span>
					</div>

					<table class="table table-striped" id="dispEmployeeTable"></table>
					<span id="disp_emp_page" class="disp_employee"></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="reference">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Data Surat</h4>
				</div>
				<div class="modal-body">
					<div id="doc_modal" class="shade-screen hidden">
						<img src="/static/img/ajax-loader.gif">
					</div>
					<div id="divisi" class="input-group">
						<input class="form-control" type="text" placeholder="cari berdasarkan no. agenda, no. surat, hal, atau dari" id="inputSearchDoc">
						<span class="input-group-btn">
							<button class="btn btn-warning" type="button" id="search_doc">cari</button>
						</span>
					</div>

					<table class="table table-striped" id="documentTable"></table>
					<span id="doc_page" class="document">
						{{ $documents->links() }}
					</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content.js')
	<script type="x-tmpl-mustache" id="tmplFileListTable">
		<thead>
			<th>File</th>
			<th>Status</th>
			<th>Jenis</th>
			<th></th>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td style="word-break: break-all">{% file_name %}</td>
					<th id="state_{% _id %}">{% state %}</th>
					<td>
						{{ Form::select('type', array(
							'INB' => 'Surat Masuk',
							'PRO' => 'Produk',
							'OTH' => 'Lain-Lain'
						), '', array(
							'id' => 'filetype_{% _id %}',
							'onchange' => 'update_type({% _id %})',
							'style' => 'width: 150px',
							'disabled'
						)) }}
					</td>
					<td>
						<button type="button" class="btn btn-danger btn-xs hidden" onclick="remove_file({% _id %})" id="btn_remove_{% _id %}" title="Hapus File">
							<span class="glyphicon glyphicon-remove">
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplEmployeeTable">
		<thead>
			<tr>
				<th>Nama</th>
				<th>Jabatan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td>{% job_title %}</td>
					<td>
						<button type="button" class="btn btn-success btn-xs" onclick="choose_emp({% _id %})">
							Pilih <span class="glyphicon glyphicon-hand-down"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplSubsEmployeeTable">
		<thead>
			<tr>
				<th>Nama</th>
				<th>SubBagian/Seksi</th>
				<th>Jabatan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td>{% department %}</td>
					<td>{% job_title %}</td>
					<td>
						<button type="button" class="btn btn-success btn-xs" onclick="choose_subs_emp({% _id %})">
							Pilih <span class="glyphicon glyphicon-hand-down"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplDispEmployeeTable">
		<thead>
			<tr>
				<th>Nama</th>
				<th>SubBagian/Seksi</th>
				<th>Jabatan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td>{% department %}</td>
					<td>{% job_title %}</td>
					<td>
						<button type="button" class="btn btn-success btn-xs" onclick="choose_disp_emp({% _id %})">
							Pilih <span class="glyphicon glyphicon-hand-down"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplDocumentTable">
		<tbody>
			{% #data %}
				<tr>
					<th>
						<ul class="list-unstyled">
							<li>Dari</li>
							<li>No Agenda</li>
							<li>No Surat</li>
							<li>Hal</li>
						</ul>
					</th>
					<td>
						<ul class="list-unstyled">
							<li>{% received_from %}</li>
							<li>{% agenda_number %}</li>
							<li>{% document_number %}</li>
							<li>{% about %}</li>
						</ul>
					</td>
					<td style="vertical-align: middle">
						<a href="/document/{% id %}" target="_blank">
							<button type="button" class="btn btn-danger btn-sm pull-right">
								Tampilkan di Tab Baru <span class="glyphicon glyphicon-share"></span>
							</button>
						</a><br><br>
						<button type="button" class="btn btn-success btn-sm pull-right" onclick="choose_doc({% _id %})">
							Pilih <span class="glyphicon glyphicon-hand-down"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplAddressedToTable">
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %} {% label %}</td>
					<td>
						<div class="checkbox addressedto {% ^id %}hidden{% /id %}">
							<label>
								<input
									onchange="set_client_notify_email({% _id %})"
									type="checkbox"
									{% #notify_email %}
									checked="checked"
									{% /notify_email %}
									>
								Kirim email pemberitahuan
							</label>
						</div>
					</td>
					<td>
						<button type="button" class="btn btn-xs btn-danger pull-right" onclick="delete_client({% _id %})">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplCarbonCopyTable">
		<thead>
			<th>Nama</th>
			<th>Bagian</th>
			<th>
				<button class="btn btn-warning btn-xs pull-right" type="button" data-toggle="modal" data-target="#dispEmployee">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</th>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td>{% department %}</td>
					<td>
					<button type="button" class="btn btn-xs btn-danger pull-right" onclick="delete_carboncopy({% _id %})">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script src="/static/js/document.js"></script>
	<script src="/static/js/upload_file.js"></script>
	<script>
		var emp = {{ json_encode($emp) }};
		var doc = {{ json_encode($doc) }};
		var deptlist = {{ json_encode($deptlist) }};
		var page_url = '{{ route('create_doc') }}';
		var clients = {{ Input::old('clients', $document->clients) }};
		var carboncopy = [];
		var user_dept = {{ Auth::user()->department ? Auth::user()->department : 'null' }};
	</script>
	<script type="x-tmpl-mustache" id="tmplListClient">
		{% #data %}
			<option value="{% id %}"
				{% #selected %}
				selected="selected"
				{% /selected %}>{% name %}</option>
		{% /data %}
	</script>
@endsection