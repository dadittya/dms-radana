@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"], input[type="password"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
	
@endsection

@section('content')
	<div class="content-title">
		<a href="{{{ action('EmployeeController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Pegawai {{{ $emp->name }}}</span>
	</div>
	
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<form class="document-form" method="post" action="{{{ action('EmployeeController@updateRow',[$emp->id]) }}}">

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Nama Pegawai
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="name" value="{{{ Input::old('name', $emp->name) }}}" size="30">
						@if ($errors->has('name'))
							<br><span class="text-danger">{{{ $errors->first('name') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Alamat Email
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="email" value="{{{ Input::old('email', $emp->email) }}}" size="30">
						@if ($errors->has('email'))
							<br><span class="text-danger">{{{ $errors->first('email') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						NIP
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="nip" value="{{{ Input::old('nip', $emp->nip) }}}" size="30">
						@if ($errors->has('nip'))
							<br><span class="text-danger">{{{ $errors->first('nip') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						SubBagian / Seksi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('department', $department, Input::old('department', $emp->department) ) }}
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Job Title
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('job_title', $jobtitle, Input::old('job_title', $emp->job_title) ) }}
					
						@if ($errors->has('job_title'))
							<br><span class="text-danger">{{{ $errors->first('job_title') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Notification
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('email_notif', ['1' => 'Aktif', '0' => 'Non Aktif'], Input::old('email_notif', $emp->email_notif) ) }}
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Status
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						{{ Form::select('active', ['1' => 'Aktif', '0' => 'Non Aktif'], Input::old('active', $emp->active) ) }}
					</div>
					</div>
				</div>

				<div class="left-right checkbox">
					<label>{{ Form::checkbox('chg_pass', '1', Input::old('chg_pass'), array('id' => 'check-change')) }} Ubah Password</label>
				</div>

				<div class="form-group" id="new-pass">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Password Baru
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="password" name="password" size="30" id="input-new-pass">
						@if ($errors->has('password'))
							<br><span class="text-danger">{{{ $errors->first('password') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group" id="conf-pass">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Ulangi Password
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="password" name="password_confirmation" size="30" id="input-conf-pass">
						@if ($errors->has('password_confirmation'))
							<br><span class="text-danger">{{{ $errors->first('password_confirmation') }}}</span>
						@endif
					</div>
					</div>
				</div>


				<button type="Submit" class="btn btn-xs btn-warning">
					UBAH
					<span class="glyphicon glyphicon-edit"></span>
				</button>
			</div>
		</form>
	</div>
@endsection

@section('content.js')
	<script>
		$(function() {
			function change_pass() {
				if ($('#check-change').prop('checked')) {
					$('#new-pass').removeClass('hidden');
					$('#conf-pass').removeClass('hidden');
					$('#input-new-pass').removeAttr('disabled');
					$('#input-conf-pass').removeAttr('disabled');
				} else {
					$('#new-pass').addClass('hidden');
					$('#conf-pass').addClass('hidden');
					$('#input-new-pass').attr('disabled', 'true');
					$('#input-conf-pass').attr('disabled', 'true');
					$('#input-new-pass').val('');
					$('#input-conf-pass').val('');
				}
			}

			$('#check-change').on('click', change_pass);
			change_pass();
		});
	</script>
@endsection