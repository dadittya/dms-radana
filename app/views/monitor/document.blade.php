@foreach($doc_list as $document)
<div style="background-color: #eee; padding: 10px">
	<div class="myr">
		<span class="title">Pemohon</span><br>
		<span class="value" >{{{ $document->received_from_id ? $document->fromClient->name : $document->received_from }}}</span>
	</div>
	<!--<div><span class="title">Jenis Layanan</span>:<span class="value"></span></div>-->
	<div class="myr">
		<span class="title">Tanggal Surat</span><br>
		<span class="value" >{{{ i18l_date($document->document_date, ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE]) }}}</span>
	</div>
	<div class="myr">
		<span class="title">No Surat</span><br>
		<span class="value" >{{{ $document->document_number }}}</span>
	</div>
	<div class="myr">
		<span class="title">Nama Produk</span><br>
		<span class="value" >{{{ $document->about }}}</span>
	</div>
	<div class="myr">
		<span class="title">Status</span><br>
		<span class="value {{{ $document->getStatusClass()}}}" >{{{ $document->getStatus() }}}</span>
	</div>
	@if ($document->getStatusCode() == 'FNS')
		<div>
			<span class="title">Tanggal Selesai</span><br>
			<span class="value" >{{{ i18l_date($document->getFinishDate(),['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE] ) }}}</span>
		</div>
	@endif
</div>
@endforeach
