<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DMS</title>

		<link rel="icon" href="/static/img/radana_icon.png" type="image/png" />
		<link href="/static/css/bootstrap.css" rel="stylesheet">
		<link href="/static/css/monitor.display.css" rel="stylesheet">

		<style type="text/css">
			.carousel {
				/*max-height: 400px;
				overflow: hidden;*/
			}

			.myr{
				margin-bottom: 10px;
			}

			.peopleCarouselImg img {
				/*width: auto;
				height: 400px;
				min-height: 350px;
				max-height: 400px;*/
				width: 100%;
				height: auto;
			}

			.no-padding{
				padding-left: 0px; padding-right: 0px;
			}

			.title{
				font-size: 0.9em;
			}

			#Header{
				padding-bottom: 15px; 
				margin-bottom: 0px; 
				border-bottom: 1px white solid;
			}
			.entry_container{
				padding : 15px;
			}

			.carousel-caption {
			    position: relative;
			    left: auto;
			    right: auto;
			}
		</style>

	</head>

	<body>
		<div class="footer">
			<div class="cerdas">
				<marquee> Maju Berkat Pembayaran</marquee>
			</div>
		</div>
		<div class="container display_monitor" style="padding-left: 0px; padding-right: 0px;">
			<div id="Header">
				<div class="logo" style="margin-left: 15px;"><img src="/static/img/radana_icon_2.png"></div>
				<div class="caption">
					<div>
						<h3 style="margin-bottom: 0;">PT Radana Bhaskara Finance Tbk</h3>
						CIBIS Nine Building 11th Floor Suite W-16<br>
						Jl. TB Simatupang No. 2 RT.001/RW.005 Kel Cilandak Timur<br>
						Kec Pasar Minggu Jakarta 12560<br>
						Telp. 021 50991088 Fax. 021 50991089
					</div>
				</div>
				<div class="tanggal" style="margin-right: 10px;"><span class="date">Sabtu, 30 Oktober 2021</span> <span class="time">24:00</span></div>
			</div>
			<div class="col-lg-6 hidden-md hidden-sm hidden-xs promo" style="padding-right: 0;">
				<p class="promo-caption">Mitra andal, sahabat Anda</p>
			@if($jumbotrons->count() > 0)
				<div id="myCarousel" class="carousel slide" data-ride="carousel" data-keyboard="true" data-pause="click" style="text-align:-webkit-center">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				  @foreach($jumbotrons as $key => $jumbotron)
				    <li data-target="#myCarousel" data-slide-to="{{{ $key }}}" class="@if($key == 0) active @endif"></li>
				  @endforeach
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				  @foreach($jumbotrons as $key => $jumbotron)
				    <div class="item peopleCarouselImg @if($key == 0) active @endif">
						<img class="peopleCarouselImg" src="{{ $jumbotron->getImageFullPath() }}">
				    </div>
				  @endforeach
				  </div>
				</div>
			@endif
				
			</div>
			<div class="col-lg-6 col-md-12 col-xs-12 jumbo">
				<div class="jumbotron no-background">
					<?php $colnumber = 12/count($list_dep);?>
					@foreach($list_dep as $dept)
					<div class="col-xs-{{{$colnumber}}}">
						<div class="caption" style="border-top: 0px; border-left: 0px; border-right: 0px; border-color: red;">Seksi <br>{{{$dept -> name}}}</div>
						<div class="list_container" data-id="{{{$dept -> id}}}" style="border: 0px;">
							<div class="entry_container">
								{{--*/$doc_list = $dept->getDisplayDocumentList()/*--}}
								@include('monitor/document')
							</div>
						</div>
					</div>
					@endforeach
					<div class="cb"></div>
				</div>
			</div>
			
		</div>

		<script src="/static/js/jquery.min.js"></script>
		<script src="/static/js/prefixfree.min.js"></script>
		<script src="/static/js/jquery.keyframes.min.js"></script>
		<script src="/static/js/jquery.upanimator.js"></script>
		<script src="/static/js/jquery.time_idr.js"></script>
		<script src="/static/js/bootstrap.min.js"></script>
		<script src="/static/js/mustache.js"></script>
		<script type="text/javascript">
			(function($){
				$(".jumbotron .list_container").upAnimator("{{ action('DocumentController@displayMonitor') }}");
				var days = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];
				var months = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
				$(".tanggal").time_idr({{{time()}}}, false, days, months);
			})(jQuery);
		</script>
	</body>
</html>
