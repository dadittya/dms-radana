@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
@endsection
@section('content')
	<div class="content-title">
		<a href="{{{ action('DepartmentController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Tambahkan SubBagian/Seksi</span>
	</div>
				
	<div class="detail-document">
		<form class="document-form" method="post" action="{{{ action('DepartmentController@createNew') }}}">
			<div class="detail-document-form">
			@include('top_notif')
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Nama SubBagian/Seksi
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="text" name="name" size="30" value="{{{ Input::old('name') }}}">
						@if ($errors->has('name'))
							<br><span class="text-danger">{{{ $errors->first('name') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="checkbox" style="margin-top: 0px">
					<label>
	                	{{ Form::checkbox('disposition_product', '1', Input::old('disposition_product')) }} 
	                	Disposisi Produk
	                </label>
				</div>

				<button type="Submit" class="btn btn-xs btn-warning">
					SIMPAN
					<span class="glyphicon glyphicon-ok"></span>
				</button>
			</div>
		</form>
	</div>
@endsection