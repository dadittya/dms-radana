<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DMS</title>

    <link rel="icon" href="/static/img/radana_icon.png" type="image/png" />
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/navbar.css" rel="stylesheet">
    <link href="/static/css/datepicker3.css" rel="stylesheet">
    <link href="/static/css/dw.css" rel="stylesheet">
    @yield('css')

  </head>

  <body>

    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="/static/img/radana_icon_2.png" height="40px"></a>
          </div>
		  
          @if (Auth::user())
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="{{{ Request::is('/') ? 'active' : '' }}}">
                <a href="{{{ action('HomeController@showHome') }}}">Beranda</a>
              </li>
              {{-- <li class="{{{ Request::is('jumbotron') ? 'active' : '' }}}">
                <a href="{{{ action('JumbotronController@showList') }}}">Jumbotron</a>
              </li> --}}
              @if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS)
                <li class="dropdown {{{ Request::is('department*') || Request::is('jobtitle*') || Request::is('employee*') || Request::is('client*') ? 'active' : '' }}}">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    Admin<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{{ action('DepartmentController@showList') }}}">SubBagian/Seksi</a></li>
                    <li><a href="{{{ action('JobTitleController@showList') }}}">Jabatan</a></li>
                    <li><a href="{{{ action('EmployeeController@showList') }}}">Pegawai</a></li>
                    <li><a href="{{{ action('ClientController@showList') }}}">Klien</a></li>
                  </ul>
                </li>
              @endif
              <li class="dropdown {{{ Request::is('document*') && !Request::is('document/archive') ? 'active' : '' }}}">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  Surat<span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{{ action('DocumentController@showList') }}}">Daftar Surat</a></li>
                  @if (Auth::user()->level == 1 || Auth::user()->level == 2)
                  <li><a href="{{{ URL::route('create_doc') }}}">Buat Surat Baru</a></li>
                  @endif
                </ul>
              </li>
              @if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS)
                 <li class="{{{ Request::is('document/archive') ? 'active' : '' }}}">
                    <a href="{{{ URL::route('archive_doc') }}}">Buat Arsip</a>
                  </li>
              @endif
              @if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS || Auth::user()->level == JobLevelModel::KASUBAG 
                || (Auth::user()->dept && Auth::user()->dept->disposition_product)
              )
                <li class="{{{ Request::is('monitor') ? 'active' : '' }}}">
                  <a href="{{{ action('DocumentController@showMonitorDocument') }}}">Monitoring</a></li>
                </li>
              @endif
                <li class="{{{ Request::is('cs') ? 'active' : '' }}}">
                  <a href="{{{ action('DocumentController@showCSList') }}}">APT Search</a></li>
                </li>
            </ul>
            {{
                Form::open(array(
                  'action' => 'AuthController@authLogout',
                  'method' => 'post',
                  'id' => 'formLogout'
                ))
              }}
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown {{{ Request::is('account/password') ? 'active' : '' }}}">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <img src="/static/assets/user.png" class="icon-nav">User<span class="caret">
                </a>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="{{{ action('AccountController@showChangePassword') }}}"><img src="/static/assets/user.png" class="icon-nav">Ubah Password</a>
                    </li>
                    <li>
                      <a href="#" onclick="$('#formLogout').submit();"><img src="/static/assets/logout.png" class="icon-nav">Sign Out</a>
                    </li>
                    <li class="divider">
                    </li>
                    <li>
                      <a href='{{ asset("docs/User-Manual-DMS.pdf") }}' target="_blank"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Dokumentasi</a>
                    </li>

                  </ul>
              </li>
            </ul>
            {{ 
                Form::close() 
              }}
          </div>
          @endif
        </div>
      </nav>

      <div class="jumbotron">   

      @yield('content')

      </div>
  
      <div class="content-footer">
        <div class="copyright">
        <a href="{{{ action('HomeController@showAbout') }}}">DMS</a> 
        by <a href="https://www.radanafinance.co.id/" target="_blank">RADANA FINANCE</a>
        &copy; Copyright 2021  |  All Right Reserved
        </div>
      </div>
    
    </div>

    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/bootstrap-datepicker.js"></script>
    <script src="/static/js/mustache.js"></script>
    <script type="text/javascript">
      $(function () {
        $('.input-date').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: true
        });
      });
      // set mustache tags
      Mustache.tags = ['{%', '%}'];
    </script>

    @yield('content.js')
    @yield('content.modal')
  </body>
</html>
