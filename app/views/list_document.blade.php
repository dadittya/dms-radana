@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title">
		Daftar Surat
		<button type="button" id="drop-sorting" class="btn btn-xs btn-warning pull-right" style="margin-top: -2px">Sortir</button>
		<button type="button" id="drop-search" class="btn btn-xs btn-warning pull-right" style="margin-top: -2px; margin-right: 10px">Cari</button>
	</div>
	<form action="{{{ action('DocumentController@showList') }}}" method="get" id="form-search">
        @if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS)
			<div class="content-state">
				<div class="document-form">
					<div class="left-right">
						<div class="document-point-left">Status</div>
						<div class="document-point-right">
							{{ Form::select('stage', array(
								'all' => 'Semua',
								'new' => 'Dokumen Baru',
								'approve' => 'Dokumen Baru Disposisi',
								'disp_approve' => 'Pelaksanaan Dokumen Disetujui Kepala SubBagian/Seksi',
								'answer' => 'Pelaksanaan Dokumen Disetujui Kepala Kantor'
							), $filter_stage, ['style' => 'min-width: 350px']) }}
						</div>
					</div>
					<div class="left-right">
						<div class="document-point-left">Nomor Agenda</div>
						<div class="document-point-right">
							{{ Form::select('no_agenda', array(
								'all' => 'Semua',
								'yes' => 'Ada',
								'no' => 'Tidak Ada'
							), $filter_no_agenda) }}
						</div>
					</div>
				</div>
			</div>
		@endif
		<div id="content-search" class="content-search" style="{{{ $search || $advanced ? '' : 'display: none;' }}}">
			<div class="document-form">
				<div class="input-group">
					<span class="input-group-addon glyphicon glyphicon-search"></span>
					<input type="text" name="q" value="{{{ $q }}}" class="form-control" placeholder="cari berdasarkan nomor agenda, nomor surat, hal, lampiran, dari">
				</div>
				<div class="button">
					<a href="#" style="color: #333" id="drop-advanced">
						Pencarian Detail 
						<span class="glyphicon glyphicon-chevron-down" id="drop-icon"></span>
					</a>
				</div>
				<div class="content-advanced" id="advanced-search" style="{{{ $advanced ? '' : 'display: none;' }}}">
					<div class="left-right">
						<div class="document-point-left">Tanggal Masuk</div>
						<div class="document-point-right">
							<input type="text" name="received_at_from" class="input-date" value="{{{ $received_at_from ? $received_at_from->format('d/m/Y') : '' }}}" id="received-at-from">
							<span class="glyphicon glyphicon-calendar"></span>
							<a href="#" style="color: #333" id="show-received-at-to">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
							<div class="hidden" id="received-at-to" style="display: inline">
								<input type="text" name="received_at_to" class="input-date" value="{{{ $received_at_to ? $received_at_to->format('d/m/Y') : '' }}}" id="r_to">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
					<div class="left-right">
						<div class="document-point-left">Tanggal Selesai</div>
						<div class="document-point-right">
							<input type="text" name="target_date_from" class="input-date" value="{{{ $target_date_from ? $target_date_from->format('d/m/Y') : '' }}}" id="target-date-from">
							<span class="glyphicon glyphicon-calendar"></span>
							<a href="#" style="color: #333" id="show-target-date-to">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
							<div class="hidden" id="target-date-to" style="display: inline">
								<input type="text" name="target_date_to" class="input-date" value="{{{ $target_date_to ? $target_date_to->format('d/m/Y') : '' }}}" id="t_to">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
					<div class="left-right">
						<div class="document-point-left">Tanggal Surat</div>
						<div class="document-point-right">
							<input type="text" name="document_date_from" class="input-date" value="{{{ $document_date_from ? $document_date_from->format('d/m/Y') : '' }}}" id="document_date_from">
							<span class="glyphicon glyphicon-calendar"></span>
							<a href="#" style="color: #333" id="show-document-date-to">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
							<div class="hidden" id="document-date-to" style="display: inline">
								<input type="text" name="document_date_to" class="input-date" value="{{{ $document_date_to ? $document_date_to->format('d/m/Y') : '' }}}" id="d_to">
								<span class="glyphicon glyphicon-calendar"></span>
							</div>
						</div>
					</div>
					<div class="left-right">
						<div class="document-point-left">SubBagian/Seksi</div>
						<div class="document-point-right">
							{{ Form::select('assigned_dep', $deptlist, $assigned_dep, array(
								'id' => 'dept'
							) ) }}
						</div>
					</div>
					<div class="left-right">
						<div class="document-point-left">Pejabat</div>
						<div class="document-point-right">
							<input type="text" id="inputEmployee" size="30" readonly>
							<input type="hidden" name="assigned_emp" value="{{{ $assigned_emp }}}" id="sentEmployee" value="">
							<button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#employee">cari</button>
						</div>
					</div>
					@if (Auth::user()->level != JobLevelModel::KEPALA && Auth::user()->level != JobLevelModel::SEKRETARIS)
					<div class="button text-right">
						<button type="submit" class="btn btn-xs btn-warning">Terapkan</button>
					</div>
					@endif
				</div>
			</div>
		</div>
       	@if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS)
		<div class="button text-right" style="margin-right: 30px;">
			<button type="submit" class="btn btn-xs btn-warning">Terapkan</button>
		</div>
		@endif
		<div class="content-sort" id="sort-doc" style="{{{ $sort_by ? '' : 'display: none;' }}}">
			<div class="document-form">
				<div class="form-group" id="sortDocs"></div>
				{{ Form::hidden('sort_by', '', array('id' => 'sentSortBy')) }}
			</div>
		</div>
	</form>
	<div class="content-column">
		<table class="table table-notif hover">
			<thead>
				<tr>
					<th>Sifat</th>
					<th>Klasifikasi</th>
					<th>No Agenda</th>
					<th>No Surat</th>
					<th>Hal</th>
					<th>Dari</th>
					<th>&nbsp;</th>
					<th style="text-align: right">Tgl Selesai</th>
				</tr>
			</thead>
			@foreach ($documents as $document)
				<tbody onclick="document.location = '{{{ action('DocumentController@showDetail', array($document->id)) }}}'" style="background-color: {{{ $document->defineBgColor() }}}">
					<tr>
						<td>
							@if ($document->urgency == 'KLT')
								Kilat
							@elseif ($document->urgency == 'SSG')
								Sangat Segera
							@elseif ($document->urgency == 'SGR')
								Segera
							@else
								Biasa
							@endif
						</td>
						<td>
							@if ($document->clasification == 'SRS')
								Sangat Rahasia
							@elseif ($document->clasification == 'RHS')
								Rahasia
							@elseif ($document->clasification == 'KFD')
								Konfidential
							@else
								Biasa
							@endif
						</td>
						<td>{{{ $document->agenda_number }}}</td>
						<td>{{{ $document->document_number }}}</td>
						<td>{{{ $document->about }}}</td>
						<td>{{{ $document->received_from_id ? $document->fromClient->name : $document->received_from }}}</td>
						<td style="min-width: 150px">
							@if ($document->is_product)
								<span class="label label-success label-big">PRODUK</span>
							@else
								<span class="label label-primary label-big">BUKAN PRODUK</span>
							@endif
							@if ($document->target_date)
								<span class="label label-danger label-big">TARGET</span>
							@endif
						</td>
						<td style="text-align:right; min-width: 130px">
							@if ($document->calcPercentageDeadline() <= 25)
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
							@elseif ($document->calcPercentageDeadline() <= 30)
								<span class="glyphicon glyphicon-fire"></span>
								<span class="glyphicon glyphicon-fire"></span>
							@elseif ($document->calcPercentageDeadline() <= 50)
								<span class="glyphicon glyphicon-fire"></span>
							@endif

							{{{ $document->target_date ? $document->target_date->format('d/m/Y') : '-' }}}
						</td>
					</tr>
					<tr>
						<td colspan="8">
							Status : {{{ $document->stageDesc }}}<br>
							File terlampir :
							<?php $count = 0?>
							@foreach ($document->action as $action)
								@foreach ($action->file as $file)
									<div style="padding-left: 20px">
										<a href="{{{ action('DocumentController@downloadFile', array($file->id)) }}}">
											<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
											{{{ $file->originalname }}} 
										</a>
											@if ($file->type == 'INB')
												(Surat Masuk)
											@elseif ($file->type == 'PRO')
												(Produk)
											@elseif ($file->type == 'OTH')
												(Lain-Lain)
											@else
												({{{ $file->type }}})
											@endif
									</div>
									<?php $count += 1?>
								@endforeach
							@endforeach
							@if ($count <= 0)
								-
							@endif
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<ul class="comma-list">
								Disposisi kepada :
								@if (count($document->docDisposition))
									@foreach ($document->docDisposition as $dept)
										<li>{{{ $dept->name }}}</li>
									@endforeach
								@else
									-
								@endif
							</ul>
						</td>
						<td colspan="3" style="text-align: right">
							Perubahan terakhir : {{{ $document->last_action()->created_at->format('d F Y') }}} 
								oleh {{{ $document->last_action()->employee->name }}} {{{ $document->last_action()->employee->dept ? '(' . $document->last_action()->employee->dept->name . ')' : '' }}}
						</td>
					</tr>
				</tbody>
			@endforeach
		</table>
	</div>
	<div style="margin: 0 30px; text-align: right">
		{{ $documents->appends(array(
			'q' => $q,
			'received_at_from' => $received_at_from ? $received_at_from->format('d/m/Y') : '',
			'received_at_to' => $received_at_to ? $received_at_to->format('d/m/Y') : '',
			'target_date_from' => $target_date_from ? $target_date_from->format('d/m/Y') : '',
			'target_date_to' => $target_date_to ? $target_date_to->format('d/m/Y') : '',
			'document_date_from' => $document_date_from ? $document_date_from->format('d/m/Y') : '',
			'document_date_to' => $document_date_to ? $document_date_to->format('d/m/Y') : '',
			'assigned_dep' => $assigned_dep,
			'assigned_emp' => $assigned_emp,
			'sort_by' => json_encode($sort_by)
		))->links() }}
	</div>

	<!--modal -->
	<div class="modal fade" id="employee">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Data Pegawai</h4>
				</div>
				<div class="modal-body">
					<div id="emp_modal" class="shade-screen hidden">
						<img src="/static/img/ajax-loader.gif">
					</div>
					<div id="divisi" class="input-group">
						<input class="form-control" type="text" placeholder="cari berdasarkan nama" id="inputSearch">		
						<span class="input-group-btn">
							<button class="btn btn-warning" type="button" id="search_emp">cari</button>
						</span>
					</div>

					<table class="table table-striped" id="employeeTable"></table>
					<span id="emp_page" class="employee">
						{{ $employees->links() }}
					</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content.js')
	<script type="x-tmpl-mustache" id="tmplEmployeeTable">
		<thead>
			<tr>
				<th>Nama</th>
				<th>SubBagian/Seksi</th>
				<th>Jabatan</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td>{% department %}</td>
					<td>{% job_title %}</td>
					<td>
						<button type="button" class="btn btn-success btn-xs" onclick="choose_emp({% _id %})">
							Pilih <span class="glyphicon glyphicon-hand-down"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>

	<script type="x-tmpl-mustache" id="tmplSortDocs">
		<table class="table table-sort">
			{% #data %}
				<tr>
					<td>{% name %}</td>
					<td style="font-size: 16px">
						<span id="icon_{% _id %}"></span>
					</td>
					<td style="text-align: right">
						<button type="button" class="btn btn-xs btn-danger" onclick="delete_sort({% _id %})">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
					</td>
				</tr>
			{%/data%}
		</table>

		{{ Form::select('', array(
			'last_action' => 'Perubahan Terakhir',
			'agenda_number' => 'Nomor Agenda',
			'received_at' => 'Tanggal Masuk',
			'document_number' => 'Nomor Surat',
			'document_date' => 'Tanggal Surat',
			'target_date' => 'Tanggal Selesai'
		), '', array('id' => 'inputFieldSort')) }}
		{{ Form::select('', array(
			'asc' => 'A > Z',
			'desc' => 'Z > A'
		), '', array('id' => 'inputSortBy')) }}
		<button type="button" class="btn btn-xs btn-success" onclick="add_sort()">
			<span class="glyphicon glyphicon-plus"></span>
		</button>
		<button type="submit" class="btn btn-xs btn-warning" id="sort-button">
			Urutkan
		</button>
	</script>

	<script>
		var result_emp;
		var dept = $('#dept').val();
		var emp = {{ json_encode($emp) }};
		var sorting = {{ $sort_by ? json_encode($sort_by) : '[]' }};

		function choose_emp(i) {
			if (result_emp)
				var choose = result_emp[i];
			else
				var choose = emp[i];
				
			$('#inputEmployee').val(choose.name);
			$('#sentEmployee').val(choose.id);
			$('#employee').modal('hide');
		}

		function check_sortby()
		{
			for (var i in sorting) {
				if (sorting[i].sort == 'asc') {
					$('#icon_' + i).addClass('glyphicon glyphicon-sort-by-alphabet')
					console.log('asc');
				}
				else
					$('#icon_' + i).addClass('glyphicon glyphicon-sort-by-alphabet-alt')
			}
		}

		function render_sort_by()
		{
			for (idx in sorting) {
				sorting[idx]['_id'] = idx;
			}

			$('#sentSortBy').val(JSON.stringify(sorting));
			var tmpl = $('#tmplSortDocs').html();
			Mustache.parse(tmpl);
			var html = Mustache.render(tmpl, { 'data': sorting });
			$('#sortDocs').html(html);

			check_sortby();
		}

		function add_sort()
		{
			var found;
			var f = $('#inputFieldSort').val();
			var n = $('#inputFieldSort option:selected').html();
			var b = $('#inputSortBy').val();

			var input = {
				'field' : f,
				'name' : n,
				'sort' : b
			};

			for (var i in sorting) {
				if (sorting[i].name == n) {
					found = sorting[i];
					break;
				}
			}

			if (!found)
				sorting.push(input);

			render_sort_by();
		}

		function delete_sort(i)
		{
			sorting.splice(i, 1);
			render_sort_by();
		}

		$(function () {
			$('#drop-search').on('click', function () {
				if ($('#content-search').attr('style') == 'display: none;')
					$('#content-search').slideDown('slow');
				else
					$('#content-search').slideUp('slow');
			});

			$('#drop-advanced').on('click', function () {
				if ($('#advanced-search').attr('style') == 'display: none;') {
					$('#advanced-search').slideDown('slow');
					$('#drop-icon').removeClass('glyphicon glyphicon-chevron-down');
					$('#drop-icon').addClass('glyphicon glyphicon-chevron-up');
				} else {
					$('#advanced-search').slideUp('slow');
					$('#drop-icon').removeClass('glyphicon glyphicon-chevron-up');
					$('#drop-icon').addClass('glyphicon glyphicon-chevron-down');
				}
			});

			$('#show-received-at-to').on('click', function () {
				if ($('#received-at-to').hasClass('hidden') && $('#received-at-from').val() != '') {
					$('#r_to').val($('#received-at-from').val());
					$('#received-at-to').removeClass('hidden');
				} else {
					$('#received-at-to').addClass('hidden');
					$('#r_to').val('');
				}
			});

			$('#show-target-date-to').on('click', function () {
				if ($('#target-date-to').hasClass('hidden') && $('#target-date-from').val() != '') {
					$('#t_to').val($('#target-date-from').val());
					$('#target-date-to').removeClass('hidden');
				} else {
					$('#target-date-to').addClass('hidden');
					$('#t_to').val('');
				}
			});

			$('#show-document-date-to').on('click', function () {
				if ($('#document-date-to').hasClass('hidden') && $('#document_date_from').val() != '') {
					$('#d_to').val($('#document_date_from').val());
					$('#document-date-to').removeClass('hidden');
				} else {
					$('#document-date-to').addClass('hidden');
					$('#d_to').val('');
				}
			});

			$('#search_emp').on('click', filter_employee);

			$('#drop-sorting').on('click', function () {
				if ($('#sort-doc').attr('style') == 'display: none;')
					$('#sort-doc').slideDown('slow');
				else 
					$('#sort-doc').slideUp('slow');
			});

			function render_employee(filter) {
				for (idx in filter) {
					filter[idx]['_id'] = idx;
				}

				var tmpl = $('#tmplEmployeeTable').html();
				Mustache.parse(tmpl);
				var html = Mustache.render(tmpl, { 'data': filter });
				$('#employeeTable').html(html);
			}

			function filter_employee() {
				var q = $('#inputSearch').val();
				$('#emp_modal').removeClass('hidden');

				$.ajax ({
					url: '/employee/filter' + '?q=' + q,
					type: 'GET',
					success: function(result){
						$('#emp_modal').addClass('hidden');
						result_emp = result.emp;
						render_employee(result_emp);
						$('#emp_page').html(result.employees);
					}
				});
			}

			function input_old() {
				for (var i = 0 in emp) {
					if (emp[i].id == $('#sentEmployee').val()) {
						$('#inputEmployee').val(emp[i].name);
						break;
					}
				}
			}

			//pagination search employee
			$(document).on('click', '.employee .pagination a', function (e) {
		        var page = $(this).attr('href').split('page=')[1];
		        var q = $('#inputSearch').val();
		        $('#emp_modal').removeClass('hidden');

		        $.ajax ({
		        	url: '/employee/filter' + '?page=' + page + '&q=' + q,
		        	type: 'GET',
		        	dataType: 'json',
		        	success: function(data){
		        		$('#emp_modal').addClass('hidden');
		        		result_emp = data.emp;
		        		render_employee(result_emp);
		        		$('#emp_page').html(data.employees);
		        	}
		        });
		        e.preventDefault();
		    });

		    if ($('#r_to').val() != '')
		    	$('#received-at-to').removeClass('hidden');

		    if ($('#t_to').val() != '')
		    	$('#target-date-to').removeClass('hidden');

		    if ($('#d_to').val() != '')
		    	$('#document-date-to').removeClass('hidden');

	        $('#employee').on('shown.bs.modal', function () {
				$('#inputEmployee').val('');
				$('#sentEmployee').val('');
			});

			render_employee(emp);
			render_sort_by();
			input_old();
		});
	</script>
@endsection