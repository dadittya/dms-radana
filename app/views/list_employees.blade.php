@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title">
		Daftar Pegawai
		<div class="pull-right">
			<form method="get" action="{{{ action('EmployeeController@showList') }}}">
				<div class="form-group search">
					<input type="text" name="q" value="{{{ (isset($q) ? $q : '') }}}" class="form-control" placeholder="Search">
				</div>
			</form>
		</div>
	</div>
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<table class="table table-hover">
				<tr>
					<th>Nama Pegawai</th>
					<th>Alamat Email</th>
					<th>NIP</th>
					<th>SubBagian/Seksi</th>
					<th>Jabatan</th>
					<th>Status</th>
				</tr>
				@foreach($emps as $emp)
					<tr>
						<td>
							<a href="{{{ action('EmployeeController@showEditForm', [$emp->id]) }}}" title="Ubah" style="color: #333">
								<span class="btn btn-default btn-xs glyphicon glyphicon-pencil"></span>
								<span>{{{ $emp->name }}}</span>
							</a>
						</td>
						<td>{{{ $emp->email }}}</td>
						<td>{{{ $emp->nip }}}</td>
						<td>{{{ $emp->dept ? $emp->dept->name : '' }}}</td>
						<td>{{{ $emp->jobtitle->name }}}</td>
						<td>
							@if ($emp->active == "1")
								Aktif
							@else
								NonAktif
							@endif
						</td>
					</tr>
				@endforeach
			</table>

			<div style="text-align: right">
				{{ $emps->links() }}
			</div>

			<a href="{{{ action('EmployeeController@showNewForm') }}}" title="Tambah">
				<button class="btn btn-warning btn-sm">Tambah Pegawai Baru</button>
			</a>
		</div>
	</div>
@endsection