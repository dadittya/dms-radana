<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Baru 
		@if ($document->is_product)
			<span class="label label-success label-big">PRODUK</span>
		@else
			<span class="label label-primary label-big">BUKAN PRODUK</span>
		@endif
		<br>
		Menunggu persetujuan dari Kepala Kantor.
	</div>
</div>