<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Baru 
		@if ($document->is_product)
			<span class="label label-success label-big">PRODUK</span>
		@else
			<span class="label label-primary label-big">BUKAN PRODUK</span>
		@endif
		<br>
		Jika isi dari dokumen ini sudah sesuai, klik tombol Setuju untuk memulai proses.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			@if ($document->is_product)
				<button type="submit" name="stage" value="PRAPP" class="btn btn-xs btn-warning">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					Setuju Proses Dokumen Produk
				</button>
			@else
				<button type="submit" name="stage" value="NPAPP" class="btn btn-xs btn-warning">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					Setuju Proses Dokumen <b>Bukan</b> Produk
				</button>
			@endif
			<button type="submit" name="stage" value="PRDSC" class="btn btn-xs btn-danger pull-right">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>