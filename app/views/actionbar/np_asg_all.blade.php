<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		@if ($target_date)
			Status : Dokumen Sedang Dalam Proses<br>
			Menunggu pelaksanaan isi dokumen oleh staf yang ditugaskan.
		@else
			Status : Dokumen Terkirim ke Staf <br>
			Menunggu konfirmasi telah diterimanya dokumen oleh staf yang bersangkutan.
		@endif
	</div>
</div>