<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Telah Disetujui <br>
		Jika dokumen telah dikirimkan kepada Kepala SubBagian/Seksi yang bersangkutan, klik tombol Terkirim.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="NPDLV" class="btn btn-xs btn-warning">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				Terkirim
			</button>
			<button type="submit" name="stage" value="NPDSC" class="btn btn-xs btn-danger pull-right">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>