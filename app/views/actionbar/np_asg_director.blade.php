<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		@if ($target_date)
			Status : Dokumen Sedang Dalam Proses<br>
			Untuk mengajukan pelaksanaan isi dokumen, klik tombol Ajukan.
		@else
			Status : Dokumen Terkirim ke Staf <br>
			Untuk konfirmasi diterimanya dokumen, klik tombol Terima.
		@endif
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="{{{ $target_date ? 'NPPRO':'NPFNS' }}}" 
				class="btn btn-xs btn-warning"
			>
				{{{ $target_date ? 'Ajukan' : 'Terima' }}}
			</button>
			<button type="submit" name="stage" value="NPDSC" class="btn btn-xs btn-danger pull-right">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>