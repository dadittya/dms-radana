<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Terkirim <br>
		Jika isi dari dokumen ini sudah sesuai, klik tombol Proses untuk menyerahkan penugasan kepada staf yang ditunjuk.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="PRASG" class="btn btn-xs btn-warning">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				Proses
			</button>
			<button type="submit" name="stage" value="PRDSC" class="btn btn-xs btn-danger pull-right">
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>