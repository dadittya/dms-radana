<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Pengajuan Pelaksanaan Dokumen <br>
		Jika pelaksanaan isi dokumen telah sesuai, klik tombol Setuju.
		Atau klik tombol Tolak, untuk meninjau ulang pelaksanaan isi dokumen.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="{{{ isset($non_product) ? 'NPDAP' : 'PRDAP' }}}" 
				class="btn btn-xs btn-success"
			>
				<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
				Setuju
			</button>
			<button type="submit" name="stage" value="{{{ isset($non_product) ? 'NPASG' : 'PRASG' }}}" 
				class="btn btn-xs btn-warning"
			>
				<span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>
				Tolak
			</button>
			<button type="submit" name="stage" value="{{{ isset($non_product) ? 'NPDSC' : 'PRDSC' }}}" 
				class="btn btn-xs btn-danger pull-right"
			>
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>