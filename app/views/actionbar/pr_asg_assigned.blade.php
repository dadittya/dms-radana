<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Sedang Dalam Proses<br>
		Untuk mengajukan pelaksanaan isi dokumen, klik tombol Ajukan.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="PRPRO" class="btn btn-xs btn-warning">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				Ajukan
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>