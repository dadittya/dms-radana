<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Produk Telah Selesai <br>
		Jika produk telah dikirimkan kepada pihak peminta layanan, klik tombol Terkirim.<br>
		Dengan terkirimnya produk ini, maka dokumen dinyatakan selesai.
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="{{{ isset($non_product) ? 'NPFNS' : 'PRFNS' }}}" 
				class="btn btn-xs btn-warning"
			>
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				Terkirim
			</button>
			<button type="submit" name="stage" value="{{{ isset($non_product) ? 'NPDSC' : 'PRDSC' }}}" 
				class="btn btn-xs btn-danger pull-right"
			>
				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				Hentikan Proses
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>