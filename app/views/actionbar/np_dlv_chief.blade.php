<div class="notification" style="margin-top: 20px">
	<div class="notification-content">
		Status : Dokumen Terkirim ke SubBagian/Seksi <br>
		@if ($target_date)
			Jika isi dari dokumen ini sudah sesuai, klik tombol Proses untuk menyerahkan penugasan kepada staf yang ditunjuk.
		@else
			Jika dokumen telah dikirimkan kepada staf yang ditunjuk, klik tombol Terkirim.
		@endif
	</div>
	<div class="notification-button">
		<form method="post" action="{{{ action('DocumentController@changeState', array($id)) }}}">
			@if (isset($_token))
				<input type="hidden" name="_token" value="{{{ $_token }}}">
			@endif
			<button type="submit" name="stage" value="NPASG" class="btn btn-xs btn-warning">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				{{{ $target_date ? 'Proses' : 'Terkirim' }}}
			</button>
		</form>
	</div>
	<div class="clearfix">
	</div>
</div>