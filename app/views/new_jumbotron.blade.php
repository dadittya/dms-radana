@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
@endsection
@section('content')
	<div class="content-title">
		<a href="{{{ action('JumbotronController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Tambahkan Jumbotron</span>
	</div>
				
	<div class="detail-document">
		<form class="document-form" method="post" enctype='multipart/form-data' action="{{{ action('JumbotronController@createNew') }}}">
			<div class="detail-document-form">
			@include('top_notif')
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<span class="control-label">
							Judul
						</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="document-point-right">
							<input type="text" name="title" size="30" value="{{{ Input::old('title') }}}">
							@if ($errors->has('title'))
								<br><span class="text-danger">{{{ $errors->first('title') }}}</span>
							@endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<span class="control-label">
							Link
						</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="document-point-right">
							<input type="text" name="link" size="30" value="{{{ Input::old('link') }}}">
							@if ($errors->has('link'))
								<br><span class="text-danger">{{{ $errors->first('link') }}}</span>
							@endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<span class="control-label">
								Gambar
						</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="document-point-right">
							<input type="file" name="image">
							<p style="font-size: 12px;">Recomended resolution 1393 x 621 px (JPG / PNG).</p>
							@if ($errors->has('image'))
								<br><span class="text-danger">{{{ $errors->first('image') }}}</span>
							@endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<span class="control-label">
								Status
						</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="document-point-right">
							<select name="state">
								<option value="DRF">Draft</option>
								<option value="PUB">Publish</option>
							</select>
							@if ($errors->has('state'))
								<br><span class="text-danger">{{{ $errors->first('state') }}}</span>
							@endif
						</div>
					</div>
				</div>

				<button type="Submit" class="btn btn-xs btn-warning">
					SIMPAN
					<span class="glyphicon glyphicon-ok"></span>
				</button>
			</div>
		</form>
	</div>
@endsection