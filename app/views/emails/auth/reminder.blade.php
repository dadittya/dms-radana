<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Lupa Password</h3>

		<div>
			<p>Untuk mereset password anda, silahkan isi form berikut ini:</p>
			<p style="margin-left: 15px">{{ URL::to('account/password/reset', array($token)) }}.</p>
			<p>Link akan kadaluarsa dalam {{ Config::get('auth.reminder.expire', 60) }} menit.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
