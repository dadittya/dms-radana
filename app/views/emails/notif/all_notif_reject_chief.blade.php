<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Produk Tidak Disetujui Kepala SubBagian/Seksi</h3>

		<div>
			<p>Produk dari dokumen dengan nomor surat {{{ $document->document_number }}} baru saja tidak disetujui oleh Kepala SubBagian/Seksi dan menunggu pengajuan kembali oleh pejabat pelaksana.</p>
			<p style="margin-left: 15px"><a href="{{{ action('DocumentController@showDetail', [$document->id]) }}}">{{{ action('DocumentController@showDetail', [$document->id]) }}}</a></p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
