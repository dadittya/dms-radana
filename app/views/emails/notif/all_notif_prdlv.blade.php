<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Menunggu Penugasan Pelaksana</h3>

		<div>
			<p>Dokumen nomor surat {{{ $document->document_number }}} baru saja dikirim dan menunggu untuk diproses.</p>
			<p style="margin-left: 15px"><a href="{{{ action('DocumentController@showDetail', [$document->id]) }}}">{{{ action('DocumentController@showDetail', [$document->id]) }}}</a></p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
