<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Telah Selesai</h3>

		<div>
			<p>
				@if ($document->target_date)
					Produk dari dokumen dengan nomor surat {{{ $document->document_number }}} baru saja telah dikirimkan kepada peminta jasa.
				@else
					Dokumen dengan nomor surat {{{ $document->document_number }}} telah diterima oleh {{{ $document->assigned->name }}}.
				@endif
			</p>
			<p style="margin-left: 15px"><a href="{{{ action('DocumentController@showDetail', [$document->id]) }}}">{{{ action('DocumentController@showDetail', [$document->id]) }}}</a></p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>