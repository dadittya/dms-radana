<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Diterima</h3>

		<div>
			<p>Dokumen anda dengan nomor surat {{{ $document->document_number }}} saat ini telah diterima oleh KPKNL Samarinda dan dimulai proses pengerjaannya.</p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
