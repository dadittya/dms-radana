<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Selesai Dikerjakan</h3>

		<div>
			<p>Dokumen anda dengan nomor surat {{{ $document->document_number }}} saat ini telah selesai dikerjakan.</p>
			<p>Anda dapat melihat hasil proses surat anda melalui link berikut ini:</p>
			<p style="margin-left: 15px"><a href="{{{ action('DocumentController@showPublic', [$document->id, $document->getPublicKey()]) }}}">{{{ action('DocumentController@showPublic', [$document->id, $document->getPublicKey()]) }}}</a></p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
