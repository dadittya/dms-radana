<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Surat No {{{ $document->document_number }}} Proses Disetujui</h3>

		<div>
			<p>Dokumen dengan nomor surat {{{ $document->document_number }}} baru saja disetujui oleh Kepala Kantor dan menunggu untuk dikirimkan ke Kepala SubBagian/Seksi.</p>
			<p style="margin-left: 15px"><a href="{{{ action('DocumentController@showDetail', [$document->id]) }}}">{{{ action('DocumentController@showDetail', [$document->id]) }}}</a></p>
		</div>

		<div style="font-size: 11px; margin-top: 10px">
			Surat elektronik ini dikirimkan secara otomatis oleh SIMOS KPKNL Samarinda.
		</div>
	</body>
</html>
