@extends('master_layout')
@section('css')
	<style type="text/css">
		.col-md-2, .col-md-10{
			padding:0 0 0 0;
		}

		.dis-label{
			width: 100%;
		}

		@media (max-width: 600px) {
		    input[type="text"]{
		    	width:100%;
		    }
		    .document-form select, .document-form textarea, .custom-table{
		    	width:100%;
		    }
		    .document-point-right{
		    	width: 100%;
		    }
		}
		.form-group span{
			font-weight: bold;
		}
	</style>
@endsection
@section('content')
	<div class="content-title">Ubah Password</div>
				
	<div class="detail-document">
		<form class="document-form" method="post" action="{{{ action('AccountController@doChangePassword') }}}">
			<div class="detail-document-form">
			@include('top_notif')
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Password
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="password" name="old_password" size="30">
						@if ($errors->has('old_password'))
							<br><span class="text-danger">{{{ $errors->first('old_password') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Password Baru
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="password" name="password" size="30">
						@if ($errors->has('password'))
							<br><span class="text-danger">{{{ $errors->first('password') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span class="control-label">
						Ulangi Password
					</span>
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="document-point-right">
						<input type="password" name="password_confirmation" size="30">
						@if ($errors->has('password_confirmation'))
							<br><span class="text-danger">{{{ $errors->first('password_confirmation') }}}</span>
						@endif
					</div>
					</div>
				</div>

				<button type="Submit" class="btn btn-xs btn-warning">
					SIMPAN
					<span class="glyphicon glyphicon-ok"></span>
				</button>
			</div>
		</form>
	</div>
@endsection