@extends('master_layout')

@section('css')
	<style type="text/css">
	.col-md-2, .col-md-10, .col-md-12, .col-md-4, .col-md-8, .col-md-6{
			padding:0 0 0 0;
		}
	.detail-header{
		text-transform: uppercase;
	    font-weight: 700;
	    color: #000;
	    font-size: 13px;
	    margin-top: 10px;
		padding-left: 40px;
		padding-right: 40px;
	}
	.header-border{
		border-bottom: 1px solid #ddd;
	}
	.detail-doc{
		color: #000;
	    font-size: 13px;
	    padding-left: 40px;
	    margin-top: 10px;
	}
	
	.list-header ul li{
		list-style: none;
	    display: inline-block;
	    padding: 0 10px;

	}
	.detail-about{
		margin-top: 10px;
		margin-bottom:10px;
	}

	.button-process button{
		margin-top: 10px;
	}

	</style>
@endsection

@section('content')
	<div class="content-title">
		<a href="{{{ action('DocumentController@showList') }}}" title="Kembali">
			<span class="btn btn-default btn-xs glyphicon glyphicon-chevron-left"></span>
		</a>
		<span>Detail Surat</span>
	</div>

	<div class="content-notif">
		@if(Session::has('UPDATE.FAIL'))
			<div class="alert alert-danger">
				Tidak berhasil disimpan
				@if ($errors->has())
					<ul>
					@foreach ($errors->all() as $error)
						<li>{{{ $error }}}</li>
					@endforeach
					</ul>
				@endif
			</div>
		@else
			@include('top_notif')
		@endif
	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12 detail-header">
		<div class="col-md-12 col-sm-12 col-xs-12 header-border">
			<div class="col-md-6 col-sm-6 col-xs-12 list-header detail-about">
				<ul style="padding-left: 0">
					<li class="border-right">{{{ $document->about }}}</li>
					<li class="border-right" style="padding-left: 0">{{{ $document->agenda_number }}}</li>
					<li class="border-right">{{{ $document->received_at->format('d F Y') }}}</li>
					<li class="border-right">
						@if ($document->is_product)
							<span class="label label-success">PRODUK</span>
						@else
							<span class="label label-primary">BUKAN PRODUK</span>
						@endif
					</li>
					<li class="border-right">
						@if ($document->calcPercentageDeadline() <= 25)
							<span class="glyphicon glyphicon-fire"></span>
						@endif
						@if ($document->calcPercentageDeadline() <= 30)
							<span class="glyphicon glyphicon-fire"></span>
						@endif
						@if ($document->calcPercentageDeadline() <= 50)
							<span class="glyphicon glyphicon-fire"></span>
						@endif
					</li>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 pull-right detail-about">
				@if ($document->stage != 'PRFNS' && $document->stage != 'PRDSC' && $document->stage != 'NPFNS' && 
					$document->stage != 'NPDSC' || Auth::user()->level == JobLevelModel::KEPALA || 
					Auth::user()->level == JobLevelModel::SEKRETARIS)
					<a href="{{{ URL::route('edit_doc', array($document->id)) }}}">
						<button class="btn btn-xs btn-warning">
							<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							Ubah
						</button>
					</a>
				@endif
				<a href="{{{ action('DocumentController@printDisposition', array($document->id)) }}}" target="_blank">
					<button class="btn btn-xs btn-success">
						<span class="glyphicon glyphicon glyphicon-print" aria-hidden="true"></span>
						Cetak
					</button>
				</a>
			</div>
		</div>
	</div>



	<div class="col-md-12 col-sm-12 col-xs-12 detail-doc ">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12">		
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Nomor Surat</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->document_number }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Tanggal Surat</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->document_date->format('d F Y') }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Dari</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->fromClient ? $document->fromClient->name : $document->received_from }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Kepada</label>
				<label class="col-md-8 col-sm-7 col-xs-12">
					<ul class="comma-list no-margin">
						@foreach ($document->toClient as $i)
							<li>{{{ $i->name }}}</li>
						@endforeach
					</ul>
				</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Hal</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->about }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Lampiran</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->enclosure ? $document->enclosure : '-' }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">	
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Sifat</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->defineUrgency() }}}</label>
			</div>
			@if ($document->reference())
				<div class="col-md-12 col-sm-12 col-xs-12">	
					<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Referensi</label>
					<label class="col-md-8 col-sm-7 col-xs-12">
						<a href="{{{ action('DocumentController@showDetail', array($document->reference()->id)) }}}"
							target="_blank"
						>
							{{{ $document->reference()->document_number }}}
						</a>
					</label>
				</div>
			@endif
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Klasifikasi</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->defineClasification() }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Tanggal Selesai</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->target_date ? $document->target_date->format('d F Y') : '-' }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">SubBagian/Seksi</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ ($document->assignedDept ? $document->assignedDept->name : '') }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Disposisi</label>
				<label class="col-md-8 col-sm-7 col-xs-12">
					<ul class="comma-list no-margin">
						@if ($document->docDisposition()->first()) 
							@foreach ($document->docDisposition()->get() as $dept)
								<li>{{{ $dept->name }}}</li>
							@endforeach
						@else
							-
						@endif
					</ul>
				</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Pelaksana Tugas</label>
				<label class="col-md-8 col-sm-7 col-xs-12">
					@if ($document->assigned)
						{{{ $document->assigned->dept ? $document->assigned->name . ' (' . $document->assigned->dept->name . ')' : $document->assigned->name }}}
					@else
						-
					@endif
				</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Petugas Pengganti</label>
				<label class="col-md-8 col-sm-7 col-xs-12">
					@if ($document->substitute)
						{{{ $document->substitute->dept ? $document->substitute->name . ' (' . $document->substitute->dept->name . ')' : $document->substitute->name }}}
					@else
						-
					@endif
				</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Nota Dinas</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->official_memo ? $document->official_memo : '-' }}}</label>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="col-md-4 col-sm-5 col-xs-12 bold-head">Tanggal Nota Dinas</label>
				<label class="col-md-8 col-sm-7 col-xs-12">{{{ $document->memo_date ? $document->memo_date->format('d F Y') : '-' }}}</label>
			</div>
		</div>
	</div>
	
	
	
	

	<div class="clearfix"></div>

	<div class="detail-overview button-process">
		{{ $document->renderActionBar() }}
	</div>
	
	<div class="disposition full">
		<div class="pull-right">
			<a href="{{{ action('DocumentController@printLog', array($document->id)) }}}" target="_blank" class="btn btn-xs btn-success" style="margin-top: 10px">
				<span class="glyphicon glyphicon-print"></span> Cetak Log
			</a>
		</div>
		@foreach ($document->action as $key => $action)
			<div class="disposition-divider {{{ $key == 0 ? 'first-divider' : '' }}}"></div>
			<div class="{{ $action->action_type == 'DOC' ? 'action' : ''}} disposition-detail add-left-margin">
				<div class="disposition-info">
					<ul>
						<li class="border-right">{{{ $action->employee_name }}}</li>
						@if ($action->employee_depname)
							<li class="border-right">{{{ $action->employee_depname }}}</li>
						@endif
						<li>{{{ $action->created_at->format('d F Y H:i') }}}</li>
					</ul>
				</div>
				
				@if ($action->notes)
				<div class="disposition-content">
					{{ $action->notes }}
				</div>
				@endif

				@if ($action->file->first())
					<div class="disposition-attachment">
						<div class="attachment-text">File terlampir :</div>
						<div class="attachment-file">
							@foreach ($action->file as $file)
								<div class="attachment-row">
									<a href="{{{ action('DocumentController@downloadFile', array($file->id)) }}}">
										<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
										{{{ $file->originalname }}} 
									</a>
										@if ($file->type == 'INB')
											(Surat Masuk)
										@elseif ($file->type == 'PRO')
											(Produk)
										@elseif ($file->type == 'OTH')
											(Lain-Lain)
										@else
											({{{ $file->type }}})
										@endif
								</div>
							@endforeach		
						</div>					
					</div>
				@endif
			</div>
		@endforeach
	</div>	
	
	<div class="respond full">			
		<div class="respond-title">Respon</div>
		<form action="{{{ action('DocumentController@addComment', array($document->id)) }}}" method="post">
			<textarea name="comment" style="margin-left: 2%; width: 96%"></textarea>
			
			<div class="respond-option" style="margin-left: 1%">
				<div class="">
					<div class="col-md-6">
						<label>
							<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
							Lampirkan file
						</label>
						<input type="file" id="one_file">
						<div class="box-upload pull-left" id="dropbox">
							<span>Letakkan file di sini</span>
						</div>
					</div>
				</div>
				<div class="document-form">
					<table class="table table-condensed" id="fileListTable" style="margin-top: 5px"></table>
				</div>
				<input type="hidden" name="file" id="inputFile">
			</div>

			<div style="text-align: right">
				<button type="submit" class="btn btn-xs btn-warning" style="margin: 10px" id="btn_submit">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					Simpan
				</button>
			</div>
		</form>
	</div>
@endsection

@section('content.js')
	<script src="/static/js/upload_file.js"></script>
	<script type="x-tmpl-mustache" id="tmplFileListTable">
		<thead>
			<th>File</th>
			<th>Status</th>
			<th>Jenis</th>
			<th></th>
		</thead>
		<tbody>
			{% #data %}
				<tr>
					<td style="word-break: break-all">{% file_name %}</td>
					<th id="state_{% _id %}">{% state %}</th>
					<td>
						{{ Auth::user()->level != JobLevelModel::KEPALA && Auth::user()->level != JobLevelModel::SEKRETARIS ? 'Lain-Lain' : 
							Form::select('type', array(
								'INB' => 'Surat Masuk',
								'PRO' => 'Produk',
								'OTH' => 'Lain-Lain'
							), $document->stage == "PRANS" ? "PRO" : "", array(
								'id' => 'filetype_{% _id %}',
								'onchange' => 'update_type({% _id %})',
								'disabled'
							))
						 }}
					</td>
					<td>
						<button type="button" class="btn btn-danger btn-xs hidden" onclick="remove_file({% _id %})" id="btn_remove_{% _id %}" title="Hapus File">
							<span class="glyphicon glyphicon-remove">
						</button>
					</td>
				</tr>
			{%/data%}
		</tbody>
	</script>
@endsection