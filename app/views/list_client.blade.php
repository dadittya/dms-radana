@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title">
		Daftar Klien
		<div class="pull-right">
			<form method="get" action="{{{ action('ClientController@showList') }}}">
				<div class="form-group search">
					<input type="text" name="q" value="{{{ (isset($q) ? $q : '') }}}" class="form-control" placeholder="Search">
				</div>
			</form>
		</div>
	</div>
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<table class="table table-hover">
				<tr>
					<th>Nama Klien</th>
					<th>Alamat</th>
					<th>Email</th>
					<th>Telepon</th>
				</tr>
				@foreach($clients as $client)
					<tr>
						<td>
							<a href="{{{ action('ClientController@showEditForm', [$client->id]) }}}" title="Ubah" style="color: #333">
								<span class="btn btn-default btn-xs glyphicon glyphicon-pencil"></span>
								<span>{{{ $client->name }}}</span>
							</a>
						</td>
						<td>{{{ $client->address }}}</td>
						<td>{{{ $client->email }}}</td>
						<td>{{{ $client->phone }}}</td>
					</tr>
				@endforeach
			</table>
			<div style="text-align: right">
				{{ $clients->links() }}
			</div>
			<a href="{{{ action('ClientController@showNewForm') }}}" title="Tambah">
				<button class="btn btn-warning btn-sm">Tambah Klien Baru</button>
			</a>
		</div>
	</div>
@endsection