@if ($document->stage == "PRAPP" || $document->stage == "NPAPP")
	<b> {{{ $employee }}} </b> memberi persetujuan
@elseif ($document->stage == "PRDLV" || $document->stage == "NPDLV")
	<b> {{{ $employee }}} </b> mengirim dokumen kepada Kepala SubBagian/Seksi yang bersangkutan
@elseif ($document->stage == "PRASG")
	@if ($ori == "PRDLV")
		<b> {{{ $employee }}} </b> memberikan penugasan kepada staf
	@else
		<b> {{{ $employee }}} </b> menolak pengajuan pelaksanaan isi dokumen
	@endif
@elseif ($document->stage == "PRPRO" || $document->stage == "NPPRO")
	<b> {{{ $employee }}} </b> mengajukan pelaksanaan isi dokumen
@elseif ($document->stage == "PRDAP" || $document->stage == "NPDAP")
	<b> {{{ $employee }}} </b> menyetujui pengajuan pelaksanaan isi dokumen
@elseif ($document->stage == "PRANS" || $document->stage == "NPANS")
	<b> {{{ $employee }}} </b> menyetujui pengajuan pelaksanaan isi dokumen telah diselesaikan
@elseif ($document->stage == "PRFNS")
	<b> {{{ $employee }}} </b> mengirim produk kepada pihak peminta layanan
@elseif ($document->stage == "PRDSC" || $document->stage == "NPDSC")
	<b> {{{ $employee }}} </b> menghentikan proses dokumen
@elseif ($document->stage == "NPASG")
	@if ($ori == "NPDLV")
		<b> {{{ $employee }}} </b> mengirimkan dokumen kepada staf yang bersangkutan
	@else
		<b> {{{ $employee }}} </b> menolak pengajuan pelaksanaan isi dokumen
	@endif
@elseif ($document->stage == "NPFNS")
	@if ($document->target_date)
		<b> {{{ $employee }}} </b> mengirim produk kepada pihak peminta layanan
	@else
		<b> {{{ $employee }}} </b> telah menerima dokumen
	@endif
@endif