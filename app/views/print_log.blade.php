<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DMS</title>

    <link rel="icon" href="/static/img/radana_icon.png" type="image/png" />
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/dw.css" rel="stylesheet">
</head>
<body style="background-color: transparent; min-width: 0px">
	<div class="container-disp" id="container">
		<table class="table content-disp" id="table-top">
			<tbody id="header">
				<tr class="to-center">
					<td class="to-vmiddle" width="190px">
						<img src="/static/img/radana_icon_2.png" style="margin-left: -20px">
					</td>
					<td colspan="3">
						<div style="margin-left: -70px">
							<h4 class="no-margin"><b>
								PT Radana Bhaskara Finance Tbk
						</b></h4>
						<span style="font-size: 10pt"><b>
							CIBIS Nine Building 11th Floor Suite W-16<br>
							Jl. TB Simatupang No. 2 RT.001/RW.005 Kel Cilandak Timur,<br>
							Kec Pasar Minggu Jakarta 12560<br>
							Telp. 021 50991088 Fax. 021 50991089
							</b></span>
						</div>
					</td>
				</tr>
				<tr class="to-center">
					<td colspan="4">
						<b>CATATAN PERUBAHAN LEMBAR DISPOSISI</b>
					</td>
				</tr>
				<tr class="to-center">
					<td colspan="4">
						<b>PERHATIAN : Dilarang memisahkan sehelai surat pun yang tergabung dalam berkas ini</b>
					</td>
				</tr>
				<tr>
					<td>
						No. Agenda
						<span class="pull-right">:</span>
					</td>
					<td width="185px">{{{ $document->agenda_number }}}</td>
					<td rowspan="2" class="merge to-vmiddle">No. KK : </td>
					<td rowspan="2" class="merge to-vmiddle" width="25%">
						<span class="disp-icon to-vmiddle" style="width: 80px">
							@if ($document->is_copy == 1)
								<img src="/static/img/checkbox_unchecked.ico"> 
							@else 
								<img src="/static/img/checkbox_checked.ico"> 
							@endif
							Asli
						</span>
						<span class="disp-icon to-vmiddle">
							@if ($document->is_copy == 0)
								<img src="/static/img/checkbox_unchecked.ico"> 
							@else 
								<img src="/static/img/checkbox_checked.ico"> 
							@endif
							Tembusan
						</span>
					</td>
				</tr>
				<tr>
					<td>
						Tanggal Penerimaan Surat
						<span class="pull-right">:</span>
					</td>
					<td>{{{ $document->received_at->format('d/m/Y') }}}</td>
				</tr>
			</tbody>
		</table>
		@foreach ($document->action as $key => $action)
			<div id="bottom_{{{ $key }}}">
				<table class="table content-log-bottom" id="table-bottom_{{{ $key }}}">
					<tr>
						<td width="50%">
							{{{ $action->employee_name }}}
							{{{ $action->employee_depname ? '(' . $action->employee_depname . ')' : '' }}}
						</td>
						<td>
							Tanggal : {{{ $action->created_at->format('d/m/Y H:i') }}}
						</td>
					</tr>
					<tr class="group" id="notes_{{{ $key }}}">
						<td colspan="2">
							<div style="margin-left: 3mm">
								@if ($action->action_type == 'DOC')
									<i>{{ $action->notes }}</i>
								@else
									{{ $action->notes }}
								@endif
								<ul>
									@foreach ($action->file as $file)
										<li>
											<b>{{{ $file->originalname }}} </b>
											@if ($file->type == 'INB')
												(Surat Masuk)
											@elseif ($file->type == 'PRO')
												(Produk)
											@elseif ($file->type == 'OTH')
												(Lain-Lain)
											@else
												({{{ $file->type }}})
											@endif
										</li>
									@endforeach
								</ul>	
							</div>
						</td>
					</tr>
				</table>
			</div>
		@endforeach
	</div>

<!-- script js -->
<script src="/static/js/jquery.min.js"></script>
<script>
	$(function () {
		var max_height = 1080 - $('#header').height();
		var count_log = {{ count($document->action) }};
		var height_log = 0;

		for (var i = 0; i < count_log; i++) {
			var last_totheight = height_log;
			var last_log = i-1;
			var residue = max_height - last_totheight + $('#table-bottom_' + last_log).height()-60;
			height_log += $('#table-bottom_' + i).height();

			if (i == count_log-1) {
				residue = max_height - last_totheight - 60;
				$('#notes_' + i).height(residue);
			}

			if (height_log > max_height) {
				$('#notes_' + last_log).height(residue);
				$('#table-bottom_' + last_log).attr('style', 'page-break-after: always');
				$('#table-bottom_' + i).addClass('break');
				$('#bottom_' + i).attr('style', 'padding-top: 5mm');
				height_log = $('#table-bottom_' + i).height();
				max_height = 1080;
			}
		}
	});
	window.print();
</script>
</body>
</html>