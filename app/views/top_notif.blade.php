@if (Session::has('UPDATE.OK'))
	@if (Request::is('document/archive'))
		<div class="alert alert-success">
			Arsip Surat berhasil disimpan. 
			<a href="{{{ action('DocumentController@showDetail', array(Session::get('doc_id'))) }}}">Tampilkan.</a>
		</div>
	@else
		<div class="alert alert-success">Berhasil disimpan</div>
	@endif
@elseif (Session::has('DELETE.OK'))
	<div class="alert alert-success">Berhasil dihapus</div>
@elseif (Session::has('UPDATE.FAIL'))
	<div class="alert alert-danger">Tidak berhasil disimpan</div>
@elseif (Session::has('IMAGE.POTRAIT'))
	<div class="alert alert-danger">Gagal disimpan. Gambar harus landscape atau persegi.</div>
@elseif (Session::has('DELETE.FAIL'))
	<div class="alert alert-danger">Tidak berhasil dihapus</div>
@elseif (Session::has('UPDATE.NOT_FOUND') || Session::has('DELETE.NOT_FOUND'))
	<div class="alert alert-danger">Data tidak ditemukan atau telah terhapus</div>
@endif