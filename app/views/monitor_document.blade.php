@extends('master_layout')

@section('css')
	<style type="text/css">
		@media (max-width:1200px){
			.container {
			    width: 970px;
			}
		}
		body{
			min-width: 768px;
		}
	</style>
@endsection

@section('content')
	<div class="content-title">Monitoring Dokumen</div>

	<div class="detail-document">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Nomor Agenda</th>
					<th>Sekretaris</th>
					<th>Kepala Kantor</th>
					<th>Sekretaris</th>
					<th>Kasubag/Kasi</th>
					<th>Pelaksana</th>
					<th>Kasubag/Kasi</th>
					<th>Kepala Kantor</th>
					<th>Sekretaris</th>
					<th>Sisa Waktu</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($documents as $key => $document)
				<tr>
					<td>
						<a href="{{{ action('DocumentController@showDetail', [$document->id]) }}}" title="{{{ $document->about }}}">
							{{{ ($document->agenda_number) ? $document->agenda_number : "-"}}}
						</a><br/>
						{{{ ($document->assignedDept ? $document->assignedDept->name : '') }}}
					</td>
					@for ($i = 0; $i < $document->stage_checked; $i++)
						<td class="text-center success" style="color: #5cb85c">
							<span class="glyphicon glyphicon-ok"></span>
						</td>
					@endfor
					@for ($i = $document->stage_checked; $i < 8; $i++)
						<td></td>
					@endfor
					<td align="center">
						@if (!$document->target_date)
							-
						@elseif ($document->target_date < \Carbon\Carbon::now())
							0 hari
						@else
							{{{ $document->target_date->diffInDays(\Carbon\Carbon::now()) + 1 }}} hari
						@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<div class="text-right">
			{{ $documents->links() }}
		</div>
	</div>
@endsection