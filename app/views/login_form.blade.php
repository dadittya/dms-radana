<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIMOS</title>

    <link rel="icon" href="/static/img/radana_icon.png" type="image/png" />
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/signin.css" rel="stylesheet">
    <link href="/static/css/dw.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
      <div style="text-align: center">
          <img src="/static/img/radana_icon_2.png" height="80px">
          <h4>PT Radana Bhaskara Finance Tbk</h4>
          <h5>CIBIS Nine Building 11th Floor Suite W-16<br>
            Jl. TB Simatupang No. 2 RT.001/RW.005 Kel Cilandak Timur,<br>
            Kec Pasar Minggu Jakarta 12560<br>
            Telp. 021 50991088 Fax. 021 50991089
          </h5>
      </div>
      <form class="form-signin" role="form" method="post" action="{{{ action('AuthController@authLogin') }}}">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus><br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="forget-pass">
          <a href="{{ action('RemindersController@getRemind') }}">
            Lupa Password?
          </a>
        </div>
        <input type="hidden" name="next_url" value="{{{ $next_url }}}">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
      </form>

    </div>

  </body>
</html>
