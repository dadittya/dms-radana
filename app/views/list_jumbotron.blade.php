@extends('master_layout')

@section('content')
	<div class="content-title">Daftar Jumbotron</div>
	<div class="detail-document">
		<div class="detail-document-form">
		@include('top_notif')
			<table class="table table-hover">
				<tr>
					<th>Order</th>
					<th>Title</th>
					<th>Link</th>
					<th>Status</th>
					<th>
						<span class="pull-right"> 
							[Edit]
							[Hapus]
						</span>
					</th>
				</tr>
				@foreach($jumbotrons as $key => $jumbotron)
					<tr>
						<td>
							<div class="col-lg-2">
							<form class="document-form" method="post" action="{{{ action('JumbotronController@moveRow', $jumbotron->id) }}}">
								<button type="submit" class="btn btn-xs" {{{ $key > 0 ? '' : 'disabled' }}}>
									<input type="hidden" name="move" value="-1">
									<span class="glyphicon glyphicon-chevron-up"></span>
								</button>
							</form>
							</div>
							<div class="col-lg-2">
							{{{ $key + 1 }}}
							</div>
							<div class="col-lg-2">
							<form class="document-form" method="post" action="{{{ action('JumbotronController@moveRow', $jumbotron->id) }}}">
								<button type="submit" class="btn btn-xs" {{{ ($key + 1) < $jumbotrons->count() ? '' : 'disabled' }}}>
									<input type="hidden" name="move" value="1">
									<span class="glyphicon glyphicon-chevron-down"></span>
								</button>
							</form>
							</div>
						</td>
						<td>{{{ $jumbotron->title }}}</td>
						<td>{{{ $jumbotron->link }}}</td>
						<td>
							@if ($jumbotron->state == "PUB")
								Publish
							@elseif ($jumbotron->state == "DRF")
								Draft
							@else
								{{{ $jumbotron->state }}}
							@endif
						</td>
						<td>
							
							
							<form action="{{{ action('JumbotronController@deleteRow', [$jumbotron->id]) }}}" method="post" >
								<button class="btn btn-danger btn-xs pull-right" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')" type="submit" title="Hapus">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
							</form>
							<a style="margin-right: 5px" href="{{url('jumbotron/'.$jumbotron->id.'/edit')}}" class="btn btn-primary btn-xs pull-right">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>

						</td>
					</tr>
				@endforeach
			</table>
			<a href="{{{ action('JumbotronController@showNewForm') }}}" title="Tambah">
				<button class="btn btn-warning btn-sm">Tambah Jumbotron Baru</button>
			</a>
		</div>
	</div>
@endsection