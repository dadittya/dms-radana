<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//HOME
Route::get('/', [
	'before' => 'auth',
	'uses' => 'HomeController@showHome'
]);
Route::get('/about', [
	'uses' => 'HomeController@showAbout'
]);

Route::get('/login', 'AuthController@showLoginForm');
Route::post('/login', 'AuthController@authLogin');
Route::post('/logout', 'AuthController@authLogout');

//ACCOUNT
Route::get('/account/password', [
	'before' => 'auth',
	'uses' => 'AccountController@showChangePassword'
]);
Route::post('/account/password', [
	'before' => 'auth',
	'uses' => 'AccountController@doChangePassword'
]);

//REMINDER
Route::get('/account/password/reminder', 'RemindersController@getRemind');
Route::post('/account/password/reminder', 'RemindersController@postRemind');
Route::get('/account/password/reset/{token}', 'RemindersController@getReset');
Route::post('/account/password/reset', 'RemindersController@postReset');

//MEDIA
Route::get('/media/new', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@showNewForm'
]);
Route::post('/media', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@createNew'
]);
Route::get('/media', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@showList'
]);
Route::get('/media/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@showEditForm'
]);
Route::post('/media/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@updateRow'
]);
Route::post('/media/{id}/del', [
	'before' => 'auth.admin',
	'uses' => 'MediaController@deleteRow'
]);

//JUMBOTRON
Route::get('/jumbotron/image/{id}', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@showJumbotronImage'
]);
Route::get('/jumbotron/new', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@showNewForm'
]);
Route::post('/jumbotron', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@createNew'
]);
Route::get('/jumbotron', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@showList'
]);
Route::get('/jumbotron/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@showEditForm'
]);
Route::post('/jumbotron/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@updateRow'
]);
Route::post('/jumbotron/{id}/del', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@deleteRow'
]);
Route::post('/jumbotron/{id}/move', [
	'before' => 'auth.admin',
	'uses' => 'JumbotronController@moveRow'
]);

//DEPARTMENT
Route::get('/department/new', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@showNewForm'
]);
Route::post('/department', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@createNew'
]);
Route::get('/department', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@showList'
]);
Route::get('/department/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@showEditForm'
]);
Route::post('/department/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@updateRow'
]);
Route::post('/department/{id}/del', [
	'before' => 'auth.admin',
	'uses' => 'DepartmentController@deleteRow'
]);

//JOB TITLE
Route::get('/jobtitle/new', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@showNewForm'
]);
Route::post('/jobtitle', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@createNew'
]);
Route::post('/jobtitle/{id}/del', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@deleteRow'
]);
Route::get('/jobtitle', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@showList'
]);
Route::get('/jobtitle/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@showEditForm'
]);
Route::post('/jobtitle/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'JobTitleController@updateRow'
]);

//EMPLOYEE
Route::get('/employee/new', [
	'before' => 'auth.admin',
	'uses' => 'EmployeeController@showNewForm'
]);
Route::post('/employee', [
	'before' => 'auth.admin',
	'uses' => 'EmployeeController@createNew'
]);
Route::get('/employee', [
	'before' => 'auth.admin',
	'uses' => 'EmployeeController@showList'
]);
Route::get('/employee/filter', [
	'before' => 'auth',
	'uses' => 'EmployeeController@filterEmployee'
]);
Route::get('/employee/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'EmployeeController@showEditForm'
]);
Route::post('/employee/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'EmployeeController@updateRow'
]);

//CLIENT
Route::get('/client/new', [
	'before' => 'auth.admin',
	'uses' => 'ClientController@showNewForm'
]);
Route::post('client', [
	'before' => 'auth.admin',
	'uses' => 'ClientController@createNew'
]);
Route::get('/client', [
	'before' => 'auth.admin',
	'uses' => 'ClientController@showList'
]);
Route::get('/client/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'ClientController@showEditForm'
]);
Route::post('client/{id}/edit', [
	'before' => 'auth.admin',
	'uses' => 'ClientController@updateRow'
]);

//DOCUMENT
Route::get('/document', [
	'before' => 'auth',
	'uses' => 'DocumentController@showList'
]);
Route::get('/document/new', [
	'as' => 'create_doc',
	'before' => 'auth.admin',
	'uses' => 'DocumentController@showFormDocument'
]);
Route::get('/document/archive', [
	'as' => 'archive_doc',
	'before' => 'auth.admin',
	'uses' => 'DocumentController@showFormDocument'
]);
Route::post('/document/archive', [
	'as' => 'save_archive',
	'before' => 'auth.admin',
	'uses' => 'DocumentController@updateDocument'
 ]);
Route::get('/document/filter', [
	'before' => 'auth.admin',
	'uses' => 'DocumentController@filterDocument'
]);
Route::get('/document/download/{id}', [
	'before' => 'auth',
	'uses' => 'DocumentController@downloadFile'
]);
Route::get('/document/{id}/edit', [
	'as' => 'edit_doc',
	'before' => 'auth',
	'uses' => 'DocumentController@showFormDocument'
]);
Route::get('/document/{id}', [
	'before' => 'auth',
	'uses' => 'DocumentController@showDetail'
]);
Route::post('/document/upload', [
	'before' => 'auth',
	'uses' => 'DocumentController@uploadFile'
]);
Route::post('/document/remove', [
	'before' => 'auth',
	'uses' => 'DocumentController@removeTempFile'
]);
Route::post('/document/comment/{id}', [
	'before' => 'auth',
	'uses' => 'DocumentController@addComment'
]);
Route::post('/document/{id?}', [
	'as' => 'save_doc',
	'before' => 'auth',
	'uses' => 'DocumentController@updateDocument'
]);
Route::post('/document/{id}/chg_state', [
	'before' => 'auth.login_or_token',
	'uses' => 'DocumentController@changeState'
]);
Route::post('/document/{id}/track', [
	'before' => 'auth',
	'uses' => 'DocumentController@saveUserTrack'
]);
Route::get('document/{id}/print', [
	'before' => 'auth',
	'uses' =>'DocumentController@printDisposition'
]);
Route::get('document/{id}/log', [
	'before' => 'auth',
	'uses' => 'DocumentController@printLog'
]);
Route::get('/monitor', [
	'before' => 'auth.monitoring',
	'uses' => 'DocumentController@showMonitorDocument'
]);


Route::get('/public/document/{id}/{hash}', [
	'uses' => 'DocumentController@showPublic'
]);
Route::get('/public/document/{id}/file/{file_id}/{hash}', [
	'uses' => 'DocumentController@downloadPublicFile'
]);

// TV DISPLAY
Route::get('/display/monitor', [
	'uses' => 'DocumentController@displayMonitor'
]);
Route::get('/display/monitor/{id}', [
	'uses' => 'DocumentController@displayMonitorAjax'
]);
//CS

Route::get('/cs', [
	'before' => 'auth',
	'uses' => 'DocumentController@showCSList'
]);
// API
//  TODO: MOVE TO SEPARATE CONTROLLER?
Route::get('/api/v1/client/list', [
	'before' => 'auth.login_or_token',
	'uses' => 'DocumentController@apiListClient'
]);
/* DASHBOARD API
   JSON REQUEST:
	{
		"_token": "aksljdlkjs token from login"
	}
 */
Route::get('/api/v1/dashboard', [
	'before' => 'auth.token',
	'uses' => 'HomeController@apiDashboard'
]);
Route::get('/api/v1/document', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiDocument'
]);
Route::get('/api/v1/document/{id}', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiDocumentDetail'
]);
Route::get('/api/v1/notification', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiNotification'
]);
Route::get('/api/v1/document/download/{id}/{hash}', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiDownloadFile'
]);

Route::get('/mobile/document/{id}/stage/fail', [
	'uses' => 'DocumentController@mobileStageChangeFail'
]);
Route::get('/mobile/document/{id}/stage/success', [
	'uses' => 'DocumentController@mobileStageChangeSuccess'
]);
/* LOGIN API
   JSON REQUEST:
	{
		"client_id": "samsung-id",
		"client_name": "Samsung Galaxy S6",
		"email": "admin@example.com",
		"password": "admin123"
	}
*/
Route::post('/api/v1/login', [
	'uses' => 'AuthController@apiLogin'
]);
Route::post('/api/v1/logout', [
	'before' => 'auth.token',
	'uses' => 'AuthController@apiLogout'
]);
Route::post('/api/v1/notification/read', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiReadNotification'
]);
Route::post('/api/v1/gcm', [
	'before' => 'auth.token',
	'uses' => 'AuthController@apiUpdateGcm'
]);
Route::post('api/v1/document/{id}/comment', [
	'before' => 'auth.token',
	'uses' => 'DocumentController@apiComment'
]);

// 404
App::missing(function($exception)
{
	return Response::view('.error_404', array(), 404);
});
