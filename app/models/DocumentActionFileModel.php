<?php


class DocumentActionFileModel extends IceDuren\Model\BaseModel  {
	protected $table = 'document_action_file';
	protected $guarded = array('id');
	protected $rules = array(
		'originalname' => 'required|utf8|max:255',
		'filename' => 'required|utf8|max:255|',
		'extension' => 'required|utf8|max:255',
		'size' => 'required|numeric',
		'type' => 'utf8|max:3|in:INB,PRO,OTH'
	);

	public function getFullPath() 
	{	
		$value = $this->filename;
		$first = strtolower(substr($value, 0, 1));

		$path = Config::get('storage.doc_attach');
		
		return "{$path}/{$first}/{$value}";
	}

	public function getPublicKey()
	{
		return hash_hmac("sha256", "DOCUMENTACTIONFILE|{$this->id}|{$this->created_at}", Config::get('app.hmac_key'));
	}

	public function generateHash($token)
	{
		$hash = base64_encode(hash_hmac('sha256', $this->id . '|' . $token, Config::get('app.hmac_key'), true));
		$hash = strtr($hash, ['/' => '_', '+' => '-', '=' => '~']);

		return $hash;
	}
}

