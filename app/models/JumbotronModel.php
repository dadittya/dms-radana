<?php

use Carbon\Carbon;

class JumbotronModel extends IceDuren\Model\BaseModel  {
	protected $table = 'jumbotrons';
	protected $guarded = array('id');
	protected $rules = array(
		'title' => 'required|utf8|max:255',
		'image' => 'required'
	);

	static public function random($extension = null) 
    {
		//$path = Config::get('storage.jumbotron');
		$path = "jumbotron";
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = str_random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

    public function setImageAttribute($value)
	{
		$this->attributes['image'] = null;
		if (!$value->isValid())
			return;

		$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['image'] = $file_path[1];
		$this->attributes['mimetype'] = $mime;
	}

	public function getImageFullPath()
	{
		$value = $this->image;
		if ($value == null)
			return null;

		//$path = Config::get('storage.jumbotron');
		$path = "jumbotron/{$value}";	
		return asset($path);
	}
}
