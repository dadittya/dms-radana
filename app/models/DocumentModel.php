<?php

use \IceDuren\RollbackException;
use League\CommonMark\CommonMarkConverter;
use \Carbon\Carbon;

class DocumentModel extends IceDuren\Model\BaseModel  {
	protected $table = 'documents';
	protected $guarded = array('id', 'created_at', 'updated_at');
	protected $rules = array(
		'received_at' => 'required|date|after_year:2010',
		'document_number' => 'required|utf8|max:255',
		'document_date' => 'required|date|after_year:2010',
		'received_from' => 'utf8|max:255',
		'about' => 'required|utf8|max:262144',
		'enclosure' => 'utf8|max:262144',
		'clasification' => 'required|utf8|max:3|in:SRS,RHS,KFD,BIA',
		'urgency' => 'required|utf8|max:3|in:KLT,SSG,SGR,BIA',
		'target_date' => 'date|date_diff_max:received_at,12,month|after_year:2010',
		'stage' => 'required|utf8|max:5|in:__NEW,PRAPP,PRASG,PRDLV,PRPRO,PRDAP,PRANS,PRFNS,PRDSC,NPAPP,NPDLV,NPASG,NPPRO,NPDAP,NPANS,NPFNS,NPDSC',
		'is_product' => 'required',
		'official_memo' => 'utf8|max:255',
		'memo_date' => 'date|after_year:2010',
		'clients' => 'array_min:1',
	);
	protected $createRules = array(
		'clients' => 'required|array_min:1',
	);
	protected $cache = array();

	protected $dates = array(
		'received_at',
		'document_date',
		'target_date',
		'memo_date'
	);

	public function getStatusCode()
	{
		if(preg_match('#FNS|DSC#i', $this->stage)){
			return "FNS";
		} else {
			return "PRG";
		}
	}

	public function getStatus()
	{
		if(preg_match('#FNS|DSC#i', $this->stage)){
			return "Selesai";
		} else {
			return "Dalam Proses";
		}
	}

	public function getStatusClass()
	{
		if(preg_match('#FNS|DSC#i', $this->stage)){
			return "finished";
		} else {
			return "unfinished";
		}
	}

	public function getFinishDate()
	{
		$act = $this->action()
			->whereIn('stage', ['NPFNS', 'NPDSC', 'PRFNS', 'PRDSC'])
			->orderBy('created_at', 'desc')
			->first();

		if (!$act)
			return null;

		return $act->created_at;
	}

	public function user()
	{
		return $this->belongsToMany('EmployeeModel', 'user_track', 'doc_id', 'emp_id')
			->withTimestamps();
	}

	public function docDisposition()
	{
		return $this->belongsToMany('DepartmentModel', 'docdisposition_department', 'doc_id', 'dept_id')
			->withPivot('notes')
			->withTimestamps();
	}

	public function assigned()
	{
		return $this->belongsTo('EmployeeModel', 'assigned_emp');
	}

	public function assignedDept()
	{
		return $this->belongsTo('DepartmentModel', 'assigned_dep');
	}

	public function substitute()
	{
		return $this->belongsTo('EmployeeModel', 'substitute_emp');
	}

	public function directive()
	{
		return $this->hasMany('DocumentDirectiveModel', 'doc_id');
	}

	public function action()
	{
		return $this->hasMany('DocumentActionModel', 'doc_id');
	}

	public function last_action()
	{
		return $this->action()->orderBy('created_at', 'DESC')->first();
	}

	public function fromClient()
	{
		return $this->belongsTo('ClientModel', 'received_from_id');
	}

	public function toClient()
	{
		return $this->hasMany('DocumentAddressedToModel', 'doc_id');
	}

	public function docCarbonCopy()
	{
		return $this->belongsToMany('EmployeeModel', 'doccarboncopy_employee', 'doc_id', 'emp_id')
			->withTimestamps();
	}

	public function chief()
	{
		return JobLevelModel::find(3)->employee()->where('department', '=', $this->assigned_dep)->take(1)->get()->first();
	}

	public function subchief()
	{
		return JobLevelModel::find(3)->employee()->where('department', '=', $this->substitute->department)->take(1)->get()->first();
	}

	public function getStageCheckedAttribute()
	{
		$stage = substr($this->stage, -3, 3);

		switch ($stage) {
			case 'NEW':
				return 1;
				break;
			case 'APP':
				return 2;
				break;
			case 'DLV':
				return 3;
				break;
			case 'ASG':
				return 4;
				break;
			case 'PRO':
				return 5;
				break;
			case 'DAP':
				return 6;
				break;
			case 'ANS':
				return 7;
				break;
			default:
				return null;
				break;
		}
	}

	public function checkChiefSubsEmp($user)
	{
		$subs = $this->substitute;
		if (!$subs || $user->department != $subs->department)
			return false;

		return true;
	}

	public function setClientsAttribute($value)
	{
		$this->attributes['clients'] = $value;
	}

	public function getCarbonCopyAttribute($value)
	{
		$ret = array();
		foreach ($this->docCarbonCopy as $key => $cc) {
			$emp = EmployeeModel::find($cc->id);
			if (!$emp)
				return true;

			$i = new StdClass();
			$i->id = $emp->id;
			$i->name = $emp->name;
			$i->dept_id = $emp->department;
			$i->department = $emp->dept ? $emp->dept->name : '-';
			$i->_id = strval($key);
			$ret[] = $i;
		}

		return json_encode($ret);
	}

	public function getClientsAttribute()
	{
		$ret = array();
		foreach ($this->toClient as $key => $client) {
			$i = new StdClass();
			$i->id = intval($client->client_id);
			$i->name = $client->name;
			$i->notify_email = intval($client->notify_email);

			if ($i->id == 0)
				$i->label = '(Lain-Lain)';
			$i->_id = strval($key);
			$ret[] = $i;
		}

		return json_encode($ret);
	}

	public function transformAttributes()
	{
		if (!isset($this->attributes['clients']))
			return true;

		$attr = json_decode($this->attributes['clients'], true);

		// force $attr to array
		if (!is_array($attr)) {
			unset($this->attributes['clients']);
			return true;
		}

		$clients = array();
		foreach ($attr as $i) {
			// another check before executing
			if (!isset($i['id']) || !isset($i['name'])) {
				unset($this->attributes['clients']);
				return true;
			}

			$newaddressedto = new DocumentAddressedToModel();
			$newaddressedto->client_id = $i['id'];
			$newaddressedto->name = $i['name'];

			if (isset($i['notify_email'])) {
				$newaddressedto->notify_email = $i['notify_email'];
			}

			$clients[] = $newaddressedto;
		}

		$obj = $this;
		$this->addDelayedTransformAttributes(function() use($obj, $clients) {
			DocumentAddressedToModel::where('doc_id', '=', $obj->id)->delete();

			if (!$obj->toClient()->saveMany($clients))
				throw new RollbackException('failed to insert');

			return true;
		});

		unset($this->attributes['clients']);
		return true;
	}

	public function reference($value = null)
	{
		if ($value == null)
			$value = $this->referenced_doc;

		$doc = DocumentModel::find($value);
		if (!$doc)
			return false;

		return $doc;
	}

	public function defineUrgency($value = null)
	{
		if ($value == null)
			$value = $this->urgency;

		switch ($value) {
			case 'KLT':
				return 'Kilat';
			case 'SSG':
				return 'Sangat Segera';
			case 'SGR':
				return 'Segera';
			case 'BIA':
				return 'Biasa';
			// Unknow Value
			default:
				return $value;
		}
	}

	public function defineClasification($value = null)
	{
		if ($value == null)
			$value = $this->clasification;

		switch ($value) {
			case 'SRS':
				return 'Sangat Rahasia';
			case 'RHS':
				return 'Rahasia';
			case 'KFD':
				return 'Konfidensial';
			case 'BIA':
				return 'Biasa';
			// Unknow Value
			default:
				return $value;
		}
	}

	public function checkHasDispositionChanged($dept, $notes)
	{
		$dispositions = $this->docDisposition()->get();

		if ($dispositions) {
			if (count($dept) != count($dispositions))
				return true;

			foreach ($dispositions as $disposition) {
				foreach($dept as $key => $i)
					if ($i == $disposition->id) {
						if ($notes[$i] != $disposition->pivot->notes)
							return true;

						unset($dept[$key]);
						break;
					} elseif ($i != $disposition->id) {
						return true;
						break;
					}
			}
		}

		return false;
	}

	public function checkHasDirectiveChanged($dir)
	{
		if (!$dir)
			return true;

		$directives = $this->directive()->get();

		if ($directives) {
			if (count($dir) != count($directives))
				return true;

			foreach ($directives as $directive) {
				foreach ($dir as $key => $i)
					if ($i == $directive->directive) {
						unset($dir[$key]);
						break;
					} elseif ($i != $directive->directive)  {
						return true;
						break;
					}
			}
		}

		return false;
	}

	public function findDataChanged($input)
	{
		$old = array();

		foreach ($input as $key => $value) {
			$old_value = $this->$key;
			if ($key == 'received_at' || $key == 'document_date' || $key == 'target_date' || $key == 'memo_date') {
				$old_value = $old_value ? $old_value->format('d/m/Y') : null;
				$value = $value ? $value->format('d/m/Y') : null;
			}
			if ($key == 'received_from'){
				if ($value == '')
					$value = $this->received_from;
			}

			$old_value = e($old_value);
			$value = e($value);

			if ($old_value != $value) {
				switch ($key) {
					case 'agenda_number':
						$i = 'Nomor Agenda';
						break;
					case 'received_at':
						$i = 'Tanggal Masuk';
						break;
					case 'referenced_doc':
						$i = 'Referensi';
						if ($value) {
							$ref = $this->reference($value);
							$new_link = action('DocumentController@showDetail', array($ref->id));
							$value = '<a href="'. $new_link .'" target="_blank">' . $ref->document_number . '</a>';
						}
						if ($this->reference()) {
							$ref = $this->reference();
							$old_link = action('DocumentController@showDetail', array($ref->id));
							$old_value = '<a href="'. $old_link .'" target="_blank">' . $ref->document_number . '</a>';
						}
						break;
					case 'document_number':
						$i = 'Nomor Surat';
						break;
					case 'document_date':
						$i = 'Tanggal Surat';
						break;
					case 'received_from_id':
						if ($value != 0) {
							$i = 'Dari';
							if ($old_value == 0)
								$old_value = $this->received_from;
							else
								$old_value = $this->fromClient->name;
							$value = ClientModel::find($value)->name;
						}
						break;
					case 'received_from':
						if ($input['received_from_id'] == 0) {
							$i = 'Dari';
							if ($this->received_from_id != 0)
								$old_value = ClientModel::find($this->received_from_id)->name;
						}
						break;
					case 'about';
						$i = 'Hal';
						break;
					case 'enclosure':
						$i = 'Lampiran';
						break;
					case 'urgency':
						$i = 'Sifat';
						$old_value = $this->defineUrgency();
						$value = $this->defineUrgency($value);
						break;
					case 'clasification':
						$i = 'Klasifikasi';
						$old_value = $this->defineClasification();
						$value = $this->defineClasification($value);
						break;
					case 'target_date':
						$i = 'Tanggal Selesai';
						break;
					case 'is_product':
						$i = 'Produk';
						if ($value == 0) {
							$old_value = 'Ya';
							$value = 'Tidak';
						} else {
							$old_value = 'Tidak';
							$value = 'Ya';
						}
						break;
					case 'assigned_dep':
						$i = 'Subbagian/Seksi';
						if ($value && $value != 0)
							$value = DepartmentModel::find($value)->name;
						if ($old_value)
							$old_value = DepartmentModel::find($old_value)->name;
						break;
					case 'assigned_emp':
						$i = 'Pelaksana Tugas';
						if ($value && $value != 0)
							$value = EmployeeModel::find($value)->name;
						if ($this->assigned)
							$old_value = $this->assigned->name;
						break;
					case 'is_copy':
						$i = 'Jenis Surat';
						if ($value == 0) {
							$old_value = 'Asli';
							$value = 'Tembusan';
						} else {
							$old_value = 'Tembusan';
							$value = 'Asli';
						}
						break;
					case 'substitute_emp':
						$i = 'Pejabat Pengganti';
						if ($value && $value != 0)
							$value = EmployeeModel::find($value)->name;
						if ($this->substitute)
							$old_value = $this->substitute->name;
						break;
					case 'official_memo':
						$i = 'Nota Dinas';
						break;
					case 'memo_date':
						$i = 'Tanggal Nota Dinas';
						break;
					case 'clients':
						$i = 'Kepada';
						$old_client = json_decode(html_entity_decode($old_value));
						$old_value = null;
						foreach ($old_client as $key => $a) {
							if (count($old_client)-1 == $key)
								$old_value = $old_value . $a->name;
							else
								$old_value = $old_value . $a->name . ', ';
						}
						$new = json_decode(html_entity_decode($value));
						$value = null;
						foreach ($new as $key => $a) {
							if (count($new)-1 == $key)
								$value = $value . $a->name;
							else
								$value = $value . $a->name . ', ';
						}
				}

				if (isset($i))
					$old[] = [
						'key' => $i,
						'old' => $old_value,
						'new' => $value,
					];
			}
		}

		return $old;
	}

	public function saveWithAction($type, $ori, $notes = null,  $filename = null, $chg_disposition = false, $chg_directive = false, $changes = null, $writeable = array(), $carbon = null)
	{
		$obj = $this;

		try {
			DB::transaction(function () use ($obj, $notes, $filename, $ori, $type, $chg_disposition, $chg_directive, $changes, $writeable, $carbon) {
				$doc_action = [];
				$input = array(
					'doc_id' => $obj->id,
					'employee_id' => Auth::user()->id,
					'employee_name' => Auth::user()->name,
					'employee_depname' => Auth::user()->dept ? Auth::user()->dept->name : null,
					'stage' => $obj->stage,
					'action_type' => $type
				);

				if (Auth::user()->dept)
					$employee = e(Auth::user()->name) . " (" . e(Auth::user()->jobtitle->name) . " " . e(Auth::user()->dept->name) . ")";
				else
					$employee = e(Auth::user()->name) . " (" . e(Auth::user()->jobtitle->name) . ")";

				$actfirst = new DocumentActionModel();
				//new doc
				if ($ori == null && $type == 'DOC') {
					$actfirst->notes = "<b>" . e($employee) . "</b>" . " membuat surat baru";
					$actfirst->fill($input);
					if(!$actfirst->save())
						throw new RollbackException('failed to save');

				//edit document
				} elseif ($ori != null && $type == "DOC" && count($changes) > 0) {
					$actfirst->notes = "<b>" . e($employee) . "</b>" . " mengubah data surat : <br>";
					foreach ($changes as $i) {
						if ($i['old'] == null)
							$detail = "'" . e($i['key']) . "' diisi '" . $i['new'] . "'<br>";
						else
							$detail = "'" . e($i['key']) . "' dari '" . $i['old'] . "' menjadi '" . $i['new'] . "'<br>";
						$actfirst->notes .= $detail;
					}

					$actfirst->fill($input);
					if(!$actfirst->save())
						throw new RollbackException('failed to save');

				//comment
				} elseif ($type == "CMT") {
					if (!$notes && count($filename) == 0)
						throw new RollbackException('failed to save');

					if ($notes) {
						$actfirst->mark_down = $notes;
						$converter = new CommonMarkConverter();
						$actfirst->notes = $converter->convertToHtml($notes);
						$actfirst->fill($input);
						if(!$actfirst->save())
							throw new RollbackException('failed to save');
					}

					//file attach
					if(count($filename) > 0) {
						$actfile = new DocumentActionModel();
						$actfile->notes = "<b>" . e($employee) . "</b>" . " melampirkan file.";
						$actfile->fill($input);
						if(!$actfile->save())
							throw new RollbackException('failed to save');

						if (!$actfile->attachFile($filename))
							throw new RollbackException('failed to save');

						$doc_action[] = $actfile->id;
					}
				//change stage
				} elseif ($ori != $obj->stage) {
					$actfirst->notes = View::make('notes_changestage')
						->with('document', $obj)
						->with('employee', $employee)
						->with('ori', $ori)->render();

					$actfirst->fill($input);
					if(!$actfirst->save())
						throw new RollbackException('failed to save');
				}

				if ($actfirst->id)
					$doc_action[] = $actfirst->id;

				//disposition
				if ($chg_disposition && $obj->docDisposition()->first()) {
					$actdisposition = new DocumentActionModel();
					$all = "<b>" . e($employee) . "</b>" . " membuat petunjuk disposisi : <dl>";
					foreach ($obj->docDisposition as $disposition) {
						$disp = "<dt>" . e($disposition->name) . "</dt><dd>" . nl2br(e($disposition->pivot->notes)) . "</dd>";
						$all .= $disp . "<br>";
					}

					$all .= "</dl>";
					$actdisposition->fill($input);
					$actdisposition->notes = $all;
					if (!$actdisposition->save())
						throw new RollbackException('failed to save');

					if ($ori)
						$doc_action[] = $actdisposition->id;
				}

				//directive
				if ($chg_directive && $obj->directive()->first()) {
					$assigned = $obj->assigned;
					$actdirective = new DocumentActionModel();

					if ($obj->assigned) {
						$assigned_dep = e($assigned->dept) ? " (" . e($assigned->dept->name) . ")" : "";
						$dir = "<b>" . e($employee) . "</b> memberi petunjuk kepada <b>" . e($assigned->name) . e($assigned_dep) . "</b> : <ul>";
					}
					else
						$dir = "<b>" . e($employee) . "</b> memberi petunjuk <ul>";

					foreach ($obj->directive as $directive)
						$dir .= "<li>" . e($directive->directive) . "</li>";

					$actdirective->fill($input);
					$actdirective->notes = $dir . "</ul>";
					if (!$actdirective->save())
						throw new RollbackException('failed to save');

					if ($ori)
						$doc_action[] = $actdirective->id;
				}

				//disposition cc
				if (in_array('carboncopy', $writeable) && is_array($carbon) && json_encode($carbon) != $obj->carbonCopy) {
					$doc_cc = json_decode($obj->carbonCopy);
					$disp_employees = array();
					$emp_cc = null;
					foreach ($carbon as $i) {
						$disp_employees[] = $i->id;
					}

					foreach ($disp_employees as $i) {
						$dept = null;
						$emp = EmployeeModel::find($i);
						if (!$emp)
							throw new RollbackException("failed to save");

						if ($emp->dept)
							$dept = " (" . e($emp->dept->name) . ")";

						//notes for adding carbon copy
						$description = null;
						$match = null;
						foreach ($doc_cc as $cc) {
							if ($i == $cc->id) {
								$match = 1;
								break;
							}
						}
						if (!$match){
							if (Auth::user()->level != JobLevelModel::KEPALA && Auth::user()->level != JobLevelModel::SEKRETARIS
								&& $emp->department != Auth::user()->department)
									throw new RollbackException("unauthorized action");

							$description = " <b>ditambahkan</b>";
						}

						$emp_cc .= "<li>" . e($emp->name) . $dept . $description . "</li>";
					}

					//notes for removing carbon copy
					foreach ($doc_cc as $cc) {
						$rm_emp = EmployeeModel::find($cc->id);
						$rm_dept = null;
						if (!in_array($cc->id, $disp_employees)) {
							if (Auth::user()->level != JobLevelModel::KEPALA && Auth::user()->level != JobLevelModel::SEKRETARIS
								&& $rm_emp->department != Auth::user()->department)
									throw new RollbackException("unauthorized action");

							if ($rm_emp->dept)
								$rm_dept = " (" . e($rm_emp->dept->name) . ")";

							$emp_cc .= "<li>" . e($rm_emp->name) . $rm_dept . " <b>dihapus</b>" . "</li>";
						}
					}

					$actcarbon = new DocumentActionModel();
					$actcarbon->fill($input);
					$actcarbon->notes = "<b>" . e($employee) . "</b> mengubah daftar tembusan pejabat disposisi :<ul>" . $emp_cc . "</ul>";

					if (!$obj->docCarbonCopy()->sync($disp_employees))
						throw new RollbackException("failed to save");

					if (!$actcarbon->save())
						throw new RollbackException("failed to save");

					if ($ori)
						$doc_action[] = $actcarbon->id;
				}

				//notes
				if ($type == "DOC" && $notes) {
					$actnotes = new DocumentActionModel();
					$actnotes->fill($input);
					$actnotes->notes = "<b>" . e($employee) . "</b> memberi catatan" . "<br>" . e($notes);
					if (!$actnotes->save())
						throw new RollbackException('failed to save');

					if ($ori)
						$doc_action[] = $actnotes->id;
				}

				//file
				if ($type == "DOC" && count($filename) > 0) {
					$actattach = new DocumentActionModel();
					$actattach->fill($input);
					$actattach->notes = "<b>" . e($employee) . "</b>" . " melampirkan file <br>";
					if (!$actattach->save())
						throw new RollbackException('failed to save');

					if (!$actattach->attachFile($filename))
						throw new RollbackException('failed to save');

					if ($ori)
						$doc_action[] = $actattach->id;
				}

				if (count($doc_action) > 0)
					if (!$obj->saveNotification($doc_action))
						throw new RollbackException('failed to save');
			});

		} catch (RollbackException $e) {
			return false;
		}

		return true;
	}

	public function saveNotification($doc_action)
	{
		$director = JobLevelModel::find(JobLevelModel::KEPALA)->employee()->get();
		$secretary = JobLevelModel::find(JobLevelModel::SEKRETARIS)->employee()->get();
		$super = $director->merge($secretary);
		$list = $this->userNotifList();

		if (($this->stage == 'PRFNS' || $this->stage == 'NPFNS') && Request::is('document/archive'))
			$employees = $super;
		else {
			switch ($this->stage) {
				case '__NEW':
					$employees = $super;
					break;
				case 'PRAPP':
					$employees = $secretary;
					break;
				case 'NPAPP':
					$employees = $secretary;
					break;
				default:
					$employees = $list;
					break;
			}
		}

		foreach($doc_action as $id) {
			foreach ($employees as $emp) {
				$notif = new NotificationModel();
				$notif->employee_id = $emp->id;
				$notif->doc_action_id = $id;

				if (!$notif->save())
					return false;

				//notification
				$emp_token = $notif->employee->clientToken()->get();

				if (!$emp_token)
					continue;

				foreach ($emp_token as $i) {
					if (!$i->gcm_token)
						continue;

					$gcm_token = $i->gcm_token;
					$data = array (
						'to' => $gcm_token,
						'data' => [
							'message' => $notif->action->notes
						]
					);

					Queue::push('GcmNotification', $data);
				}
			}
		}

		return true;
	}

	public function getUpdateRules()
	{
		return $this->getStageChangeRules() + parent::getUpdateRules();
	}

	public function checkStageChangesAuthorization($user)
	{
		$orig = isset($this->original['stage']) ? $this->original['stage'] : '';
		$stage = isset($this->attributes['stage']) ? $this->attributes['stage'] : '';

		if ($orig == $stage)
			return true;

		$changes = $orig . ':' . $stage;

		$auth = [];
		switch ($changes) {
			case ':__NEW':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS,
				];
				break;
			case '__NEW:PRAPP':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRAPP:PRDLV':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			case 'PRDLV:PRASG':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'PRASG:PRPRO':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG,
					JobLevelModel::STAF
				];
				break;
			case 'PRPRO:PRDAP':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'PRPRO:PRASG':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'PRDAP:PRANS':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRDAP:PRASG':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRANS:PRFNS':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			case '__NEW:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRAPP:PRDSC':
			 	$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRDLV:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRASG:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRPRO:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRDAP:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'PRANS:PRDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case '__NEW:NPAPP':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPAPP:NPDLV':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			case 'NPDLV:NPASG':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'NPASG:NPFNS':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG,
					JobLevelModel::STAF
				];
				break;
			case 'NPASG:NPPRO':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG,
					JobLevelModel::STAF
				];
				break;
			case 'NPPRO:NPDAP':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'NPPRO:NPASG':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::KASUBAG
				];
				break;
			case 'NPDAP:NPANS':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPDAP:NPASG':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPANS:NPFNS':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			case '__NEW:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPAPP:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPDLV:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPASG:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPPRO:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPDAP:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case 'NPANS:NPDSC':
				$auth = [
					JobLevelModel::KEPALA
				];
				break;
			case ':PRFNS':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			case ':NPFNS':
				$auth = [
					JobLevelModel::KEPALA,
					JobLevelModel::SEKRETARIS
				];
				break;
			default:
				$auth = [];
		}

		if (!in_array($user->level, $auth))
			return false;

		// HIGH PRIV USER
		if (in_array($user->level, [JobLevelModel::KEPALA, JobLevelModel::SEKRETARIS]))
			return true;

		// IF STAFF CHECK FOR ASSIGNED EMP / SUBTITUTE EMP
		if ($user->level == JobLevelModel::STAF) {
			if ($user->id == $this->assigned_emp)
				return true;
			if ($user->id == $this->substitute_emp)
				return true;
		}

		// IF KASUBAG CHECK FOR SAME DEPT CODE
		if ($user->level == JobLevelModel::KASUBAG) {
			if ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user))
				return true;
		}

		return false;
	}

	public function checkFieldChangesAuthorization($user)
	{
		return true;
	}

	public function checkAuthorization($user)
	{
		if (!$this->checkStageChangesAuthorization($user))
			return false;

		if (!$this->checkFieldChangesAuthorization($user))
			return false;

		return true;
	}

	public function getStageChangeRules()
	{
		if ($this->original['stage'] == $this->attributes['stage'])
			return [];

		$changes = $this->original['stage'] . ':' . $this->attributes['stage'];

		switch ($changes) {
		case '__NEW:PRAPP':
			// NEW => PRAPP
			//  assigned_dep should choosed
			//  is_product should true
			return [
				'assigned_dep' => 'required',
				'is_product' => 'required|is:1,Ya'
			];
			break;
		case 'PRAPP:PRDLV':
			return [
				'agenda_number' => 'required|utf8|max:255|unique:documents,agenda_number,' . $this->id
			];
			break;
		case 'PRDLV:PRASG':
			return [
				'assigned_emp' => 'required'
			];
			break;
		case 'PRASG:PRPRO':
			return [
				'official_memo' => 'required|utf8|max:255',
				'memo_date' => 'required|date'
			];
			break;
		case 'PRPRO:PRASG':
			return [];
			break;
		case 'PRPRO:PRDAP':
			return [];
			break;
		case 'PRDAP:PRANS':
			return [];
			break;
		case 'PRDAP:PRASG':
			return [];
			break;
		case 'PRANS:PRFNS';
			return [];
			break;
		case '__NEW:PRDSC':
			return [];
			break;
		case 'PRAPP:PRDSC':
			return [];
			break;
		case 'PRDLV:PRDSC':
			return [];
			break;
		case 'PRASG:PRDSC':
			return [];
			break;
		case 'PRPRO:PRDSC':
			return [];
			break;
		case 'PRDAP:PRDSC':
			return [];
			break;
		case 'PRANS:PRDSC':
			return [];
			break;
		case '__NEW:NPAPP':
			//  is_product should false
			return [
				'assigned_dep' => 'required',
				'is_product' => 'required|is:0,Tidak'
			];
			break;
		case 'NPAPP:NPDLV':
			return [
				'agenda_number' => 'required|utf8|max:255|unique:documents,agenda_number,' . $this->id
			];
			break;
		case 'NPDLV:NPASG':
			return [
				'assigned_emp' => 'required'
			];
			break;
		case 'NPASG:NPFNS':
			if ($this->target_date)
				throw new InvalidStageException("Invalid Stage Changes {$changes}");

			return [];
			break;
		case 'NPASG:NPPRO':
			return [
				'official_memo' => 'required|utf8|max:255',
				'memo_date' => 'required|date'
			];
			break;
		case 'NPPRO:NPASG':
			return [];
			break;
		case 'NPPRO:NPDAP':
			return [];
			break;
		case 'NPDAP:NPANS':
			return [];
			break;
		case 'NPDAP:NPASG':
			return [];
			break;
		case 'NPANS:NPFNS':
			return [];
			break;
		case '__NEW:NPDSC':
			return [];
			break;
		case 'NPAPP:NPDSC':
			return [];
			break;
		case 'NPDLV:NPDSC':
			return [];
			break;
		case 'NPASG:NPDSC':
			return [];
			break;
		case 'NPPRO:NPDSC':
			return [];
			break;
		case 'NPDAP:NPDSC':
			return [];
			break;
		case 'NPANS:NPDSC':
			return [];
			break;
		default:
			throw new InvalidStageException("Invalid Stage Changes {$changes}");
		}
	}

	public function userNotifList()
	{
		$list = array();

		//director
		$director = JobLevelModel::find(1)->employee()
			->where('active', '=', '1')
			->where('email_notif', '=', '1')
			->get();
		foreach ($director as $it) {
			$list[$it->id] = $it;
		}

		//secretary
		$secretary = JobLevelModel::find(2)->employee()
			->where('active', '=', '1')
			->where('email_notif', '=', '1')
			->get();
		foreach ($secretary as $it) {
			$list[$it->id] = $it;
		}

		//disposition
		foreach ($this->docDisposition as $i) {
			$disp = JobLevelModel::find(3)->employee()
				->where('department', '=', $i->id)
				->where('active', '=', '1')
				->where('email_notif', '=', '1')
				->get();
			foreach ($disp as $it) {
				$list[$it->id] = $it;
			}
		}

		//assignment
		$asg_emp = $this->assigned;
		if ($asg_emp && $asg_emp->active)
			$list[$asg_emp->id] = $asg_emp;

		if ($this->assigned_dep) {
			$chief = JobLevelModel::find(3)->employee()
				->where('department', '=', $this->assigned_dep)
				->where('active', '=', '1')
				->where('email_notif', '=', '1')
				->get();
			foreach ($chief as $it) {
				$list[$it->id] = $it;
			}
		}

		//substitute
		$subs_emp = $this->substitute;
		if ($subs_emp) {
			if ($subs_emp->active && $subs_emp->email_notif)
				$list[$subs_emp->id] = $subs_emp;

			if ($subs_emp->dept) {
				$chief = JobLevelModel::find(3)->employee()
					->where('department', '=', $subs_emp->departement)
					->where('active', '=', '1')
					->where('email_notif', '=', '1')
					->get();
				foreach ($chief as $it) {
					$list[$it->id] = $it;
				}
			}
		}

		//disposition staf
		foreach ($this->docCarbonCopy as $i) {
			 $emp = EmployeeModel::find($i->id);
			 if ($emp->active && $emp->email_notif)
			 	$list[$i->id] = $emp;
		}

		return $list;
	}

	public function renderActionBar()
	{
		$user = Auth::user();
		return $this->renderActionBarForUser($user->id);
	}

	public function renderActionBarWithToken($_token)
	{
		$user = Auth::user();
		return $this->renderActionBarForUser($user->id, $_token);
	}

	public function renderActionBarForUser($user_id, $_token = null)
	{
		$user = EmployeeModel::find($user_id);
		$joblevel = $user->level;

		switch ($this->stage) {
			//create new document
			case '__NEW':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.___new_director');
				else
					$view = View::make('actionbar.___new_all');
				break;
			//get approved from director
			case 'PRAPP':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_app_director');
				elseif ($joblevel == JobLevelModel::SEKRETARIS)
					$view = View::make('actionbar.pr_app_secretary');
				else
					$view = View::make('actionbar.pr_app_all');
				break;
			//already sent to other related user
			case 'PRDLV':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_dlv_director');
				elseif ($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user)))
					$view = View::make('actionbar.pr_dlv_chief');
				else
					$view = View::make('actionbar.pr_dlv_all');
				break;
			//chief assigned task to staf
			case 'PRASG':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_asg_director');
				elseif (($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user))) ||
						($joblevel == JobLevelModel::STAF && ($user_id == $this->assigned_emp || $user_id == $this->substitute_emp)))
					$view = View::make('actionbar.pr_asg_assigned');
				else
					$view = View::make('actionbar.pr_asg_all');
				break;
			//staf propose to chief
			case 'PRPRO':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_pro_director');
				elseif ($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user)))
					$view = View::make('actionbar.pr_pro_chief');
				else
					$view = View::make('actionbar.pr_pro_all');
				break;
			//the proposed approved by chief
			case 'PRDAP':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_dap_director');
				else
					$view = View::make('actionbar.pr_dap_all');
				break;
			//document get the answered and done
			case 'PRANS':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_ans_director');
				elseif ($joblevel == JobLevelModel::SEKRETARIS)
					$view = View::make('actionbar.pr_ans_secretary');
				else
					$view = View::make('actionbar.pr_ans_all');
				break;
			case 'PRFNS':
				$view = View::make('actionbar.pr_fns_all');
				break;
			case 'PRDSC':
				$view = View::make('actionbar.pr_dsc_all');
				break;
			case 'NPAPP':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.np_app_director');
				elseif ($joblevel == JobLevelModel::SEKRETARIS)
					$view = View::make('actionbar.np_app_secretary');
				else
					$view = View::make('actionbar.np_app_all');
				break;
			case 'NPDLV':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.np_dlv_director')->with('target_date', $this->target_date);
				elseif ($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user)))
					$view = View::make('actionbar.np_dlv_chief')->with('target_date', $this->target_date);
				else
					$view = View::make('actionbar.np_dlv_all');
				break;
			case 'NPASG':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.np_asg_director')->with('target_date', $this->target_date);
				elseif (($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user))) ||
						($joblevel == JobLevelModel::STAF && ($user_id == $this->assigned_emp || $user_id == $this->substitute_emp)))
					$view = View::make('actionbar.np_asg_assigned')->with('target_date', $this->target_date);
				else
					$view = View::make('actionbar.np_asg_all')->with('target_date', $this->target_date);
				break;
			//staf propose to chief
			case 'NPPRO':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_pro_director')->with('non_product', true);
				elseif ($joblevel == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user)))
					$view = View::make('actionbar.pr_pro_chief')->with('non_product', true);
				else
					$view = View::make('actionbar.pr_pro_all')->with('non_product', true);
				break;
			//the proposed approved by chief
			case 'NPDAP':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_dap_director')->with('non_product', true);
				else
					$view = View::make('actionbar.pr_dap_all')->with('non_product', true);
				break;
			//document get the answered and done
			case 'NPANS':
				if ($joblevel == JobLevelModel::KEPALA)
					$view = View::make('actionbar.pr_ans_director')->with('non_product', true);
				elseif ($joblevel == JobLevelModel::SEKRETARIS)
					$view = View::make('actionbar.pr_ans_secretary')->with('non_product', true);
				else
					$view = View::make('actionbar.pr_ans_all')->with('non_product', true);
				break;
			case 'NPFNS':
				$view = View::make('actionbar.np_fns_all');
				break;
			case 'NPDSC':
				$view = View::make('actionbar.np_dsc_all');
				break;
			default:
				return null;
		}

		if ($_token) {
			$view = $view->with('_token', $_token);
		}

		return $view
			->with('id', $this->id)
			->with('document', $this)
			->render();
	}

	static function sortByPercentage($documents)
	{
		if ($documents instanceof Traversable) {
			$documents = iterator_to_array($documents);
		}
		usort($documents, function($a, $b) {
			if (!$a->target_date && !$b->target_date)
				return 0;

			if (!$a->target_date && $b->target_date)
				return 1;

			if ($a->target_date && !$b->target_date)
				return -1;

			if ($a->target_date < $b->target_date)
				return -1;

			if ($a->target_date > $b->target_date)
				return 1;

			$a_percent = $a->calcPercentageDeadline();
			$b_percent = $b->calcPercentageDeadline();

			if ($a_percent == $b_percent) {
				return 0;
			}

			return ($a_percent < $b_percent) ? -1 : 1;
		});

		return $documents;
	}

	protected function _calcPercentageDeadline()
	{
		$target = $this->target_date->copy()->subDay(1);

		// target is passed already, alert!
		if ($target <= Carbon::now())
			return 0;

		// calculate from receive at
		$create = $this->received_at;
		$closure = function(Carbon $date) {
		   	return !$date->isWeekend();
		};

		$diff = $create->diffInDaysFiltered($closure, $target);

		$remain = Carbon::now()->diffInDaysFiltered($closure, $target);

		if ($diff <= 0)
			return 0;

		$percentage = 100*$remain/$diff;
		if ($percentage > 100)
			$percentage = 100;

		return floor($percentage);
	}

	public function calcPercentageDeadline()
	{
		if (isset($this->cache['percentDeadline']) &&
			$this->target_date == $this->cache['percentDeadline']['target_date'] &&
			$this->received_at == $this->cache['percentDeadline']['received_at'])
			return $this->cache['percentDeadline']['value'];

		if (!$this->target_date || $this->is_done == 1)
			return 100;

		$this->cache['percentDeadline'] = [
			'target_date' => $this->target_date->copy(),
			'received_at' => $this->received_at->copy(),
			'value' => $this->_calcPercentageDeadline()
		];

		return $this->cache['percentDeadline']['value'];
	}

	public function defineBgColor()
	{
		$percentage = $this->calcPercentageDeadline();

		if ($percentage <= 25)
			return "#FFCDD2";
		elseif ($percentage <= 30)
			return "#FFE0B2";
		elseif ($percentage <= 50)
			return "#FFF9C4";
		else {
			if ($this->is_product == 0)
				return "#B3E5FC";
			else
				return "#C5E1A5";
		}
	}

	public function getStageDescAttribute()
	{
		switch ($this->stage) {
			case '__NEW':
				return "Menunggu persetujuan dari Kepala Kantor";
				break;
			case 'PRAPP':
				return "Dokumen telah disetujui Kepala Kantor";
				break;
			case 'PRDLV':
				return "Dokumen telah dikirimkan ke Kepala SubBagian/Seksi";
				break;
			case 'PRASG':
				return "Menunggu pelaksanaan dokumen oleh staf";
				break;
			case 'PRPRO':
				return "Menunggu persetujuan dari Kepala SubBagian/Seksi";
				break;
			case 'PRDAP':
				return "Pelaksanaan dokumen telah disetujui oleh Kepala SubBagian/Seksi";
				break;
			case 'PRANS':
				return "Pelaksanaan dokumen telah disetujui oleh Kepala Kantor";
				break;
			case 'PRFNS':
				return "Pelaksanaan dokumen telah selesai";
				break;
			case 'PRDSC':
				return "Proses dokumen dihentikan";
				break;
			case 'NPAPP':
				return "Dokumen telah disetujui Kepala Kantor";
				break;
			case 'NPDLV':
				return "Dokumen telah dikirimkan ke Kepala SubBagian/Seksi";
				break;
			case 'NPASG':
				if ($this->target_date)
					return "Menunggu pelaksanaan dokumen oleh staf";
				else
					return "Menunggu dokumen diterima oleh staf";

				break;
			case 'NPPRO':
				return "Menunggu persetujuan dari Kepala SubBagian/Seksi";
				break;
			case 'NPDAP':
				return "Pelaksanaan dokumen telah disetujui oleh Kepala SubBagian/Seksi";
				break;
			case 'NPANS':
				return "Pelaksanaan dokumen telah disetujui oleh Kepala Kantor";
				break;
			case 'NPFNS':
				if ($this->target_date)
					return "Proses dokumen telah selesai";
				else
					return "Dokumen telah diterima oleh staf";

				break;
			case 'NPDSC':
				return "Proses dokumen dihentikan";
				break;
			default:
				return "";
		}
	}

	public function writeableField($user)
	{
		$field = [
			'agenda_number',
			'received_at',
			'document_number',
			'is_copy',
			'document_date',
			'received_from',
			'received_from_id',
			'about',
			'enclosure',
			'urgency',
			'clasification',
			'referenced_doc',
			'target_date',
			'official_memo',
			'memo_date',
			'clients',
			'dispositions',
			'directives',
			'carboncopy',
		];

		if ($user->level == JobLevelModel::STAF && ($user->id == $this->assigned_emp || $user->id == $this->substitute_emp)) {
			switch ($this->stage) {
				case 'PRASG':
					return [
						'official_memo',
						'memo_date'
					];
					break;
				case 'NPASG':
					return [
						'official_memo',
						'memo_date'
					];
					break;
				default:
					return [];
					break;
			}
		} elseif ($user->level == JobLevelModel::KASUBAG && ($user->department == $this->assigned_dep || $this->checkChiefSubsEmp($user))) {
			switch ($this->stage) {
				case 'PRDLV':
					return [
						'assigned_emp',
						'substitute_emp',
						'official_memo',
						'memo_date'
					];
					break;
				case 'PRASG':
					return [
						'assigned_emp',
						'substitute_emp',
						'official_memo',
						'memo_date'
					];
					break;
				case 'NPDLV':
					return [
						'assigned_emp',
						'substitute_emp',
						'official_memo',
						'memo_date'
					];
					break;
				case 'NPASG':
					return [
						'assigned_emp',
						'substitute_emp',
						'official_memo',
						'memo_date'
					];
					break;
				default:
					return [];
					break;
			}
		} elseif ($user->level == JobLevelModel::KASUBAG && $user->department != $this->assigned_dep && !$this->checkChiefSubsEmp($user)) {
			if ($this->is_done)
				return [];

			foreach ($this->docDisposition as $i)
				if ($user->department == $i->id)
					return ['carboncopy'];
			return [];

		} elseif ($user->level == JobLevelModel::KEPALA || $user->level == JobLevelModel::SEKRETARIS) {
			switch ($this->stage) {
				case '__NEW':
					array_push ($field,
						'assigned_dep',
						'assigned_emp',
						'substitute_emp',
						'is_product'
					);
					return $field;
					break;
				case 'PRAPP':
					array_push ($field,
						'assigned_dep',
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'PRDLV':
					array_push ($field,
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'PRASG':
					array_push ($field,
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'PRPRO':
					return $field;
					break;
				case 'PRDAP':
					return $field;
					break;
				case 'PRANS':
					return $field;
					break;
				case 'NPAPP':
					array_push ($field,
						'assigned_dep',
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'NPDLV':
					array_push ($field,
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'NPASG':
					array_push ($field,
						'assigned_emp',
						'substitute_emp'
					);
					return $field;
					break;
				case 'PRPRO':
					return $field;
					break;
				case 'PRDAP':
					return $field;
					break;
				case 'PRANS':
					return $field;
					break;
				default:
					array_push ($field,
						'assigned_dep',
						'assigned_emp',
						'substitute_emp',
						'is_product'
					);
					return $field;
					break;
			}
		} else {
			return [];
		}
	}

	static function doSortBy($sort_by)
	{
		$documents = DocumentModel::select(DB::raw('*'))
			 ->from(DB::raw('(SELECT documents.*, document_action.created_at as last_action FROM documents JOIN document_action ON document_action.doc_id = documents.id ORDER BY document_action.created_at DESC) as documents'));

		foreach ($sort_by as $i) {
			$documents = $documents->orderBy($i->field, $i->sort);
		}

		return $documents;
	}

	public function getPublicKey()
	{
		return hash_hmac("sha256", "DOCUMENT|{$this->id}|{$this->created_at}", Config::get('app.hmac_key'));
	}

	public function send_notif()
	{
		$doc = $this;
		switch ($this->stage) {
		case 'PRASG':
			foreach ($this->toClient as $client) {
				if (!$client->notify_email)
					continue;

				if (!$client->id)
					continue;

				$c = $client->client;

				if (strlen($c->email) == 0)
					continue;

				Mail::send('emails.notif.client_notif_prasg', ['document' => $this], function($message) use ($doc, $c)
				{
					$message->to($c->email, $c->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Diterima");
				});
			}

			break;
		case 'PRFNS':
			foreach ($this->toClient as $client) {
				if (!$client->notify_email)
					continue;

				if (!$client->id)
					continue;

				$c = $client->client;

				if (strlen($c->email) == 0)
					continue;

				Mail::send('emails.notif.client_notif_prfns', ['document' => $this], function($message) use ($doc, $c)
				{
					$message->to($c->email, $c->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Selesai");
				});
			}

			break;
		}
	}

	public function send_notif_intern($ori = null)
	{
		$list = $this->userNotifList();
		$doc = $this;

		//SUPER USER
		$director = JobLevelModel::find(JobLevelModel::KEPALA)->employee()->get();
		$secretary = JobLevelModel::find(JobLevelModel::SEKRETARIS)->employee()->get();
		$super = $director->merge($secretary);

		switch ($doc->stage) {
			case '__NEW':
				foreach ($super as $e) {
					Mail::send('emails.notif.kepalakantor_notif_new', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Baru Dibuat");
					});
				}
				break;
			case 'PRAPP':
				foreach ($secretary as $e) {
					Mail::send('emails.notif.sekretaris_notif_app', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Disetujui");
					});
				}
				break;
			case 'PRDLV':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prdlv', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Dikirimkan");
					});
				}
				break;
			case 'PRASG':
				foreach ($list as $e) {
					if ($ori == 'PRPRO')
						Mail::send('emails.notif.all_notif_reject_chief', ['document' => $this], function($message) use ($doc, $e)
						{
							$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Tidak Disetujui");
						});
					elseif ($ori == 'PRDAP')
						Mail::send('emails.notif.all_notif_reject_director', ['document' => $this], function($message) use ($doc, $e)
						{
							$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Tidak Disetujui");
						});
					else
						Mail::send('emails.notif.all_notif_prasg', ['document' => $this], function($message) use ($doc, $e)
						{
							$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Ditugaskan");
						});
				}
				break;
			case 'PRPRO':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prpro', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Diajukan");
					});
				}
				break;
			case 'PRDAP':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prdap', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Disetujui");
					});
				}
				break;
			case 'PRANS':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prans', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Disetujui");
					});
				}
				break;
			case 'PRFNS':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prfns', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Telah Selesai");
					});
				}
				break;
			case 'NPAPP':
				foreach ($secretary as $e) {
					Mail::send('emails.notif.sekretaris_notif_app', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Disetujui");
					});
				}
				break;
			case 'NPDLV':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_npdlv', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Dikirimkan");
					});
				}
				break;
			case 'NPASG':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_npasg', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Dikirimkan Kepada Pejabat");
					});
				}
				break;
			case 'NPPRO':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prpro', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Diajukan");
					});
				}
				break;
			case 'NPDAP':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prdap', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Disetujui");
					});
				}
				break;
			case 'NPANS':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_prans', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Produk Disetujui");
					});
				}
				break;
			case 'NPFNS':
				foreach ($list as $e) {
					Mail::send('emails.notif.all_notif_npfns', ['document' => $this], function($message) use ($doc, $e)
					{
						$message->to($e->email, $e->name)->subject("[SIMOS] Surat No.{$doc->document_number} Telah Selesai");
					});
				}
				break;
		}
	}
}
