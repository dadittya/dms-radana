<?php

class JobLevelModel extends IceDuren\Model\BaseModel  {
	protected $table = 'job_level';
	protected $guarded = array('id');

	const KEPALA = 1;
	const SEKRETARIS = 2;
	const KASUBAG = 3;
	const STAF = 4;
	const ENGINEER = 5;
	const AUDITOR = 6;

	public function employee()
	{
		return $this->hasManyThrough('EmployeeModel', 'JobTitleModel', 'level', 'job_title');
	}
}