<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class EmployeeModel extends IceDuren\Model\BaseModel implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employees';
    protected $guarded = array('id');
    protected $rules = array(
        'name' => 'required|utf8|max:255',
        'email' => 'required|email',
        'password' => 'required|min:6',
        'nip' => 'utf8',
        'job_title' => 'required',
        'email_notif' => 'numeric',
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public function jobtitle()
    {
        return $this->belongsTo('JobTitleModel', 'job_title');
    }

    public function dept()
    {
        return $this->belongsTo('DepartmentModel', 'department');
    }

    public function getLevelAttribute()
    {
        return $this->jobtitle->level;
    }

    public function action()
    {
        return $this->hasMany('DocumentActionModel', 'employee_id');
    }

    public function checkPassword($password)
    {
        return Hash::check($password, $this->getAttribute('password'));
    }

    public function clientToken()
    {
        return $this->hasMany('EmployeeTokenModel', 'employee_id');
    }

    public function notif()
    {
        return $this->hasMany('NotificationModel', 'employee_id');
    }
}
