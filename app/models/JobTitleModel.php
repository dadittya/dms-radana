<?php

class JobTitleModel extends IceDuren\Model\BaseModel  {
	protected $table = 'job_title';
	protected $guarded = array('id');
	protected $rules = array(
		'name' => 'required|utf8|max:255'
	);

	public function employee()
	{
		return $this->hasMany('EmployeeModel', 'job_title');
	}

	public function joblevel()
	{
		return $this->belongsTo('JobLevelModel', 'level');
	}
}

