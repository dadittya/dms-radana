<?php

class ClientModel extends IceDuren\Model\BaseModel  {
	protected $table = 'clients';
	protected $guarded = array('id');
	protected $rules = array(
		'name' => 'required|utf8|max:255',
		'address' => 'utf8|max:255',
		'email' => 'email',
		'phone' => 'utf8|max:255'
	);

	public function setEmailAttribute($value)
	{
		if (strlen($value) == 0)
			$this->attributes['email'] = null;
		else
			$this->attributes['email'] = $value;
	}

	public function setAddressAttribute($value)
	{
		if (strlen($value) == 0)
			$this->attributes['address'] = null;
		else
			$this->attributes['address'] = $value;
	}

	public function setPhoneAttribute($value)
	{
		if (strlen($value) == 0)
			$this->attributes['phone'] = null;
		else
			$this->attributes['phone'] = $value;
	}
}