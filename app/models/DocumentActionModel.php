<?php


class DocumentActionModel extends IceDuren\Model\BaseModel  {
	protected $table = 'document_action';
	protected $guarded = array('id');
	protected $rules = array(
		'notes' => 'utf8|max:262144',
		'stage' => 'required|utf8|max:5',
		'action_type' => 'required|utf8|max:3|in:DOC,CMT'
	);

	public function employee()
	{
		return $this->belongsTo('EmployeeModel', 'employee_id');
	}

	public function document()
	{
		return $this->belongsTo('DocumentModel', 'doc_id');
	}

	public function file()
	{
		return $this->hasMany('DocumentActionFileModel', 'docaction_id');
	}

	static protected function cleanTempFile()
	{
		$path = Config::get('storage.temp');

		if ($handle = opendir($path)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry[0] == '.')
					continue;

				//separate file name from its extension
				$file = explode(".", $entry);
				if (!ctype_alnum($file[0]))
					continue;
				
				$full_path = $path . '/' . $entry;
				if (!is_file($full_path)) 
					continue;
				
				// delete file created 2 weeks before
				if (time() - filectime($full_path) > 2 * 7 * 24 * 60 * 60)
					@unlink($full_path);
			}
			closedir($handle);
		}
	}

	public function attachFile($filename)
	{
		// Try to clean up before attach file
		static::cleanTempFile();
		
		$files = array();
		foreach ($filename as $i) {
			$first = strtolower(substr($i->temp, 0, 1));
			$file = Config::get('storage.temp') . '/' . $i->temp;

			if (file_exists($file)) {
				copy($file, Config::get('storage.doc_attach') . '/' . $first . "/" . $i->temp);
				@unlink($file);

				$actfile = new DocumentActionFileModel();
				$actfile->originalname = $i->name;
				$actfile->filename = $i->temp;
				$actfile->extension = $i->ext;
				$actfile->size = $i->size;
				$actfile->type = $i->type == 'undefined' ? 'OTH' : $i->type;
				$files[] = $actfile;

			} else {
				return false;
			}
		}

		if (!$this->file()->saveMany($files))
			return false;

		return true;
	}
}

