<?php

use Carbon\Carbon;

class DepartmentModel extends IceDuren\Model\BaseModel  {
	protected $table = 'departments';
	protected $guarded = array('id');
	protected $rules = array(
		'name' => 'required|utf8|max:255'
	);

	public function employee()
	{
		return $this->hasMany('EmployeeModel', 'department');
	}

	public function document()
	{
		return $this->hasMany('DocumentModel', 'assigned_dep');
	}

	public function docDisposition()
	{
		return $this->belongsToMany('DocumentModel', 'docdisposition_department', 'dept_id', 'doc_id')
			->withPivot('notes')
			->withTimestamps();
	}

	public function getDisplayDocumentList()
	{
		$carbon = Carbon::now();
		$carbon->subMonth();
		$carbon->day = 1;
		$carbon->hour = 0;
		$carbon->minute = 0;
		$carbon->second = 0;
		$id = $this->id;
		$doc = DocumentModel::where('received_at', '>=', $carbon)
			->where('assigned_dep','=',$id)
			->where('is_product', '=', 1);
		return $doc->get();
	}
}
