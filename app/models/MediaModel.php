<?php

use Carbon\Carbon;

class MediaModel extends IceDuren\Model\BaseModel  {
	protected $table = 'media';
	protected $guarded = array('id');

	protected $dates = ['upload_at'];

	public function getMediaFullPath()
	{
		$value = $this->filename;
		$upload_at = $this->attributes['upload_at'];
		$year = Carbon::createFromFormat('Y-m-d H:i:s', $upload_at)->year;

		if ($value == null)
			return null;

		//$path = "media/{$year}/{$value}";
		$path = "media/{$year}/{$value}";
		return asset($path);
	}
}