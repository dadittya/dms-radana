<?php

class DocumentAddressedToModel extends IceDuren\Model\BaseModel  {
	protected $table = 'document_addressedto';
	protected $guarded = array('id');
	protected $rules = array(
		'name' => 'required|utf8|max:255',
		'email' => 'email',
	);

	public function client()
	{
		return $this->belongsTo('ClientModel', 'client_id');
	}
}