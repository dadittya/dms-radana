<?php

class NotificationModel extends IceDuren\Model\BaseModel  {
	protected $table = 'notifications';
	protected $guarded = array('id');
	protected $rules = array(
		'employee_id' => 'required',
		'doc_action_id' => 'required'
	);

	public function action()
	{
		return $this->belongsTo('DocumentActionModel', 'doc_action_id');
	}

	public function employee()
	{
		return $this->belongsTo('EmployeeModel', 'employee_id');
	}
}