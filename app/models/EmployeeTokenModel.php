<?php

class EmployeeTokenModel extends IceDuren\Model\BaseModel  {
	protected $table = 'employee_tokens';
	protected $guarded = array('id');

	public function employee()
	{
		return $this->belongsTo('EmployeeModel', 'employee_id');
	}
}

