<?php


class DocumentDirectiveModel extends IceDuren\Model\BaseModel  {
	protected $table = 'document_directive';
	protected $guarded = array('id');
	protected $rules = array(
		'directive' => 'required|utf8|max:255',
		'notes' => 'utf8|max:255'
	);
}

