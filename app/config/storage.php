<?php

return array(
	'doc_attach' => storage_path() . '/files/doc_attach',
	
	'temp' => storage_path() . '/files/temp',

	'media' => storage_path() . '/media',
);