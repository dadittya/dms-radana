<?php namespace IceDuren;

use \View;
use \Config;
use \Swift_Message;

class Mail {
	public $subject;
	public $body;
	public $type = 'text/plain';

	public static function make($view, $data)
	{
		$mail = new Mail();

		// override _mail object
		$data['_mail'] = $mail;
		// render
		$mail->body = View::make($view, $data)->render();

		return $mail;
	}

	// api for template rendering
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	public function send($to)
	{
		if (Config::get('mail.pretend'))
			return true;

		$from = Config::get('mail.from');
		$replyto = Config::get('mail.replyto');
		$subject = $this->subject;

		// if mail testing
		if (Config::get('mail.testing')) {
			$oldto = $to;
			$to = Config::get('mail.testing');
			$subject .= ' (' . $oldto . ')';
		}

		$type = $this->type;
		$body = $this->body;

		// clean up new line
		if ($type == 'text/plain') {
			$body = strtr($body, "\r\n", "\n");
			$body = strtr($body, "\n", "\r\n");
		}

		$message = new Swift_Message;
		$message->setFrom(array($from['address'] => $from['name']))
			->setReplyTo(array($replyto['address'] => $replyto['name']))
			->setTo($to)
			->setSubject($subject)
			->setBody($body, $type);

		$result = \Mail::getSwiftMailer()->send($message);
		
		return $result;
	}

	public function sendAdmin()
	{
		return $this->send(Config::get('mail.notification_to'));
	}
}
