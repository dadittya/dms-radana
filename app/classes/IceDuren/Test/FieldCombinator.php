<?php namespace IceDuren\Test;

class FieldCombinator {
	protected $goodField = [];
	protected $badField = [];
	protected $fieldMap = [];
	protected $fieldCount = 0;
	protected $counter = [];

	public function start()
	{
		$this->fieldMap = array_unique(array_keys($this->goodField) + array_keys($this->badField));
		sort($this->fieldMap);
		$this->fieldCount = count($this->fieldMap);
		$this->counter = array_fill(0, $this->fieldCount, 0);
	}

	public function next()
	{
		// check last position for combination
		if ($this->counter[$this->fieldCount - 1] >= count($this->getBadField($this->fieldCount - 1)))
			return null;

		$ret = array();
		foreach ($this->counter as $key => $val)
		{
			$ret[$this->fieldMap[$key]] = $this->getCombination($key, $val);
		}

		foreach ($this->counter as $key => &$val)
		{
			$val++;

			if ($val > count($this->getBadField($key)))
				$val = 0;
			else
				break;
		}

		return $ret;
	}

	protected function getBadField($position)
	{
		return $this->badField[$this->fieldMap[$position]];
	}

	protected function getCombination($position, $counter)
	{
		$bad = $this->getBadField($position);

		$n = count($bad);
		if ($counter > $n)
			throw new Exception('Counter more than bad field possibilities');

		if ($counter == $n)
			return $this->goodField[$this->fieldMap[$position]];

		return $bad[$counter];
	}

	public function setGoodField($input)
	{
		$this->goodField = $input;
	}

	public function setBadField($input)
	{
		$this->badField = $input;
	}
}