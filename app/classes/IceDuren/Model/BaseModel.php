<?php namespace IceDuren\Model;

use \Carbon\Carbon;
use \Exception;
use \DB;

class BaseModel extends \Eloquent {
	// common rules
	protected $rules = array();
	protected $createRules = array();
	protected $updateRules = array();
	protected $validator = array();
	protected $dates = array();
	protected $delayedTransformAttributes = array();

	public static function boot()
	{
		parent::boot();

		static::creating(function($model) {
			DB::beginTransaction();
			$ret = $model->validateCreate() && $model->transformAttributes();
			if (!$ret) {
				DB::rollback();
				return false;
			}
			return true;
		});
		static::created(function($model) {
			try {
				$ret = $model->runDelayedTransformAttributes();
			} catch (RollbackException $e) {
				DB::rollback();
				return false;
			} catch (Throwable $e) {
				DB::rollback();
				throw $e;
			}

			if (!$ret) {
				DB::rollback();
				return $ret;
			}
			DB::commit();
			return $ret;
		});
		static::updating(function($model) {
			DB::beginTransaction();
			$ret = $model->validateUpdate() && $model->transformAttributes();
			if (!$ret) {
				DB::rollback();
				return false;
			}

			try {
				$ret = $model->runDelayedTransformAttributes();
			} catch (RollbackException $e) {
				DB::rollback();
				return false;
			} catch (Throwable $e) {
				DB::rollback();
				throw $e;
			}

			if (!$ret) {
				DB::rollback();
				return $ret;
			}
			DB::commit();
			return $ret;
		});
	}

	public static function construct(array $attributes = array())
	{
		return new static($attributes);
	}

	public function getCreateRules()
	{
		return $this->createRules + $this->rules;
	}

	public function getUpdateRules()
	{
		return $this->updateRules + $this->rules;
	}

	public function setUpdateRequired($field)
	{
		$val = $this->getUpdateRules();

		if (!isset($val[$field]))
			throw new Exception('failed to required');

		if (strlen($val[$field]) == 0)
			$this->updateRules[$field] = 'required';
		else
			$this->updateRules[$field] = $val[$field] . '|required';
	}
	
	public function validateCreate()
	{
		return $this->validate($this->getCreateRules());
	}

	public function validateUpdate()
	{
		return $this->validate($this->getUpdateRules());
	}

	public function validate($rules)
	{
		if (count($rules) == 0) {
			$this->validator = new \Validator();
			return true;
		}

		$validator = \Validator::make($this->attributes, $rules);
		$this->validator = $validator;

		return $validator->passes();
	}

	public function validationErrors()
	{
		if ($this->validator)
			return $this->validator->errors();

		return new \MessageBag();
	}

	public function validator()
	{
		if ($this->validator)
			return $this->validator;

		return new \Validator();
	}

	public function transformAttributes()
	{
		return true;
	}

	public function getDates()
	{
		return parent::getDates() + $this->dates;
	}

	public function transformToArray($attr)
	{
		$ret = array();
		foreach ($attr as $i) {
			$ret[$i] = $this->__get($i);
		}

		return $ret;
	}

	public function addDelayedTransformAttributes($closure)
	{
		$this->delayedTransformAttributes[] = $closure;
	}

	public function runDelayedTransformAttributes()
	{
		$ret = true;
		foreach ($this->delayedTransformAttributes as $run) {
			$ret = $ret && $run();
		}

		$this->delayedTransformAttributes = array();

		return $ret;
	}
}
