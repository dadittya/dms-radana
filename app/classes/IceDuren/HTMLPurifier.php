<?php namespace IceDuren;

include('Libs/htmlpurifier/HTMLPurifier.auto.php');

class HTMLPurifier {
	static protected $purifier = null;

	static public function purify($text) {
		if (static::$purifier == null) {
			$config = \HTMLPurifier_Config::createDefault();
			$config->set('HTML.Allowed', ''); // Allow Nothing
			static::$purifier = new \HTMLPurifier($config);
		}
		$res = static::$purifier->purify($text);
		$pattern = '/[\s]+/mu';
		$replacement = ' ';
		$res = preg_replace($pattern, $replacement, $res);
		return $res;
	}
}