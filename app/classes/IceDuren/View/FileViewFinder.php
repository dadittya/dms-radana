<?php namespace IceDuren\View;

class FileViewFinder extends \Illuminate\View\FileViewFinder {

	protected function findInPaths($name, $paths)
	{
		if ($name[0] == '.') {
			$theme = \Config::get('theme.name');
			$name = "{$theme}{$name}";
		}
		
		return parent::findInPaths($name, $paths);
	}
}