<?php namespace IceDuren;

class FontManager {
	static protected $font_cache = array();
	static protected $font_set = array();

	static protected $font_map_file = array();
	static protected $font_map_name = array();

	static protected function initialize($force = false)
	{
		if (!$force && count(static::$font_cache))
			return;

		$output = array();
		$ret = 0;
		exec('fc-list : file family style slant weight', $output, $ret);
		if ($ret)
			return;

		static::$font_cache = array();
		static::$font_set = array();
		foreach ($output as $list) {
			$font = Font::parse($list);

			static::$font_map_file[$font->file()] = $font;
			if (!isset(static::$font_map_name[$font->name()]))
				static::$font_map_name[$font->name()] = array();
			static::$font_map_name[$font->name()][] = $font;

			if (isset(static::$font_set[$font->hash_name()]))
				continue;

			static::$font_cache[] = $font;
			static::$font_set[$font->hash_name()] = true;
		}

		usort(static::$font_cache, array("IceDuren\Font", "_compare"));
	}

	static public function refreshCache()
	{
		static::initialize(true);
	}

	static public function get()
	{
		static::initialize();
		return static::$font_cache;
	}

	static public function findByFile($file)
	{
		static::initialize();
		if (!isset(static::$font_map_file[$file]))
			return null;

		return static::$font_map_file[$file];
	}

	static public function findByName($name)
	{
		$name = escapeshellarg($name);
		$output = array();
		$ret = 0;
		exec("fc-match {$name} file", $output, $ret);
		if ($ret)
			return null;

		if (count($output) == 0)
			return null;

		$output = $output[0];

		if (strlen($output) == 0)
			return null;

		$filename = explode(':', $output);
		$filename = explode('=', $filename[1]);
		$filename = strtr($filename[1], '\\', '');

		return static::findByFile($filename);
	}

	static public function findByNameStyle($name, $bold, $italic)
	{
		$style = array();
		if ($bold)
			$style[] = 'bold';
		if ($italic)
			$style[] = 'italic';

		$style = implode(' ', $style);

		if (strlen($style))
			$name = escapeshellarg("{$name}: style={$style}");
		else
			$name = escapeshellarg($name);

		$output = array();
		$ret = 0;
		exec("fc-match {$name} file", $output, $ret);
		if ($ret)
			return null;

		if (count($output) == 0)
			return null;

		$output = $output[0];

		if (strlen($output) == 0)
			return null;

		$filename = explode(':', $output);
		$filename = explode('=', $filename[1]);
		$filename = strtr($filename[1], array('\\' => ''));

		return static::findByFile($filename);
	}

	static public function install($file)
	{
		if (!$file->isValid())
			return null;

		$file_path = \IceDuren\Storage::random('storage.font', $file->getClientOriginalExtension()); 
		$file->move($file_path[0], $file_path[1]);

		static::updateSystemFont();

		$font_sets = static::$font_set;

		static::refreshCache();
		$font = static::findByFile($file_path[2]);

		if (!$font) {
			@unlink($file_path[2]);
		}

		// font already uploaded
		if (isset($font_sets[$font->hash_name()])) {
			@unlink($file_path[2]);
			static::updateSystemFont();
			static::refreshCache();

			return null;
		}

		return $font;
	}

	static protected function updateSystemFont()
	{
		$output = array();
		$ret = 0;
		exec('fc-cache', $output, $ret);
	}

	static public function findDefault()
	{
		reset(static::$font_cache);
		return current(static::$font_cache);
	}
}
