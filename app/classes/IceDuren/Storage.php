<?php namespace IceDuren;

use \Config;
use \Str;
use \StdClass;

class Storage {
	static public function random($key, $extension = null, $len = 32) {
		$path = Config::get($key);
		if (!$path)
			throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random($len);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

	static public function cleanFilename($value) {
		$pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
		$replacement = '-';
		$value = preg_replace($pattern, $replacement, $value);
		return $value;
	}
}