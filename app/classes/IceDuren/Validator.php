<?php namespace IceDuren;

use \Config;
use \Carbon\Carbon;

class Validator extends \Illuminate\Validation\Validator {
	protected function validateUtf8($attribute, $value, $parameters)
	{
		return mb_check_encoding($value, 'UTF-8');;
	}

	protected function validateNotExists($attribute, $value, $parameters)
	{
		return !$this->validateExists($attribute, $value, $parameters);
	}

	protected function validateFileExists($attribute, $value, $parameters)
	{
		$path = Config::get($parameters[0]);
		return file_exists("{$path}/{$value}");
	}

	protected function validateRecaptcha($attribute, $value, $parameters)
	{
		$challenge = isset($_POST["recaptcha_challenge_field"]) ? $_POST["recaptcha_challenge_field"] : '';
		$response = isset($_POST["recaptcha_response_field"]) ? $_POST["recaptcha_response_field"] : '';
		return ReCaptcha::check($challenge, $response);
	}

	protected function validateCurrency($attribute, $value, $parameters)
	{
		return preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $value);
	}

	protected function validateMathOperator($attribute, $value, $parameters)
	{
		$math = new MathFunction();
		return $math->check($value);
	}

	protected function validateArrayMin($attribute, $value, $parameters)
	{
		$val = json_decode($value);
		if (!is_array($val))
			return false;

		return count($val) >= $parameters[0];
	}

	protected function validateArrayMax($attribute, $value, $parameters)
	{
		$val = json_decode($value);
		if (!is_array($val))
			return false;

		return count($val) <= $parameters[0];
	}

	protected function validateArrayUnique($attribute, $value, $parameters)
	{
		$val = json_decode($value, true);
		if (!is_array($val))
			return false;

		$set = array();
		foreach ($val as $i) {
			$obj = array();
			foreach ($parameters as $params) {
				$obj[$params] = $i[$params];
			}
			$key = json_encode($obj);
			if (isset($set[$key]))
				return false;
			$set[$key] = true;
		}
		return true;
	}

	protected function replaceArrayMin($message, $attribute, $rule, $parameters)
	{
		return str_replace(':array_min', $parameters[0], $message);
	}

	protected function validateAfterYear($attribute, $value, $parameters)
	{
		if (is_string($value))
			$value = Carbon::parse($value);
		return $value->year >= $parameters[0];
	}

	protected function replaceAfterYear($message, $attribute, $rule, $parameters)
	{
		return str_replace(':after_year', $parameters[0], $message);
	}

	protected function validateDateDiffMax($attribute, $value, $parameters)
	{
		if (is_string($value))
			$value = Carbon::parse($value);
		$v2 = array_get($this->data, $parameters[0]);
		if (is_string($v2))
			$v2 = Carbon::parse($v2);
		$diff = abs(intval($parameters[1]));
		switch ($parameters[2]) {
		case 'm':
		case 'month':
		case 'months':
			if (abs($value->diffInMonths($v2)) > $diff)
				return false;
			return true;
		default:
			throw new \Exception('Parameter Error');
		}
	}

	protected function replaceDateDiffMax($message, $attribute, $rule, $parameters)
	{
		return str_replace(':other', $this->getAttribute($parameters[0]), $message);
	}

	protected function validateIs($attribute, $value, $parameters)
	{
		return $value == $parameters[0];
	}

	protected function replaceIs($message, $attribute, $rule, $parameters)
	{
		return str_replace(':expected_value', $parameters[1], $message);
	}
}
