<?php namespace IceDuren;

class Snippet {
	static public function word($text, $word = 72)
	{
		$text = HTMLPurifier::purify($text);
		$text = explode(" ", $text, $word + 1);
		unset($text[$word]);
		return implode(" ", $text);
	}
}