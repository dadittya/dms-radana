<?php namespace IceDuren;

use \StdClass;

class SimpleTree {
	protected $root = null;

	public function __construct()
	{
		$this->root = new SimpleNode();
	}

	public function setNodeVal($paths, $value)
	{
		$node = $this->root;
		foreach ($paths as $path) {
			if (!isset($node->content[$path])) {
				$node->content[$path] = new SimpleNode();
			}
			$node = $node->content[$path];
		}
		$node->type = 'C';
		$node->content = $value;
	}

	public function getNodeVal($paths)
	{
		$node = $this->root;
		foreach ($paths as $path) {
			if (!isset($node->content[$path])) {
				return null;
			}
			$node = $node[$path]->content;
		}
		if ($node->type != 'C')
			return null;
		return $node;
	}

	public function walkNode($function)
	{
		$queue = array();
		$n = new StdClass();
		$n->node = $this->root;
		$n->path = array();
		$queue[] = $n;
		while (count($queue)) {
			$n = array_shift($queue);
			switch ($n->node->type) {
			case 'N':
				foreach ($n->node->content as $path => $it) {
					$q = new StdClass();
					$q->node = $it;
					$q->path = array_merge($n->path, array($path));
					$queue[] = $q;
				}
				break;
			case 'C':
				$function($n->node->content, $n->path);
				break;
			default:
				throw new Exception('FATAL_ERR_WRONG_NODE_TYPE');
			}
		}
	}

	public function reset()
	{
		$this->root = array();
	}
}
