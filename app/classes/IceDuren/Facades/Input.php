<?php namespace IceDuren\Facades;

use \Carbon\Carbon;
use \DateTime;
use \Exception;

class Input extends \Illuminate\Support\Facades\Input {

	public static function parseDate($value, $default, $format)
	{
		if ($value instanceof Carbon)
			return $value;

		if ($value instanceof DateTime)
			return Carbon::instance($value);

		$date = date_parse_from_format($format, $value);

		if ($date['warning_count'] || $date['error_count']) {
			if ($default == null || $value instanceof Carbon)
				return $default;

			if ($default instanceof DateTime)
				return Carbon::instance($default);

			$date = date_parse_from_format($format, $default);
			if ($date['warning_count'] || $date['error_count'])
				throw new Exception('Date Default Format Error');

			return Carbon::createFromFormat($format, $default);
		}

		return Carbon::createFromFormat($format, $value);
	}

	public static function date($key, $default = null)
	{
		$key = static::get($key, $default);
		$ret = static::parseDate($key, $default, 'd/m/Y');
		if ($ret)
			return $ret->setTime(0, 0, 0);
		return $ret;
	}

	public static function datetime($key, $default = null)
	{
		$key = static::get($key, $default);
		return static::parseDate($key, $default, 'd/m/Y H:i');
	}

	public static function datetimeValue($key)
	{
		return static::parseDate($key, '', 'd/m/Y H:i');
	}

	public static function nullify($key, $default = null)
	{
		$val = Input::get($key, $default);
		if (strlen($val) == 0)
			return null;
		return $val;
	}

	public static function currency($key, $default = null)
	{
		$val = Input::get($key, $default);
		if ($val == null)
			return $val;

		// remove whitespace
		$val = trim($val);
		// remove leading zeros
		$val = ltrim($val, '0');

		return $val;
	}
}
