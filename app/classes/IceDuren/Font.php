<?php namespace IceDuren;

use \Exception;

class Font {
	protected $file;
	protected $name;
	protected $style;
	protected $slant;
	protected $weight;

	public function __construct($file, $name, $style, $slant, $weight)
	{
		$this->file = $file;
		$this->name = $name;
		$this->style = $style;
		$this->slant = $slant;
		$this->weight = $weight;
	}

	public static function parse($list)
	{
		$data = explode(':', $list);
		if (count($data) != 5)
			throw new \Exception('failed to parse');

		$file = trim($data[0]);
		$name = explode(',', trim($data[1]));
		$name = $name[0];
		$style = explode('=', trim($data[2]));
		$style = explode(',', trim($style[1]));
		$style = $style[0];
		$slant = explode('=', trim($data[3]));
		$slant = $slant[1];
		$weight = explode('=', trim($data[4]));
		$weight = $weight[1];

		return new Font($file, $name, $style, $slant, $weight);
	}

	public function file()
	{
		return $this->file;
	}

	public function name()
	{
		return $this->name;
	}

	public function style()
	{
		return $this->style;
	}

	public function slant()
	{
		return $this->slant;
	}

	public function weight()
	{
		return $this->weight;
	}

	public function isSystemFont()
	{
		$user_path = Config::get('storage.font');
		return strncmp($this->file, $user_path, strlen($user_path)) == 0;
	}

	public function isUserFont()
	{
		return !$this->isSystemFont();
	}

	public function hash_name()
	{
		return $this->name() . '|' . $this->style();
	}

	static public function _compare($a, $b) { 
		if ($a->hash_name() == $b->hash_name()) {
			return 0;
		} 
		return ($a->hash_name() < $b->hash_name()) ? -1 : 1;
	}
}