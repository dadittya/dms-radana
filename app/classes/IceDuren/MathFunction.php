<?php namespace IceDuren;

use \Pentangle\EqOS\EOS;
use \Exception;

class MathFunction extends EOS {
	public function check($func)
	{
		try {
			$this->checkInfix($func);
		} catch (Throwable $e) {
			return false;
		}
		return true;
	}

	public function solve($func, $var = null)
	{
		return $this->solveIF($func, $var);
	}
}