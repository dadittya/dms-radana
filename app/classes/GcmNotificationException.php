<?php

class GcmNotificationException extends \Exception {

	public function setResponse($response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
