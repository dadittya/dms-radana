<?php

class GcmNotification {

	public function fire($job, $data) 
	{
		if (!$data['to']) {
			$job->delete();
			return;
		}

		$header = array(
			'Authorization' => 'key=AIzaSyAY52PXW7ftdVtWelLSK8Mhy4rmmhLmPkk',
			'Content-Type' => 'application/json'
		);

		try {
			
			$response = Requests::POST('https://gcm-http.googleapis.com/gcm/send', $header, json_encode($data));

			if ($response->status_code != 200) {
				$error = new GcmNotificationException();
				$error->setResponse($response->body);
				throw $error;
			}
			
			Log::info('Success send gcm notif to ' . $data['to']);
			$job->delete();
			return;

		} catch (GcmNotificationException $e) {
			Log::info('Failed send gcm notif: ' . $e->response);
			$job->release(5);
			throw $e;	
		} catch (Throwable $e) {
			$job->release(5);
			throw $e;	
		}
	}

}
