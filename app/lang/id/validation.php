<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => "Pilihan :attribute harus diterima.",
	"active_url"       => "Isi kolom :attribute bukanlah alamat URL yang valid.",
	"after"            => "Isi kolom :attribute haruslah setelah tanggal :date.",
	"alpha"            => "Isi kolom :attribute hanya boleh berisi huruf.",
	"alpha_dash"       => "Isi kolom :attribute hanya bisa diisi : huruf, angka, - .",
	"alpha_num"        => "Isi kolom :attribute hanya boleh berisi huruf dan atau angka.",
	"before"           => "Isi kolom :attribute haruslah sebelum tanggal :date.",
	"between"          => array(
		"numeric" => "Isi kolom :attribute harus berjumlah diantara :min - :max.",
		"file"    => "Isi kolom :attribute harus berjumlah diantara :min - :max kilobyte.",
		"string"  => "Isi kolom :attribute harus berjumlah diantara :min - :max karakter.",
	),
	"confirmed"        => "Isi kolom :attribute tidak sama.",
	"date"             => "Isi kolom :attribute tidak dalam format yang benar.",
	"date_format"      => "Isi kolom :attribute tidak sesuai format :format.",
	"different"        => "Isi kolom :attribute dan :other tidak boleh sama.",
	"digits"           => "Isi kolom :attribute harus berjumlah :digits digit.",
	"digits_between"   => "Isi kolom :attribute harus berjumlah diantara :min dan :max digit.",
	"email"            => "Isi kolom :attribute tidak dalam format yang valid.",
	"exists"           => "Isi kolom :attribute yang dipilih tidak valid.",
	"image"            => "Isi kolom :attribute harus berupa gambar.",
	"in"               => "Isi kolom :attribute yang dipilih tidak valid.",
	"integer"          => "Isi kolom :attribute harus berupa angka integer.",
	"ip"               => "Isi kolom :attribute harus berupa alamat IP yang valid.",
	"max"              => array(
		"numeric" => "Isi kolom :attribute tidak boleh lebih dari :max.",
		"file"    => "Isi kolom :attribute tidak boleh lebih dari :max kilobyte.",
		"string"  => "Isi kolom :attribute tidak boleh lebih dari :max karakter.",
	),
	"mimes"            => "Isi kolom :attribute haruslah file dengan tipe: :values.",
	"min"              => array(
		"numeric" => "Isi kolom :attribute minimal berjumlah :min.",
		"file"    => "Isi kolom :attribute minimal berjumlah :min kilobyte.",
		"string"  => "Isi kolom :attribute minimal berjumlah :min karakter.",
	),
	"not_in"           => "Isi kolom :attribute yang dipilih tidak valid.",
	"numeric"          => "Isi kolom :attribute hanya dengan angka.",
	"regex"            => "Isi kolom :attribute tidak dalam format yang benar.",
	"required"         => "Kolom :attribute wajib di isi atau tidak dalam format yang benar.",
	"required_if"      => "Isi kolom :attribute wajib diisi jika kolom :other berisi :value.",
	"required_with"    => "Isi kolom :attribute field is required when :values is present.",
	"required_without" => "Isi kolom :attribute field is required when :values is not present.",
	"same"             => "Isi kolom :attribute dan :other harus sama.",
	"size"             => array(
		"numeric" => "Isi kolom :attribute harus berjumlah :size.",
		"file"    => "Isi kolom :attribute harus berjumlah :size kilobyte.",
		"string"  => "Isi kolom :attribute harus berjumlah :size karakter.",
	),
	"unique"           => "Isi kolom :attribute sudah terpakai.",
	"url"              => "Format :attribute tidak valid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'login'			=> "No telp atau password kurang tepat"
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/
	'attributes' => array(
			'is_product' => 'produk',
			'assigned_dep' => 'subbagian/seksi',
			'assigned_emp' => 'pelaksana tugas',
			'official_memo' => 'nota dinas',
			'memo_date' => 'tanggal nota dinas',
			'target_date' => 'tanggal selesai',
			'received_at' => 'tanggal masuk',
	),

	'recaptcha' => 'Mohon ketik ulang kode recaptcha kembali',
	'utf8' => 'Isi kolom :attribute mengandung symbol yang tidak dikenal.',
	'file_exists' => 'Mohon cek kembali format file dan ukuran file :attribute.',
	'not_exists' => 'Isi :attribute sudah terpakai',
	'array_min' => 'Isi kolom :attribute minimal :array_min',

	'is' => 'Isi kolom :attribute harus diisi dengan \':expected_value\'',

	'after_year' => 'Tahun tidak bisa kurang dari :after_year',

	'date_diff_max' => 'Jarak waktu :attribute dan :other terlalu jauh',
);
