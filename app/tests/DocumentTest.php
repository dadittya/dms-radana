<?php

class DocumentTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowListPage()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/document');
        $this->assertResponseStatus(200);
    }

    public function testShowNewDocumentPage()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/document/new');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testSaveDocumentEmpty()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/document', []);

        $this->assertRedirectedTo('/document/new');
        $this->assertSessionHas('UPDATE.FAIL');
    }

    public function testSaveDocumentSuccess()
    {   
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $employee = EmployeeModel::whereNotNull('department')->orderBy('department')->first();
        $dept = $employee->dept->id;
        $dept2 = $dept+1;

        $this->call('POST', '/document', [
            'agenda_number' => Str::random(20),
            'received_at' => '25/01/2015',
            'document_number' => Str::random(20),
            'is_copy' => 0,
            'document_date' => '24/01/2015',
            'received_from' => 'Daniek',
            'about' => 'Something',
            'enclosure' => 'Lorem Ipsum Bla Bla',
            'clasification' => 'RHS',
            'urgency' => 'SGR',
            'assigned_emp' => $employee->id,
            'assigned_dep' => $dept,
            'stage' => 'NEW',
            'target_date' => '01/02/2015',
            'is_product' => 0,
            'department' => [$dept, $dept2],
            'notes_' . $dept => 'First Dept Notes',
            'notes_' . $dept2 => 'Second Dept Notes and Guidance',
            'directive' => ['Setuju', 'Perbaiki'],
            'notes' => 'Create new doc',
            'clients' => '[{"id":"0","name":"Bank Mandiri","_id":"0"}]',
            'carbonCopy' => '[]'
        ]);
        $this->assertRedirectedTo('/document');
        $this->assertSessionHas('UPDATE.OK');
    }

    public function testShowDetailDoc()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $document = DocumentModel::first();
        $this->call('GET', '/document/' . $document->id);
        $this->assertResponseStatus(200);
    }
    
    /**
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testShowDetailDocWrongId()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/document/1000000');
    }    
}
