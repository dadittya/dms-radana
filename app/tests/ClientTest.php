<?php

class ClientTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowListPage()
    {
    	$emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/client');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testShowNewForm()
    {
    	$emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/client/new');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testSaveJobTitleEmpty()
    {
    	$emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/client', []);

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/client/new');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        } 
    }

    public function testSaveJobTitleSuccess()
    {
    	 $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/client', [
        	'name' => 'PT Jaya',
            'address' => 'Jl Leci 50',
            'email' => 'ptjaya@example.com',
            'phone' => '0245126'
        ]);
        $this->assertRedirectedTo('/client');
        $this->assertSessionHas('UPDATE.OK');
    }

   	/**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testShowEditFormWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/client/1000000/edit');	
    }

    public function testShowEditFormSuccess()
    {
    	$emps = EmployeeModel::get();
        $client = ClientModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/client/' . $client->id . '/edit'); 

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertResponseStatus(200);
                $this->assertEquals('PT Jaya', $client->name);
            }

            $this->call('POST', '/logout');
        } 
    }

    /**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testUpdateRowWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/client/1000000/edit');
    }

    public function testUpdateRowEmpty()
    {
    	$emps = EmployeeModel::get();
        $client = ClientModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/client/' . $client->id . '/edit');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/client/' . $client->id . '/edit');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }
    }

    public function testUpdateRowSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $client = ClientModel::orderBy('created_at', 'DESC')->first();
        $this->call('POST', '/client/' . $client->id . '/edit', [
        	'name' => 'PT Jayakarta',
            'address' => 'Jl Jeruk Raya 10',
            'email' => 'jayakarta@example.com',
            'phone' => '0212345'
        ]);

        $update = ClientModel::orderBy('created_at', 'DESC')->first();
        $this->assertRedirectedTo('/client');
        $this->assertSessionHas('UPDATE.OK');
        $this->assertEquals('PT Jayakarta', $update->name);
        $this->assertEquals('Jl Jeruk Raya 10', $update->address);
        $this->assertEquals('jayakarta@example.com', $update->email);
        $this->assertEquals('0212345', $update->phone);
    }
}