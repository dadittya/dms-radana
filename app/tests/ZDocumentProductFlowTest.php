<?php

use \Carbon\Carbon;
/*
 * TODO
 *  - Add check for not approved flow
 *  - Add check for field changes
 */
class ZDocumentProductFlowTest extends TestCase {
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();

		Mail::pretend(true);
		Session::start();
		Route::enableFilters();

		Artisan::call('migrate');
		$this->seed();
	}

	public function pretendJobAs($job_level)
	{
		$e = JobLevelModel::find($job_level)->employee()->first();
		$this->assertNotNull($e);

		$e->password = Hash::make('admin123');
		$e->save();

		$this->call('POST', '/logout');
    	$this->assertRedirectedTo('/login');

		$this->call('POST', '/login', [
			'email' => $e->email,
			'password' => 'admin123'
		]);

		$this->assertRedirectedTo('/');
		return $e;
	}

	public function pretendEmployeeAs($emp_id)
	{
		$e = EmployeeModel::find($emp_id);
		$this->assertNotNull($e);

		$e->password = Hash::make('admin123');
		$e->save();

		$this->call('POST', '/logout');
    	$this->assertRedirectedTo('/login');

		$this->call('POST', '/login', [
			'email' => $e->email,
			'password' => 'admin123'
		]);

		$this->assertRedirectedTo('/');
		return $e;
	}

	public static function createNewDocumentInputData()
	{
		$f = Faker\Factory::create();
		return [
			'agenda_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
			'received_at' => $f->dateTimeBetween('-4 days', 'now'),
			'document_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
			'is_copy' => 0,
			'document_date' => $f->dateTimeBetween('-14 days', '-4 days'),
			'received_from_id' => 0,
			'received_from' => $f->name,
			'about' => $f->sentence(11),
			'enclosure' => $f->paragraph(4),
			'clasification' => $f->randomElement(['SRS','RHS','KFD','BIA']),
			'urgency' => $f->randomElement(['KLT','SSG','SGR','BIA']),
			'assigned_emp' => null,
			'assigned_dep' => null,
			'stage' => 'NEW',
			'target_date' => $f->dateTimeBetween('2 days', '7 days'),
			'is_product' => 0,
			'department' => null,
			'directive' => [],
			'notes' => 'Testing new document ',
			'clients' => '[{"id":"0","name":"Rumah"}]',
			'carbonCopy' => '[]'
		];
	}

	public function createNewDocumentProduct($product = 1)
	{
		$docs = static::createNewDocumentInputData();
		$docs['is_product'] = $product;

		$this->call('POST', '/document', $docs);

		return $docs;
	}

	public function assertNewDocumentSuccess()
	{
		$this->assertRedirectedTo('/document');
		$this->assertSessionHas('UPDATE.OK');
	}

	public function assertNewDocumentFailNoAuth()
	{
		$this->assertRedirectedTo('/');
		$this->assertFalse(Session::has('UPDATE.OK'));
		$this->assertFalse(Session::has('UPDATE.FAIL'));
	}

	public function saveNewDocumentProduct($product = 1)
	{
		$doc = $this->createNewDocumentProduct($product);
		$this->assertNewDocumentSuccess();

		$d = DocumentModel::where('agenda_number', '=', $doc['agenda_number'])->first();
		$this->assertNotNull($d);

		return $d;
	}

	public function assertDocumentCount($p, $np)
	{
		$this->assertEquals($p, DocumentModel::where('is_product', 1)->count());
		$this->assertEquals($np, DocumentModel::where('is_product', 0)->count());
	}

	public function assertDocumentStage($id, $stage)
	{
		$doc = DocumentModel::find($id);
		$this->assertNotNull($doc);
		$this->assertEquals($stage, DocumentModel::find($id)->stage);
	}

	// Dokumen baru hanya bisa dibuat oleh
	//  - Kepala Kantor
	//  - Sekretaris
	public function testCreateNewDocument()
	{
		// count all doc number
		$product = DocumentModel::where('is_product', 1)->count();
		$nonproduct = DocumentModel::where('is_product', 0)->count();
		
		$levels = JobLevelModel::get();

		$job_tested = [];
		foreach ($levels as $level) {
			$this->pretendJobAs($level->id);
			$this->assertNotNull(AUth::user());
			$this->assertEquals(Auth::user()->level, $level->id);

			if (in_array($level->id, [
				JobLevelModel::KEPALA,
				JobLevelModel::SEKRETARIS])) {
				$this->createNewDocumentProduct();
				$this->assertNewDocumentSuccess();
				$product++;
				$this->assertDocumentCount($product, $nonproduct);
				
				$this->createNewDocumentProduct(0);
				$this->assertNewDocumentSuccess();
				$nonproduct++;

				$this->assertDocumentCount($product, $nonproduct);
			} else {
				$this->createNewDocumentProduct();
				$this->assertNewDocumentFailNoAuth();
				$this->assertDocumentCount($product, $nonproduct);

				$this->createNewDocumentProduct(0);
				$this->assertNewDocumentFailNoAuth();
				$this->assertDocumentCount($product, $nonproduct);
			}
			$job_tested[$level->id] = true;

			Session::clear();
		}

		// make sure all admin is tested
		$this->assertArrayHasKey(1, $job_tested);
		$this->assertArrayHasKey(2, $job_tested);
		// make sure non admin is tested
		unset($job_tested[1]);
		unset($job_tested[2]);
		$this->assertNotEmpty($job_tested);
	}

	public function do_stage_change_fail_as_job($id, $job_id, $stage)
	{
		$doc = DocumentModel::find($id);
		$this->assertNotNull($doc);
		$stage_orig = $doc->stage;

		$this->pretendJobAs($job_id);

		$id = $doc->id;
		$this->call('POST', "/document/{$id}/chg_state", ['stage' => $stage]);
		$this->assertRedirectedTo("/document/{$id}");
		$this->assertSessionHas('UPDATE.FAIL');
		$this->assertDocumentStage($id, $stage_orig);

		return $id;
	}

	public function do_stage_change_success_as_job($id, $job_id, $stage)
	{
		$doc = DocumentModel::find($id);
		$this->assertNotNull($doc);

		$this->pretendJobAs($job_id);

		$id = $doc->id;
		$this->call('POST', "/document/{$id}/chg_state", ['stage' => $stage]);
		$this->assertRedirectedTo("/document/{$id}");
		$this->assertSessionHas('UPDATE.OK');
		$this->assertDocumentStage($id, $stage);

		return $id;
	}

	public function do_stage_change_fail_as_emp($id, $emp_id, $stage)
	{
		$doc = DocumentModel::find($id);
		$this->assertNotNull($doc);
		$stage_orig = $doc->stage;

		$this->pretendEmployeeAs($emp_id);

		$id = $doc->id;
		$this->call('POST', "/document/{$id}/chg_state", ['stage' => $stage]);
		$this->assertRedirectedTo("/document/{$id}");
		$this->assertSessionHas('UPDATE.FAIL');
		$this->assertDocumentStage($id, $stage_orig);

		return $id;
	}

	public function do_stage_change_success_as_emp($id, $emp_id, $stage)
	{
		$doc = DocumentModel::find($id);
		$this->assertNotNull($doc);

		$this->pretendEmployeeAs($emp_id);

		$id = $doc->id;
		$this->call('POST', "/document/{$id}/chg_state", ['stage' => $stage]);
		$this->assertRedirectedTo("/document/{$id}");
		$this->assertSessionHas('UPDATE.OK');
		$this->assertDocumentStage($id, $stage);

		return $id;
	}

	/**
	 * __NEW => PRAPP
	 * Dokumen Baru => Approval Kepala Kantor
	 *  
	 * Syarat approval produk:
	 *  - User kepala kantor
	 *  - Assigned Dept terisi
	 *  - Dokumen adalah produk
	 *  
	 */
	public function test__NEW_PRAPP_FailDeptNull()
	{
		$this->pretendJobAs(JobLevelModel::KEPALA);
		$this->do_stage_change_fail_as_job(
			$this->saveNewDocumentProduct()->id,
			JobLevelModel::KEPALA,
			'PRAPP'
		);
	}

	public function test__NEW_PRAPP_FailNotOfficeHead()
	{
		$this->pretendJobAs(JobLevelModel::KEPALA);
		$doc = $this->saveNewDocumentProduct();

		// Put assigned dep first
		$dept = DepartmentModel::first();
		$this->assertNotNull($dept);
		$doc->assigned_dep = $dept->id;
		
		$this->assertTrue($doc->save());

		$this->do_stage_change_fail_as_job(
			$doc->id,
			JobLevelModel::STAF,
			'PRAPP'
		);
	}

	public function test__NEW_PRAPP_FailNotProduct()
	{
		$this->pretendJobAs(JobLevelModel::KEPALA);
		$doc = $this->saveNewDocumentProduct(0);

		// Put assigned dep first
		$dept = DepartmentModel::first();
		$this->assertNotNull($dept);
		$doc->assigned_dep = $dept->id;

		$this->assertTrue($doc->save());

		$this->do_stage_change_fail_as_job(
			$doc->id,
			JobLevelModel::KEPALA,
			'PRAPP'
		);
	}

	public function test__NEW_PRAPP_Success()
	{
		$this->pretendJobAs(JobLevelModel::KEPALA);
		$doc = $this->saveNewDocumentProduct();

		// Put assigned dep first
		$dept = DepartmentModel::first();
		$this->assertNotNull($dept);
		$doc->assigned_dep = $dept->id;
		
		$this->assertTrue($doc->save());

		return $this->do_stage_change_success_as_job(
			$doc->id,
			JobLevelModel::KEPALA,
			'PRAPP'
		);
	}

	/**
	 * PRAPP => PRDLV
	 * Dokumen Disetujui => Terkirim
	 *  
	 * Syarat deliveri produk:
	 *  - User kepala kantor
	 *  - Sekretaris
	 * 
	 */
	public function test_PRAPP_PRDLV_FailNotHeadOffice()
	{
		$except = [
			JobLevelModel::KEPALA,
			JobLevelModel::SEKRETARIS,
			JobLevelModel::ENGINEER,
		];
		$jobs = JobLevelModel::whereNotIn('id', $except)->get();

		foreach ($jobs as $j) {
			$this->do_stage_change_fail_as_job($this->test__NEW_PRAPP_Success(), $j->id, 'PRDLV');
		}
	}

	public function test_PRAPP_PRDLV_SuccessKepalaKantor()
	{
		return $this->do_stage_change_success_as_job(
			$this->test__NEW_PRAPP_Success(),
			JobLevelModel::KEPALA,
			'PRDLV'
		);
	}

	public function test_PRAPP_PRDLV_SuccessSekretaris()
	{
		return $this->do_stage_change_success_as_job(
			$this->test__NEW_PRAPP_Success(),
			JobLevelModel::SEKRETARIS,
			'PRDLV'
		);
	}

	/**
	 * PRDLV => PRASG
	 * Dokumen Dikirim => Diserahkan kepada staff
	 *  
	 * Syarat deliveri produk:
	 *  - User kepala kantor
	 *  - Sekretaris
	 * 
	 */
	public function test_PRDLV_PRASG_FailNotAssigned()
	{
		$doc = DocumentModel::find($this->test_PRAPP_PRDLV_SuccessSekretaris());
		$this->assertNotNull($doc);
		$this->assertNull($doc->assigned_emp);

		$this->do_stage_change_fail_as_job(
			$doc->id,
			JobLevelModel::KEPALA,
			'PRASG'
		);
	}

	public function createPRDLVAssigned()
	{
		$doc = DocumentModel::find($this->test_PRAPP_PRDLV_SuccessSekretaris());

		$emp = EmployeeModel::where('department', '=', $doc->assigned_dep)->first();
		$this->assertNotNull($emp);
		$doc->assigned_emp = $emp->id;
		$doc->save();

		return $doc->id;
	}

	public function test_PRDLV_PRASG_FailNotKasubag()
	{
		$except = [
			JobLevelModel::KEPALA,
			JobLevelModel::SEKRETARIS,
			JobLevelModel::KASUBAG,
			JobLevelModel::ENGINEER
		];
		$jobs = JobLevelModel::whereNotIn('id', $except)->get();

		$id = $this->createPRDLVAssigned();
		foreach ($jobs as $j) {
			$this->do_stage_change_fail_as_job(
				$id,
				$j->id,
				'PRASG'
			);
		}
	}

	public function test_PRDLV_PRASG_FailNotKasubagAssignedDep()
	{
		$id = $this->createPRDLVAssigned();
		$doc = DocumentModel::find($id);

		$emps = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '!=', $doc->assigned_dep)
			->get();

		foreach ($emps as $e) {
			$this->do_stage_change_fail_as_emp(
				$id,
				$e->id,
				'PRASG'
			);
		}
	}

	public function test_PRDLV_PRASG_SuccessKasubag()
	{
		$id = $this->createPRDLVAssigned();
		$doc = DocumentModel::find($id);

		$emp = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '=', $doc->assigned_dep)
			->take(1)->get()->first();

		$this->assertNotNull($emp);

		return $this->do_stage_change_success_as_emp(
			$id,
			$emp->id,
			'PRASG'
		);
	}
	
	public function test_PRDLV_PRASG_SuccessSuperUser()
	{
		$this->do_stage_change_success_as_job(
			$this->createPRDLVAssigned(),
			JobLevelModel::KEPALA,
			'PRASG'
		);
	}

	/**
	 * PRASG => PRPRO
	 * Dokumen Diproses oleh staff kemudian diajukan sebagai proposal
	 *  
	 * Syarat proposal produk:
	 *  - Diajukan oleh staff yang bertugas
	 *  - Diajukan oleh staff pengganti
	 *  - Diajukan oleh kasubagnya
	 *  - Diajukan oleh sekretaris / kepala kantor
	 */
	public function test_PRASG_PRPRO_FailedNotAssignedEmp()
	{
		$id = $this->test_PRDLV_PRASG_SuccessKasubag();
		$doc = DocumentModel::find($id);

		$emps = JobLevelModel::find(JobLevelModel::STAF)->employee()
			->whereNotIn('employees.id', [$doc->assigned_emp, $doc->substitute_emp])
			->get();

		foreach ($emps as $e) {
			$this->do_stage_change_fail_as_emp(
				$id,
				$e->id,
				'PRPRO'
			);
		}
	}

	public function test_PRASG_PRPRO_FailedNotAssignedKasubag()
	{
		$id = $this->test_PRDLV_PRASG_SuccessKasubag();
		$doc = DocumentModel::find($id);

		$emps = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '!=', $doc->assigned_dep)
			->get();			

		foreach ($emps as $e) {
			$this->do_stage_change_fail_as_emp(
				$id,
				$e->id,
				'PRPRO'
			);
		}
	}

	public function test_PRASG_PRPRO_SuccessAssignedStaff()
	{
		$id = $this->test_PRDLV_PRASG_SuccessKasubag();
		$doc = DocumentModel::find($id);

		// fill memo notes
		$f = Faker\Factory::create();
		$doc->official_memo = $f->lexify('???/') . $f->numerify('####/####/##');
		$doc->memo_date = Carbon::now();
		$doc->save();

		return $this->do_stage_change_success_as_emp(
			$id,
			$doc->assigned_emp,
			'PRPRO'
		);
	}

	public function test_PRASG_PRPRO_SuccessSubtituteStaff()
	{
		$id = $this->test_PRDLV_PRASG_SuccessKasubag();
		$doc = DocumentModel::find($id);

		// fill memo notes
		$f = Faker\Factory::create();
		$doc->official_memo = $f->lexify('???/') . $f->numerify('####/####/##');
		$doc->memo_date = Carbon::now();

		// assign subtitute staff
		$emp = JobLevelModel::find(JobLevelModel::STAF)->employee()
			->where('employees.id', '!=', $doc->assigned_emp)
			->where('department', '!=', $doc->assigned_dep)
			->take(1)->get()->first();

		$this->assertNotNull($emp);

		$doc->substitute_emp = $emp->id;
		$doc->save();

		return $this->do_stage_change_success_as_emp(
			$id,
			$emp->id,
			'PRPRO'
		);
	}

	public function test_PRASG_PRPRO_SuccessKasubag()
	{
		$id = $this->test_PRDLV_PRASG_SuccessKasubag();
		$doc = DocumentModel::find($id);

		// fill memo notes
		$f = Faker\Factory::create();
		$doc->official_memo = $f->lexify('???/') . $f->numerify('####/####/##');
		$doc->memo_date = Carbon::now();
		$doc->save();

		$emp = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '=', $doc->assigned_dep)
			->take(1)->get()->first();

		return $this->do_stage_change_success_as_emp(
			$id,
			$emp->id,
			'PRPRO'
		);
	}
	/**
	 * PRRO => PRASG
	 * Department Reject
	 */
	public function test_PRPRO_PRASG_SuccessKasubag()
	{
		$id = $this->test_PRASG_PRPRO_SuccessAssignedStaff();
		$doc = DocumentModel::find($id);

		$emp = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '=', $doc->assigned_dep)
			->take(1)->get()->first();

		return $this->do_stage_change_success_as_emp(
			$id,
			$emp->id,
			'PRASG'
		);
	}
	/**
	 * PRPRO => PRDAP
	 * Department Approve
	 */
	public function test_PRPRO_PRDAP_SuccessKasubag()
	{
		$id = $this->test_PRASG_PRPRO_SuccessAssignedStaff();
		$doc = DocumentModel::find($id);

		$emp = JobLevelModel::find(JobLevelModel::KASUBAG)->employee()
			->where('department', '=', $doc->assigned_dep)
			->take(1)->get()->first();

		return $this->do_stage_change_success_as_emp(
			$id,
			$emp->id,
			'PRDAP'
		);
	}
	/**
	 * PRDAP => PRANS
	 * Head Office Proposal Approve
	 */
	public function test_PRDAP_PRANS_SuccessHeadOffice()
	{
		$id = $this->test_PRPRO_PRDAP_SuccessKasubag();

		return $this->do_stage_change_success_as_job(
			$id,
			JobLevelModel::KEPALA,
			'PRANS'
		);
	}
	/**
	 * PRANS => PRFNS
	 * Document confirmed sent!
	 */
	public function test_PRANS_PRFNS_SuccessSekretaris()
	{
		$id = $this->test_PRDAP_PRANS_SuccessHeadOffice();

		return $this->do_stage_change_success_as_job(
			$id,
			JobLevelModel::SEKRETARIS,
			'PRFNS'
		);
	}

	//test add cc disposition
	public static function inputDataEditDoc($doc)
	{
		$input = array(
			'agenda_number' => $doc->agenda_number,
			'received_at' => $doc->received_at->format('d/m/Y'),
			'document_number' => $doc->document_number,
			'is_copy' => $doc->is_copy,
			'document_date' => $doc->document_date->format('d/m/Y'),
			'received_from_id' => $doc->received_from_id,
			'received_from' => $doc->received_from,
			'about' => $doc->about,
			'enclosure' => $doc->enclosure,
			'clasification' => $doc->clasification,
			'urgency' => $doc->urgency,
			'assigned_emp' => $doc->assigned_emp,
			'assigned_dep' => $doc->assigned_dep,
			'stage' => $doc->stage,
			'target_date' => $doc->target_date ? $doc->target_date->format('d/m/Y') : null,
			'is_product' => $doc->is_product,
			'clients' => $doc->clients,
		);

		return $input;
	}

	public function addCCDispositionAsDept($user_dept, $doc)
	{
		$f = Faker\Factory::create();
		$input = static::inputDataEditDoc($doc);
		$employees = EmployeeModel::lists('id');
		if ($user_dept)
			$employees = EmployeeModel::where('department', '=', $user_dept)->lists('id');
		$id = $f->randomElement($employees);

		$input['carbonCopy'] = json_encode([[
					'id' => $id
				]]);

		$this->call('POST', '/document/' . $doc->id, $input);		
		return $id;
	}

	public function addCCDispositionAsNotDept($user_dept, $doc)
	{
		$f = Faker\Factory::create();
		$input = static::inputDataEditDoc($doc);
		$employees = EmployeeModel::lists('id');
		if ($user_dept)
			$employees = EmployeeModel::where('department', '<>', $user_dept)->lists('id');
		$id = $f->randomElement($employees);

		$input['carbonCopy'] = json_encode([[
					'id' => $id
				]]);

		$this->call('POST', '/document/' . $doc->id, $input);		
		return $id;
	}

	public function removeCCDispositionAsDept($id, $doc)
	{
		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id)->first();
		$this->assertNotNull($cc);
		$input = static::inputDataEditDoc($doc);
		$input['carbonCopy'] = '[]';

		$this->call('POST', '/document/' . $doc->id, $input);		
		return $id;
	}

	public function addCCDispositionSuccess($doc, $id)
	{
		$this->assertSessionHas('UPDATE.OK');
		$this->assertRedirectedTo('/document/' . $doc->id);

		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id)->first();
		$this->assertNotNull($cc);
	}

	public function addCCDispositionFailed($doc, $id)
	{
		$this->assertSessionHas('UPDATE.FAIL');
		$this->assertRedirectedTo('/document/' . $doc->id . '/edit');

		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id)->first();
		$this->assertNull($cc);
	}

	public function addCCDispositionUnauthorized($doc, $id)
	{
		$this->assertSessionHas('UPDATE.OK');
		$this->assertRedirectedTo('/document/' . $doc->id);

		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id)->first();
		$this->assertNull($cc);
	}

	public function removeCCDispositionSuccess($doc, $id)
	{
		$this->assertSessionHas('UPDATE.OK');
		$this->assertRedirectedTo('/document/' . $doc->id);

		$cc = $doc->docCarbonCopy()->first();
		$this->assertNull($cc);
	}

	public function removeCCDispositionFailed($doc, $id)
	{
		$this->assertSessionHas('UPDATE.FAIL');
		$this->assertRedirectedTo('/document/' . $doc->id . '/edit');

		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id);
		$this->assertNotNull($cc);
	}	

	public function removeCCDispositionUnauthorized($doc, $id)
	{
		$this->assertSessionHas('UPDATE.OK');
		$this->assertRedirectedTo('/document/' . $doc->id);

		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id)->first();
		$this->assertNotNull($cc);
	}

	public function pretendAddCCDisposition($user_dept, $doc)
	{
		$f = Faker\Factory::create();
		$employees = EmployeeModel::whereNotNull('department')->lists('id');
		if ($user_dept)
			$employees = EmployeeModel::where('department', '<>', $user_dept)->lists('id');
		$id = $f->randomElement($employees);

		$doc->docCarbonCopy()->sync(array($id));
		$cc = $doc->docCarbonCopy()->where('emp_id', '=', $id);
		$this->assertNotNull($cc);
		return $id;
	}

	public function test_add_cc_disposition()
	{
		$levels = JobLevelModel::get();

		$job_tested = [];
		foreach ($levels as $level) {
			$this->pretendJobAs($level->id);
			$this->assertNotNull(AUth::user());
			$this->assertEquals(Auth::user()->level, $level->id);
			$user_dept = Auth::user()->department;

			if (in_array($level->id, [
				JobLevelModel::KEPALA,
				JobLevelModel::SEKRETARIS])) {
				//add as dept
				$doc = DocumentModel::orderBy('created_at', 'DESC')->first();
				$id = $this->addCCDispositionAsDept($user_dept, $doc);
				$this->addCCDispositionSuccess($doc, $id);

				//remove as dept
				$id = $this->removeCCDispositionAsDept($id, $doc);
				$this->removeCCDispositionSuccess($doc, $id);

				//remove as not dept
				$id = $this->pretendAddCCDisposition($user_dept, $doc);
				$this->removeCCDispositionAsDept($id, $doc);
				$this->removeCCDispositionSuccess($doc, $id);

				//add as not dept
				$id = $this->addCCDispositionAsNotDept($user_dept, $doc);
				$this->addCCDispositionSuccess($doc, $id);

			} elseif ($level->id == JobLevelModel::KASUBAG) {
				//add as dept
				$doc = DepartmentModel::find($user_dept)->docDisposition()->where('assigned_dep', '<>', $user_dept)
					->where('is_done', '=', 0)->first();
				$id = $this->addCCDispositionAsDept($user_dept, $doc);
				$this->addCCDispositionSuccess($doc, $id);

				//remove as dept
				$id = $this->removeCCDispositionAsDept($id, $doc);
				$this->removeCCDispositionSuccess($doc, $id);

				//remove as not dept
				$id = $this->pretendAddCCDisposition($user_dept, $doc);
				$this->removeCCDispositionAsDept($id, $doc);
				$this->removeCCDispositionFailed($doc, $id);

				//add as not dept
				$id = $this->addCCDispositionAsNotDept($user_dept, $doc);
				$this->addCCDispositionFailed($doc, $id);

			} else {
				//add unauthorized
				$doc = DocumentModel::orderBy('created_at', 'DESC')->first();
				$id = $this->addCCDispositionAsDept($user_dept, $doc);
				$this->addCCDispositionUnauthorized($doc, $id);

				//remove unauthorized
				$id = $this->pretendAddCCDisposition($user_dept, $doc);
				$this->removeCCDispositionAsDept($id, $doc);
				$this->removeCCDispositionUnauthorized($doc, $id);
			}

			$job_tested[$level->id] = true;
			Session::clear();
		}

		// make sure all admin is tested
		$this->assertArrayHasKey(1, $job_tested);
		$this->assertArrayHasKey(2, $job_tested);
		// make sure non admin is tested
		unset($job_tested[1]);
		unset($job_tested[2]);
		$this->assertNotEmpty($job_tested);
	}
}