<?php

class AuthTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();

        Artisan::call('migrate');
        $this->seed();
    }

    public function testShowLoginForm()
    {
    	$this->call('GET', '/login');
    	$this->assertResponseStatus(200);
    }

    public function testAuthLoginEmpty()
    {
    	$this->call('POST', '/login/', []);
    	$this->assertRedirectedTo('/login');
    }

    public function testAuthLoginSuccess()
    {
    	$this->call('POST', '/login', [
    		'email' => 'admin@example.com',
    		'password' => 'admin123'
    	]); 

    	$this->assertRedirectedTo('/');
    	$this->assertEquals('Admin', Auth::user()->name);
    }

    public function testAuthLoginWrong()
    {
    	$this->call('POST', '/login', [
    		'email' => 'admin@example.com',
    		'password' => 'admin'
    	]); 

    	$this->assertRedirectedTo('/login');
    }

    public function testAuthLogout()
    {
    	$this->call('POST', '/logout');
    	$this->assertRedirectedTo('/login');
    	$this->assertEquals(false, Auth::check());
    }
}