<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

	public function setUp()
	{
		parent::setUp();

		$class_list = get_declared_classes();

		foreach ($class_list as $class) {
			if (is_subclass_of($class, '\IceDuren\Model\BaseModel')) {
				call_user_func($class . '::flushEventListeners');
				call_user_func($class . '::boot');
			}
		}
	}

	public function tearDown()
	{
		parent::tearDown();

		// workarround for mysql resource exhaustion
		// a.k.a too many connection
		DB::disconnect();
	}

}
