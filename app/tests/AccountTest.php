<?php

class AccountTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }
    
    public function testRefreshTable()
    {
        Artisan::call('migrate');
        $this->seed();
    }

    public function testShowChangePass()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/account/password');
        $this->assertResponseStatus(200);
    }

    public function testDoChangePassEmpty()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/account/password', []);
        $this->assertRedirectedTo('/account/password');
        $this->assertSessionHas('UPDATE.FAIL');
    }

	public function testDoChangePassSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'deka@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/account/password', [
        	'old_password' => 'admin123',
        	'password' => 'deka123',
        	'password_confirmation' => 'deka123'
        ]);

        $this->assertRedirectedTo('/account/password');
        $this->assertSessionHas('UPDATE.OK');

        $this->call('POST', '/logout');

        $this->call('POST', '/login', [
            'email' => 'deka@example.com',
            'password' => 'deka123'
        ]); 

        $this->assertRedirectedTo('/');
    }    
 }