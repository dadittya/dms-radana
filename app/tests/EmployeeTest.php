<?php

class EmployeeTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowListPage()
    {
    	$emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/employee');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testShowNewFormPage()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/employee/new');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testSaveEmployeeEmpty()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/employee', []);

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/employee/new');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }
    }

    public function testSaveEmployeeSuccess()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $dept = DepartmentModel::first();
        $jobtitle = JobTitleModel::first();

        $this->call('POST', '/employee', [
            'name' => 'Jaya',
            'email' => 'jaya@example.com',
            'nip' => Str::random(15),
            'password' => 'admin123',
            'password_confirmation' => 'admin123',
            'department' => $dept->id,
            'job_title' => $jobtitle->id,
            'active' => 1
        ]);
        $this->assertRedirectedTo('/employee');
        $this->assertSessionHas('UPDATE.OK');

        $check = EmployeeModel::orderBy('created_at', 'DESC')->first();
        $this->assertEquals('Jaya', $check->name);
    }

    public function testSaveEmployeeWrongPass()
    {
        $emps = EmployeeModel::get();
        $dept = DepartmentModel::first();
        $jobtitle = JobTitleModel::first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/employee', [
                'name' => 'Jack',
                'email' => 'jack@example.com',
                'nip' => Str::random(15),
                'password' => 'jack123',
                'password_confirmation' => 'jaya123',
                'department' => $dept->id,
                'job_title' => $jobtitle->id,
                'active' => 1
            ]);

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/employee/new');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }
    }

    public function testShowEditPage()
    {
        $emps = EmployeeModel::get();
        $employee = EmployeeModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/employee/' . $employee->id . '/edit');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }        
    }

    public function testFilterEmployee()
    {
        $dept = DepartmentModel::orderBy('created_at', 'DESC')->first();

        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/employee/filter', [
            'q' => 'Jack',
            'dept' => $dept->id
        ]);

        $this->assertResponseStatus(200);

        $this->call('POST', '/logout');
    }

    /**
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testShowEditWrongId()
    {
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/employee/1000000/edit');
    }

    /**
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testUpdateRowWrongId()
    {
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/employee/1000000/edit');
    }

    public function testUpdateRowEmpty()
    {
        $emps = EmployeeModel::get();
        $employee = EmployeeModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/employee/' . $employee->id . '/edit', []);

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/employee/' . $employee->id . '/edit');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        } 
    }

    public function testUpdateRowSuccess()
    {
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $dept = DepartmentModel::orderBy('created_at', 'DESC')->first();
        $jobtitle = JobTitleModel::orderBy('created_at', 'DESC')->first();

        $emp = EmployeeModel::orderBy('created_at', 'DESC')->first();
        $this->call('POST', '/employee/' . $emp->id . '/edit', [
            'name' => 'Jack',
            'email' => 'jack@example.com',
            'nip' => Str::random(20),
            'department' => $dept->id,
            'job_title' => $jobtitle->id,
            'active' => 1
        ]);

        $this->assertRedirectedTo('/employee');
        $this->assertSessionHas('UPDATE.OK');

        $check = EmployeeModel::find($emp->id);
        $this->assertEquals('Jack', $check->name);
    }
}