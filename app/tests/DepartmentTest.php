<?php

class DepartmentTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowListPage()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/department');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testShowNewForm()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/department/new');
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testSaveDeptEmpty()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/department', []);
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/department/new');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }
    }

    public function testSaveDeptSuccess()
    {
    	 $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/department', [
        	'name' => 'Kekayaan Negara'
        ]);
        $this->assertRedirectedTo('/department');
        $this->assertSessionHas('UPDATE.OK');
    }

   	/**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testShowEditFormWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/department/1000000/edit');	
    }

    public function testShowEditFormSuccess()
    {
        $emps = EmployeeModel::get();
        $dept = DepartmentModel::orderBy('created_at', 'desc')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/department/' . $dept->id . '/edit');   
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertResponseStatus(200);
                $this->assertEquals('Kekayaan Negara', $dept->name);
            }

            $this->call('POST', '/logout');
        }        
    }

    /**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testUpdateRowWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/department/1000000/edit');
    }

    public function testUpdateRowEmpty()
    {
        $emps = EmployeeModel::get();
        $dept = DepartmentModel::orderBy('created_at', 'desc')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/department/' . $dept->id . '/edit');
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/department/' . $dept->id . '/edit');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }        
    }

    public function testUpdateRowSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $dept = DepartmentModel::orderBy('created_at', 'desc')->first();
        $this->call('POST', '/department/' . $dept->id . '/edit', [
        	'name' => 'Pengelolaan Kekayaan Negara'
        ]);

        $update = DepartmentModel::orderBy('created_at', 'desc')->first();
        $this->assertRedirectedTo('/department');
        $this->assertSessionHas('UPDATE.OK');
        $this->assertEquals('Pengelolaan Kekayaan Negara', $update->name);
    }

    /**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testDeleteRowWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/department/1000000/del');
    }

    public function testDeleteRowFailed()
    {
        $emps = EmployeeModel::get();
        $dept = EmployeeModel::whereNotNull('department')->first()->dept;

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/department/' . $dept->id . '/del');
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/department');
                $this->assertSessionHas('DELETE.FAIL');
            }

            $this->call('POST', '/logout');
        } 
    }

    public function testDeleteRowSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $dept = DepartmentModel::orderBy('created_at', 'desc')->first();
        $this->call('POST', '/department/' . $dept->id . '/del');
        $this->assertRedirectedTo('/department');
        $this->assertSessionHas('DELETE.OK');	
    }
}