<?php

class JobTitleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowListPage()
    {
    	$emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/jobtitle');

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testShowNewForm()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/jobtitle/new');
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else
                $this->assertResponseStatus(200);

            $this->call('POST', '/logout');
        }
    }

    public function testSaveJobTitleEmpty()
    {
        $emps = EmployeeModel::get();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/jobtitle', []);
            
            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/jobtitle/new');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }
    }

    public function testSaveJobTitleSuccess()
    {
         $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/jobtitle', [
            'name' => 'Sekretaris Asisten',
            'level' => 2
        ]);
        $this->assertRedirectedTo('/jobtitle');
        $this->assertSessionHas('UPDATE.OK');
    }

   	/**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testShowEditFormWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('GET', '/jobtitle/1000000/edit');	
    }

    public function testShowEditFormSuccess()
    {
    	$emps = EmployeeModel::get();
        $jobtitle = JobTitleModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('GET', '/jobtitle/' . $jobtitle->id . '/edit'); 

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertResponseStatus(200);
            }

            $this->call('POST', '/logout');
        }
    }

    /**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testUpdateRowWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/jobtitle/1000000/edit');
    }

    public function testUpdateRowEmpty()
    {
    	$emps = EmployeeModel::get();
        $jobtitle = JobTitleModel::orderBy('created_at', 'DESC')->first();

        foreach ($emps as $emp) {
            //login
            $this->call('POST', '/login', [
                'email' => $emp->email,
                'password' => 'admin123'
            ]); 

            $this->call('POST', '/jobtitle/' . $jobtitle->id . '/edit'); 

            if ($emp->level != 1 && $emp->level != 2 && $emp->level != 5)
                $this->assertRedirectedTo('/');
            else {
                $this->assertRedirectedTo('/jobtitle/' . $jobtitle->id . '/edit');
                $this->assertSessionHas('UPDATE.FAIL');
            }

            $this->call('POST', '/logout');
        }   
    }

    public function testUpdateRowSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $jobtitle = JobTitleModel::orderBy('created_at', 'DESC')->first();
        $this->assertNotEquals('Office Boy', $jobtitle->name);

        $this->call('POST', '/jobtitle/' . $jobtitle->id . '/edit', [
        	'name' => 'Office Boy',
            'level' => 4
        ]);

        $update = JobTitleModel::orderBy('created_at', 'DESC')->first();
        $this->assertRedirectedTo('/jobtitle');
        $this->assertSessionHas('UPDATE.OK');
        $this->assertEquals('Office Boy', $update->name);
    }

    /**
	 * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
    public function testDeleteRowWrongId()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $this->call('POST', '/jobtitle/1000000/del');
    }


    public function testDeleteRowSuccess()
    {
    	//login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

        $jobtitle = JobTitleModel::orderBy('created_at', 'DESC')->first();
        $this->call('POST', '/jobtitle/' . $jobtitle->id . '/del');
        $this->assertRedirectedTo('/jobtitle');
        $this->assertSessionHas('DELETE.OK');   
    }
}