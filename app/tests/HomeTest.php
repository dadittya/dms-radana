<?php

class HomeTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function setUp()
	{
        parent::setUp();

        Mail::pretend(true);
        Session::start();
        Route::enableFilters();
    }

    public function testShowHomePage()
    {
        //login
        $this->call('POST', '/login', [
            'email' => 'admin@example.com',
            'password' => 'admin123'
        ]); 

    	$this->call('GET', '/');
    	$this->assertResponseStatus(200);
    }
}