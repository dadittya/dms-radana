<?php

if (!function_exists('i18l_currency'))
{
	function i18l_currency($value, $locale = null) {
		if ($locale == null)
			$locale = Config::get('app.locale');
		$formater = new NumberFormatter($locale, NumberFormatter::CURRENCY);
		return $formater->formatCurrency($value, "IDR");
	}
}

if (!function_exists('check_field'))
{
	function check_field($writeable, $value) {
		if (!in_array($value, $writeable))
			return "disabled";
	}
}

if (!function_exists('auth_token'))
{
	function auth_token() {
		$token = Input::get('_token');
		if (strlen($token) < 64)
			return false;

		$token = EmployeeTokenModel::where('token', '=', $token)->first();

		if (!$token || $token->employee->active == "0")
			return false;

		$token->touch();

		Auth::login($token->employee);

		return $token;
	}
}
if (!function_exists('i18l_date'))
{
	function i18l_date($value, $pattern = array(), $locale = null, $timezone = null) {
		if ($locale == null)
			$locale = Config::get('app.locale');

		if ($timezone == null)
			$timezone = Config::get('app.timezone');

		if (is_array($pattern)) {
			$dateformat = (isset($pattern['date']) ? $pattern['date'] : IntlDateFormatter::MEDIUM);
			$timeformat = (isset($pattern['time']) ? $pattern['time'] : IntlDateFormatter::MEDIUM);
			$formater = new IntlDateFormatter($locale, $dateformat, $timeformat, $timezone);
		} else {
			$formater = new IntlDateFormatter($locale, null, null, $timezone, null, $pattern);
		}

		return $formater->format($value);
	}
}