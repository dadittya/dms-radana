<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) {
		if (Request::method() == 'GET')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::fullUrl()));
		if (Request::method() == 'POST')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::header('referer')));
		return Redirect::action('AuthController@showLoginForm');
	}

	if (Auth::user()->active == "0")
		return Redirect::action('AuthController@showLoginForm');
});

Route::filter('auth.admin', function()
{
	if (Auth::guest()) {
		if (Request::method() == 'GET')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::fullUrl()));
		if (Request::method() == 'POST')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::header('referer')));
		return Redirect::action('AuthController@showLoginForm');
	}

	if (Auth::user()->active == "0")
		return Redirect::action('AuthController@showLoginForm');

	$jobs = [
		JobLevelModel::KEPALA,
		JobLevelModel::SEKRETARIS,
		// TODO: PLEASE REMOVE AFTER BETA STATE
		JobLevelModel::ENGINEER,
	];

	//page monitoring
	if (Request::is('monitor') && Auth::user()->dept && Auth::user()->dept->disposition_product)
		return;
	
	if (!in_array(Auth::user()->level, $jobs))
		return Redirect::action('HomeController@showHome');
});

Route::filter('auth.monitoring', function()
{
	if (Auth::guest()) {
		if (Request::method() == 'GET')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::fullUrl()));
		if (Request::method() == 'POST')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::header('referer')));
		return Redirect::action('AuthController@showLoginForm');
	}

	if (Auth::user()->active == "0")
		return Redirect::action('AuthController@showLoginForm');

	$jobs = [
		JobLevelModel::KEPALA,
		JobLevelModel::SEKRETARIS,
		JobLevelModel::ENGINEER,
		JobLevelModel::KASUBAG,
	];

	//page monitoring
	if (Request::is('monitor') && Auth::user()->dept && Auth::user()->dept->disposition_product)
		return;
	
	if (!in_array(Auth::user()->level, $jobs))
		return Redirect::action('HomeController@showHome');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

// API FILTER
Route::filter('auth.token', function()
{
	if (!auth_token())
		App::abort(403);
});

Route::filter('auth.login_or_token', function()
{
	if (auth_token())
		return;

	if (Input::get('_token'))
		App::abort(403);

	if (Auth::guest()) {
		if (Request::method() == 'GET')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::fullUrl()));
		if (Request::method() == 'POST')
			return Redirect::action('AuthController@showLoginForm', array('next_url' => Request::header('referer')));
		return Redirect::action('AuthController@showLoginForm');
	}

	if (Auth::user()->active == "0")
		return Redirect::action('AuthController@showLoginForm');
});