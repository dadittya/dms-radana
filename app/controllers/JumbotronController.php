<?php

class JumbotronController extends BaseController {

	public function showNewForm()
	{
		return View::make('new_jumbotron');
	}

	public function createNew()
	{
		try{
	$jumbotron = new JumbotronModel();
			$file = Input::file('image');
			$jumbotron->fill([
				'title' => $_POST['title'],
				'link' => $_POST['link'],
				'state' => $_POST['state']
			]);

			list($originalWidth, $originalHeight) = getimagesize(Input::file('image'));
			$ratio = $originalWidth / $originalHeight;

			//var_dump($ratio);die;

			if (Input::file('image'))
				$jumbotron->image = Input::file('image');

			$max = JumbotronModel::max('show_order');
			if ($max == null)
				$max = 1;
			else
				$max += 1;

			$jumbotron->show_order = $max;

			if (!$jumbotron->save()){
				@unlink($jumbotron->getImageFullPath());
				return Redirect::action('JumbotronController@showNewForm')
					->withErrors($jumbotron->validator())
					->withInput()
					->with('UPDATE.FAIL', true);
			}
			
			return Redirect::action('JumbotronController@showList')
				->with('UPDATE.OK', true);
		}catch(Symfony\Component\HttpFoundation\File\Exception\FileException $e){
			echo $e;
		}catch(\Exception $e){
			echo $e;
		}
		
	}

	public function showList()
	{
		$jumbotrons = JumbotronModel::orderBy('show_order')->get();

		return View::make('list_jumbotron')->with("jumbotrons", $jumbotrons);
	}

	public function showEditForm($id)
	{
		$jumbotron = JumbotronModel::find($id);
		if (!$jumbotron)
			App::abort(404);

		return View::make('edit_jumbotron')->with("jumbotron",$jumbotron);
	}

	public function updateRow($id)
	{
		$jumbotron = JumbotronModel::findOrFail($id);
		if (!$jumbotron)
			App::abort(404);

		$jumbotron->fill([
			'title' => $_POST['title'],
			'link' => $_POST['link'],
			'state' => $_POST['state']
		]);

		if (Input::hasFile('image')) {
			$old_image = $jumbotron->getImageFullPath();
			$jumbotron->image = Input::file('image');
		}

		//var_dump(Input::hasFile('image'));die;

		if ($jumbotron->save())
			@unlink($old_image);
		else{
			@unlink($jumbotron->getImageFullPath());
			return Redirect::action('JumbotronController@showEditForm', array($id))
				->withErrors($jumbotron->validator())
				->withInput()
				->with('UPDATE.FAIL', true);
		}
		
		return Redirect::action('JumbotronController@showList')
			->with('UPDATE.OK', true);
	}

	public function deleteRow($id)
	{
		$jumbotron = JumbotronModel::findOrFail($id);

		if (!$jumbotron)
			App::abort(404);

		$image = $jumbotron->getImageFullPath();

		if ($jumbotron->delete()) {
			@unlink($image);
			return Redirect::action('JumbotronController@showList')
				->with('DELETE.OK', true);
		}else{
			return Redirect::action('JumbotronController@showList')
				->with('DELETE.FAIL', true);
		}
	}

	public function moveRow($id)
	{
		$move = $_POST['move'];
		$curr = JumbotronModel::findOrFail($id);

		//var_dump($curr);die;

		$next = null;
		if ($move > 0) {
			$next = JumbotronModel::where('show_order', '>', $curr->show_order)
				->orderBy('show_order')
				->first();
			//var_dump($next);die;
		} elseif ($move < 0) {
			$next = JumbotronModel::where('show_order', '<', $curr->show_order)
				->orderBy('show_order', 'desc')
				->first();
			//var_dump($next);die;
		}

		if (!$next)
			return redirect()->action('JumbotronController@showList')
				->with('UPDATE.FAIL', true);

		$temp = $curr->show_order;
		$curr->show_order = $next->show_order;
		$next->show_order = $temp;

		$curr->save();
		$next->save();

		return Redirect::action('JumbotronController@showList')
			->with('UPDATE.OK', true);
	}

	public function showJumbotronImage($id)
	{
		$jumbotron = JumbotronModel::findOrFail($id);

		$headers = array(
			'Content-Disposition: inline',
			'Content-Type:' . $jumbotron->mimetype,
			'X-Content-Type-Options: nosniff',
			'Cache-Control: no-cache, no-store, must-revalidate',
			'Pragma: no-cache',
			'Expires: 0'
		);

		//var_dump($jumbotron->getImageFullPath());die;
		$resp = Response::download($jumbotron->getImageFullPath(), $jumbotron->image, $headers);
		var_dump($resp);die;

		return $resp;
	}
}
