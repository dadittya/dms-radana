<?php

class DepartmentController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showNewForm()
	{
		return View::make('new_department');
	}

	public function createNew()
	{
		$dep = new DepartmentModel();
		$dep->name = Input::get('name');
		$dep->disposition_product = Input::get('disposition_product');
		
		if (!$dep->save())
			return Redirect::action('DepartmentController@showNewForm')
				->withErrors($dep->validator())
				->withInput()
				->with('UPDATE.FAIL', true);
		
		return Redirect::action('DepartmentController@showList')
			->with('UPDATE.OK', true);
	}

	public function showList()
	{
		$deps = DepartmentModel::paginate(25);

		return View::make('list_department')->with("deps",$deps);
	}

	public function showEditForm($id)
	{
		$dep = DepartmentModel::find($id);
		if (!$dep)
			App::abort(404);

		return View::make('edit_department')->with("dep",$dep);
	}

	public function updateRow($id)
	{
		$dep = DepartmentModel::find($id);
		if (!$dep)
			App::abort(404);

		$dep->name = Input::get('name');
		$dep->disposition_product = Input::get('disposition_product');
		$dep->display_monitor = Input::get('display_monitor');
		
		if (!$dep->save())
			return Redirect::action('DepartmentController@showEditForm', array($id))
				->withErrors($dep->validator())
				->withInput()
				->with('UPDATE.FAIL', true);

		return Redirect::action('DepartmentController@showList')
			->with('UPDATE.OK', true);
	}

	public function deleteRow($id)
	{
		$dep = DepartmentModel::find($id);
		if (!$dep)
			App::abort(404);
		
		if (count($dep->employee) > 0 || count($dep->document) > 0 || !$dep->delete())
			return Redirect::action('DepartmentController@showList')
				->with('DELETE.FAIL', true);

		return Redirect::action('DepartmentController@showList')
			->with('DELETE.OK', true);
	}
}
