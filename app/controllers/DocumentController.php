<?php

use \IceDuren\RollbackException;
use \Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Carbon\Carbon;

class DocumentController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showList()
	{
		$product = Input::get('is_product');
		$user = Auth::user();
		$sort_by = json_decode(Input::get('sort_by'));

		//sorting
		if (count($sort_by) > 0)
			$documents = DocumentModel::doSortBy($sort_by);
		else
			$documents = DocumentModel::orderBy('created_at', 'DESC');

		if ($product)
			$documents = $documents->where('is_product', '=', $product == 'yes' ? 1 : 0);

		if (Input::get('target'))
			$documents = $documents->whereNotNull('target_date');

		if ($user->level == JobLevelModel::KASUBAG) {
			$documents->where(function ($query) use ($user) {
				$query->where('assigned_dep', '=', $user->department);
				$query->orWhereRaw(
					"EXISTS (SELECT * FROM docdisposition_department AS disp WHERE documents.id = disp.doc_id AND disp.dept_id = {$user->department})"
				);
			})->whereNotIn('stage', array('__NEW', 'PRAPP', 'NPAPP'));

		} elseif ($user->level == JobLevelModel::STAF) {
			$documents = $documents->where(function ($query) use ($user) {
                $query->where('assigned_emp', '=', $user->id)
                      ->orWhere('substitute_emp', '=', $user->id)
                      ->orWhereRaw(
            			"EXISTS (SELECT * FROM doccarboncopy_employee AS cc WHERE documents.id = cc.doc_id AND cc.emp_id = {$user->id})"
            	);
            })->whereNotIn('stage', array('__NEW', 'PRAPP', 'PRDLV','NPAPP', 'NPDLV'));
        }

		$employees = EmployeeModel::paginate(5);
		$deptlist = DepartmentModel::lists('name', 'id');
		$deptlist[0] = null;
		ksort($deptlist);

		$search = null;
		$advanced = null;
		$emp = array();

		foreach ($employees as $employee) {
			$emp[] = array (
				'id' => $employee->id,
                'name' => $employee->name,
                'department' => $employee->dept ? $employee->dept->name : '-',
                'job_title' => $employee->jobtitle->name,
			);
		}

		$q = Input::get('q');
		$received_at_from = Input::date('received_at_from');
		$received_at_to = Input::date('received_at_to');
		$target_date_from = Input::date('target_date_from');
		$target_date_to = Input::date('target_date_to');
		$document_date_from = Input::date('document_date_from');
		$document_date_to = Input::date('document_date_to');
		$assigned_dep = Input::get('assigned_dep');
		$assigned_emp = Input::get('assigned_emp');
		$filter_stage = Input::get('stage');
		$filter_no_agenda = Input::get('no_agenda');

		if ($q) {
			$client_id = array();
			$clients = ClientModel::where('name', 'like', "%{$q}%")->get();
			foreach ($clients as $i)
				$client_id[] = $i->id;

			$documents = $documents->where(function ($query) use($q, $client_id)
            {
                $query->orWhere('agenda_number', 'like', "%{$q}%")
					->orWhere('document_number', 'like', "%{$q}%")
					->orWhere('received_from', 'like', "%{$q}%")
					->orWhere('about', 'like', "%{$q}%")
					->orWhere('enclosure', 'like', "%{$q}%");

				if (count($client_id) > 0)
					$query->orWhereIn('received_from_id', $client_id);
            });
			$search = true;
		}

		//filter stage
		if ($filter_stage == 'new')
			$documents = $documents->where('stage', '=','__NEW');
		elseif ($filter_stage == 'approve')
			$documents = $documents->whereIn('stage', ['PRAPP', 'NPAPP']);			
		elseif ($filter_stage == 'disp_approve')
			$documents = $documents->whereIn('stage', ['PRDAP', 'NPDAP']);	
		elseif ($filter_stage == 'answer')
			$documents = $documents->whereIn('stage', ['PRANS', 'NPANS']);			

		//filter agenda
		if ($filter_no_agenda == 'yes')
			$documents = $documents->where(function ($query) {
				$query->whereNotNull('agenda_number')
					->orWhere('agenda_number', '<>', '');
			});
		elseif ($filter_no_agenda == 'no')
			$documents = $documents->where(function ($query) {
				$query->whereNull('agenda_number')
					->orWhere('agenda_number', '=', '');
			});


		if ($received_at_from) {
			$r_from = $received_at_from->toDateString();

			if (!$received_at_to)
				$r_to = $r_from;
			else
				$r_to = $received_at_to->toDateString();

			$documents = $documents->whereBetween('received_at', array($r_from . ' 00:00:00', $r_to . ' 23:59:59'));
			$advanced = true;
		}

		if ($target_date_from) {
			$t_from = $target_date_from->toDateString();

			if (!$target_date_to)
				$t_to = $t_from;
			else
				$t_to = $target_date_to->toDateString();

			$documents = $documents->whereBetween('target_date', array($t_from . ' 00:00:00', $t_to . ' 23:59:59'));
			$advanced = true;
		}

		if ($document_date_from) {
			$d_from = $document_date_from->toDateString();

			if (!$document_date_to)
				$d_to = $d_from;
			else
				$d_to = $document_date_to->toDateString();

			$documents = $documents->whereBetween('document_date', array($d_from . ' 00:00:00', $d_to . ' 23:59:59'));
			$advanced = true;
		}

		if ($assigned_dep != 0) {
			$dept = DepartmentModel::find($assigned_dep);
			if ($dept) {
				$doc_id = array();
				foreach ($dept->docDisposition as $i)
					$doc_id[] = $i->id;
			}

			$documents = $documents->where(function ($query) use ($assigned_dep, $doc_id) {
               if (count($doc_id) > 0)
	                $query->where('assigned_dep', '=', $assigned_dep)
	                      ->orWhereIn('id', $doc_id);
	            else
	            	$query->where('assigned_dep', '=', $assigned_dep);
            });
			$advanced = true;
		}

		if ($assigned_emp) {
			$documents = $documents->where(function ($query) use ($assigned_emp) {
                $query->where('assigned_emp', '=', $assigned_emp)
                      ->orWhere('substitute_emp', '=', $assigned_emp);
            });
			$advanced = true;
		}

		$documents = $documents->groupBy('id')->paginate(10);
		return View::make('list_document')
			->with('received_at_from', $received_at_from)
			->with('received_at_to', $received_at_to)
			->with('target_date_from', $target_date_from)
			->with('target_date_to', $target_date_to)
			->with('document_date_from', $document_date_from)
			->with('document_date_to', $document_date_to)
			->with('assigned_dep', $assigned_dep)
			->with('assigned_emp', $assigned_emp)
			->with('documents', $documents)
			->with('deptlist', $deptlist)
			->with('employees', $employees)
			->with('search', $search)
			->with('advanced', $advanced)
			->with('emp', $emp)
			->with('q', $q)
			->with('sort_by', $sort_by)
			->with('filter_stage', $filter_stage)
			->with('filter_no_agenda', $filter_no_agenda);
	}


	public function showCSList()
	{

		$about = Input::get('about');
		$assigned_dept = Input::get('assigned_dept');
		$no_agenda = Input::get('no_agenda');
		$no_surat = Input::get('no_surat');
		$pemohon = Input::get('pemohon');

		$search = false;
		$deptlist = DepartmentModel::lists('name', 'id');
		$deptlist[0] = null;
		ksort($deptlist);

		if ($about || $assigned_dept || $no_agenda || $no_surat || $pemohon) {
			$documents = DocumentModel::orderBy('created_at', 'DESC');

			$documents = $documents->where('is_product', '=', 1);

			if ($about){
				$documents -> where('about','LIKE',"%{$about}%");
				$search = true;
			}

			if($no_agenda){
				$documents -> where('agenda_number','LIKE',"%{$no_agenda}%");
				$search = true;
			}

			if($no_surat){
				$documents -> where('document_number','LIKE',"%{$no_surat}%");
				$search = true;
			}

			if($pemohon){
				$clients = ClientModel::select('id')->where('name', 'LIKE', "%{$pemohon}%")->get()->map(function ($i) {
					return $i->id;
				})->toArray();
				$documents->where(function ($q) use ($pemohon, $clients) {
					return $q->where('received_from', 'LIKE', "%{$pemohon}%")
						->orWhereIn('received_from_id', $clients);
				});
				$search = true;
			}

			if($assigned_dept){
				$dept = DepartmentModel::find($assigned_dept);
				if ($dept) {
					$doc_id = array();
					foreach ($dept->docDisposition as $i)
						$doc_id[] = $i->id;
				}

				$documents = $documents->where(function ($query) use ($assigned_dept, $doc_id) {
				   if (count($doc_id) > 0)
						$query->where('assigned_dep', '=', $assigned_dept)
							  ->orWhereIn('id', $doc_id);
					else
						$query->where('assigned_dep', '=', $assigned_dept);
				});
				$search = true;
			}

			$documents = $documents->groupBy('id')->paginate(10);
		} else {
			$documents = array();
		}

		return View::make('list_document_cs')
			->with('documents', $documents)
			->with('about', $about)
			->with('assigned_dept', $assigned_dept)
			->with('no_agenda', $no_agenda)
			->with('no_surat', $no_surat)
			->with('search', $search)
			->with('deptlist', $deptlist)
			->with('pemohon', $pemohon);
	}

	public function showFormDocument($id = 0)
	{
		$documents = DocumentModel::orderBy('document_date', 'DESC')->paginate(3);
		$departments = DepartmentModel::get();
		$emp = array();
		$doc = array();
		$directive = array();

		$deptlist = DepartmentModel::orderBy('name')->lists('name', 'id');
		$deptlist[0] = null;

		$clientlist = ClientModel::orderBy('name')->lists('name', 'id');
		$clientlist[0] = 'Lain-Lain';
		$year = Carbon::now()->year;

		if ($id == 0) {
			$document = new DocumentModel();
			if (Request::is('document/new'))
				$form_url = URL::route('save_doc');
			else
				$form_url = URL::route('save_archive');

			$view = View::make('new_document')->with('form_url', $form_url);
		} else {
			$document = DocumentModel::find($id);
			if (!$document)
				App::abort('404');

			$view = View::make('edit_document');
			foreach ($document->directive as $key => $i) {
				$directive[] = $i->transformToArray(array('id', 'directive'));
			}
		}

	 	foreach($departments as $department) {
	        $p = array();
	        foreach ($department->employee as $employee) {
	            $p[] = array(
	                'id' => $employee->id,
	                'name' => $employee->name,
	               	'dept_id' => $employee->department,
	                'department' => $employee->dept->name,
	                'job_title' => $employee->jobtitle->name,
            	);
            }

            $emp[$department->id] = $p;
        }

		//for department is null
		$non_dept = EmployeeModel::where('department', '=', null)->paginate(5);
		$p = array();
		foreach ($non_dept as $i) {
			$p[] = array(
				'id' => $i->id,
				'name' => $i->name,
				'job_title' => $i->jobtitle->name,
			);
		}
		$emp[0] = $p;

		$doc = array();
		foreach ($documents as $key => $i) {
			$doc[$key] = $i->transformToArray(array('id', 'agenda_number', 'document_number', 'about', 'received_from'));
			if ($i->received_from_id || $i->received_from_id != 0)
				$doc[$key]['received_from'] = $i->fromClient->name;
		}

		$writeable = $document->writeableField(Auth::user());

		if (Request::wantsJson()) {
			return Response::json(array(
				'doc' => $doc,
				'documents' => $documents->links()->render()
			));
		}

		return $view->with('document', $document)
			->with('documents', $documents)
			->with('departments', $departments)
			->with('deptlist', $deptlist)
			->with('emp', $emp)
			->with('doc', $doc)
			->with('directive', $directive)
			->with('non_dept', $non_dept)
			->with('clientlist', $clientlist)
			->with('writeable', $writeable)
			->with('year', $year);
	}

	public function showDetail($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort(404);

		return View::make('detail_document')
			->with('document', $document);
	}

	public function updateDocument($id = 0)
	{
		if($id == 0){
			if(Input::get('agenda_number') != null)
				$agenda = Input::get('agenda_number').'/'.Input::get('agenda_year');
			else
				$agenda = Input::get('agenda_number');
		}
		else{
			if(Input::get('agenda_year'))
				if(Input::get('agenda_number') != null)
					$agenda = Input::get('agenda_number').'/'.Input::get('agenda_year');
				else
					$agenda = Input::get('agenda_number');
			else
				$agenda = Input::get('agenda_number');
		}
		$input = array(
			'agenda_number' => $agenda,
			'received_at' => Input::date('received_at'),
			'document_number' => Input::get('document_number'),
			'is_copy' => Input::get('is_copy'),
			'document_date' => Input::date('document_date'),
			'received_from_id' => Input::get('received_from_id'),
			'received_from' => Input::get('received_from'),
			'about' => Input::get('about'),
			'enclosure' => Input::get('enclosure'),
			'urgency' => Input::get('urgency'),
			'clasification' => Input::get('clasification'),
			'assigned_dep' => Input::get('assigned_dep') == 0 ? null : Input::get('assigned_dep'),
			'assigned_emp' => Input::get('assigned_emp') == 0 ? null : Input::get('assigned_emp'),
			'substitute_emp' => Input::get('substitute_emp') == 0 ? null : Input::get('substitute_emp'),
			'referenced_doc' => Input::get('referenced_doc') == 0 ? null : Input::get('referenced_doc'),
			'target_date' => Input::date('target_date'),
			'is_product' => strlen(Input::get('is_product')) == 0 ? null : Input::get('is_product'),
			'official_memo' => Input::get('official_memo'),
			'memo_date' => Input::date('memo_date'),
			'clients' => Input::get('clients'),
		);

		if ($id == 0) {
			$document = new DocumentModel();
			$rules = $document->getCreateRules();

			if (Request::is('document/archive')) {
				$redirect_failed = Redirect::route('archive_doc');
				$redirect_success = Redirect::route('archive_doc');
				$input['is_done'] = 1;
				if ($input['is_product'] == 1)
					$input['stage'] = 'PRFNS';
				else
					$input['stage'] = 'NPFNS';

				//set unique agenda number
				$rules['agenda_number'] = 'required|utf8|max:255|unique:documents,agenda_number';

			} else {
				$redirect_failed = Redirect::route('create_doc');
				$redirect_success = Redirect::action('DocumentController@showList');
				$input['stage'] = '__NEW';

				//set unique agenda number
				$rules['agenda_number'] = 'utf8|max:255|unique:documents,agenda_number';
			}


			$changes = null;
			$writeable = $document->writeableField(Auth::user());
		} else {
			$document = DocumentModel::find($id);
			if (!$document)
				App::abort('404');

			//set unique agenda number
			$rules = $document->getUpdateRules();
			$rules ['agenda_number'] = 'utf8|max:255|unique:documents,agenda_number,' . $id;

			if (Auth::user()->level == JobLevelModel::KEPALA || Auth::user()->level == JobLevelModel::SEKRETARIS)
				$document->setUpdateRequired('clients');

			//remove input unwriteable field
			$writeable = $document->writeableField(Auth::user());
			foreach ($input as $key => $i) {
				if (!in_array($key, $writeable)) {
					unset($input[$key]);
					unset($rules[$key]);
				}
			}

			$redirect_failed = Redirect::route('edit_doc', array($id));
			$redirect_success = Redirect::action('DocumentController@showDetail', array($id));
			$input['stage'] = $document->stage;
			$changes = $document->findDataChanged($input);
		}

		$v = Validator::make($input, $rules);
   		if (!$v->passes())
   			return $redirect_failed
			->withInput()
			->withErrors($v)
			->with('UPDATE.FAIL', true);

		$filename = json_decode(Input::get('file'));
		$carbon = json_decode(Input::get('carbonCopy'));
		$document->fill($input);

		try {
			DB::transaction(function () use ($document, $filename, $redirect_failed, $changes, $writeable, $carbon, $id) {
				if (!$document->checkAuthorization(Auth::user()))
					throw new AuthorizationException('failed to save');

				$ori = $document->getOriginal('stage');
				if (!$document->save())
					throw new RollbackException('failed to save');

				//disposition
				$departments = Input::get('department');
				$disp_notes = Input::get('disp_notes');
				$chg_disposition = false;
				$dispositions = array();
				if ($departments)
					foreach ($departments as $i) {
						$dispositions[$i] = array('notes' => isset($disp_notes[$i]) ? $disp_notes[$i] : null);
					}

				if (in_array('dispositions', $writeable) && $document->checkHasDispositionChanged($departments, $disp_notes)) {
					$chg_disposition = true;
					$document->docDisposition()->detach();

					if (!$document->docDisposition()->sync($dispositions))
						throw new RollbackException('failed to save');
				}

				//directive
				$chg_directive = false;
				$directives = Input::get('directive');

				if (in_array('directives', $writeable) && $document->checkHasDirectiveChanged($directives)) {
					$chg_directive = true;
					$document->directive()->delete();

					if ($directives)
						foreach ($directives as $i) {
							$directive = new DocumentDirectiveModel();
							$directive->doc_id = $document->id;
							$directive->directive = $i;

							if (!$directive->save())
								throw new RollbackException('failed to save');
						}
				}

				//action
				$notes = Input::get('notes');

				if (!$document->saveWithAction('DOC', $ori, $notes, $filename, $chg_disposition, $chg_directive, $changes, $writeable, $carbon))
					throw new RollbackException('failed to save');

				//send notif new doc
				if ($id == 0 && $document->stage == '__NEW')
					$document->send_notif_intern();
			});
		} catch (AuthorizationException $e) {
			return Redirect::action('HomeController@showHome');
		} catch (RollbackException $e) {
			//remove file
			if (count($filename) > 0)
				foreach ($filename as $i) {
					$first = strtolower(substr($i->temp, 0, 1));
					@unlink(Config::get('storage.doc_attach') . '/' . $first . '/' . $i->temp);
					@unlink(Config::get('storage.temp') . '/' . $i->temp);
				}

			return $redirect_failed
				->withInput()
				->withErrors($document->validator())
				->with('UPDATE.FAIL', true);
		} catch (Throwable $e) {
			throw $e;
		}

		return $redirect_success
			->with('doc_id', $document->id)
			->with('UPDATE.OK', true);
	}

	static function filterDocument()
	{
		$q = Input::get('q');
		$received_from_id = array();

		$clientfrom = ClientModel::where('name', 'like', "%{$q}%")->get();
		foreach ($clientfrom as $i)
			$received_from_id[] = $i->id;

		$documents = DocumentModel::where('agenda_number', 'like', "%{$q}%")
			->orWhere('document_number', 'like', "%{$q}%")
			->orWhere('about', 'like', "%{$q}%")
			->orWhere('received_from', 'like', "%{$q}%")
			->orderBy('created_at', 'DESC')->orderBy('document_date', 'DESC');

		if (count($received_from_id) > 0)
			$documents = $documents->orWhereIn('received_from_id', $received_from_id);

		$doc = array();
		foreach ($documents->paginate(3) as $key => $i) {
			if($i->agenda_number == null)
				$i->agenda_number = '-';
			if($i->document_number == null)
				$i->document_number = '-';
			if($i->about == null)
				$i->about = '-';
			$doc[$key] = $i->transformToArray(array('id', 'agenda_number', 'document_number', 'about', 'received_from'));

			if ($i->received_from_id || $i->received_from_id != 0)
				$doc[$key]['received_from'] = $i->fromClient->name;
		}

		return Response::json(array(
			'doc' => $doc,
			'documents' => $documents->paginate(3)->appends(array('q' => $q))->links()->render()
		));
	}

	static function uploadFile()
	{
		$file = Input::file('file_name');

		$file_path = IceDuren\Storage::random('storage.temp', $file->getClientOriginalExtension());
		$temp = array(
			'name' => $file->getClientOriginalName(),
			'temp' => $file_path[1],
			'ext' => $file->getClientOriginalExtension(),
			'size' => $file->getSize(),
			'type' => Input::get('type')
		);

		if (!$file->move($file_path[0], $file_path[1]))
			return false;

		return Response::json($temp);
	}

	public function removeTempFile()
	{
		$file = Input::get('file_name');
		@unlink(storage_path() . '/temp/' . $file);
	}

	public function downloadFile($id)
	{
		$actfile = DocumentActionFileModel::find($id);
		if (!$actfile)
			App::abort('404');

		$resp = Response::download($actfile->getFullPath())
			->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $actfile->originalname);

		$resp->headers->set('X-Content-Type-Options', 'nosniff');
    	return $resp;
	}

	public function addComment($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		$comment = Input::get('comment');
		$filename = json_decode(Input::get('file'));
		$ori = $document->getOriginal('stage');

		if (!$document->saveWithAction('CMT', $ori ,$comment, $filename)) {
			if (count($filename) > 0)
				foreach ($filename as $i) {
					$first = strtolower(substr($i->temp, 0, 1));
					@unlink(Config::get('storage.doc_attach') . '/' . $first . '/' . $i->temp);
					@unlink(Config::get('storage.temp') . '/' . $i->temp);
				}

			return Redirect::action('DocumentController@showDetail', array($id))
				->with('UPDATE.FAIL', true);
		}

		return Redirect::action('DocumentController@showDetail', array($id))
			->with('UPDATE.OK', true);
	}

	public function changeState($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		$token = auth_token();

		$ori = $document->stage;
		$stage = Input::get('stage');

		try {
	   		if ($document->stage == $stage)
	   			throw new RollbackException('failed to save');

			DB::transaction(function() use ($document, $stage, $ori, $id) {
				$document->stage = $stage;
				if (!$document->checkAuthorization(Auth::user()))
					throw new AuthorizationException('failed to save');

				if ($stage == "PRFNS" || $stage == "PRDSC" || $stage == "NPFNS" || $stage == "NPDSC" )
					$document->is_done = 1;

				if (!$document->save())
					throw new DataRequiredException('failed to save');

				$chg_disposition = false;
				if ($document->is_product) {
					$dispositions = array();
					$depts = DepartmentModel::where('disposition_product', '=', 1)->get();
					//old disposition
					foreach ($document->docDisposition as $i)
						$dispositions[$i->id] = array('notes' => $i->pivot->notes);

					//diposition product
					foreach ($depts as $i)
						if (!isset($dispositions[$i->id])) {
							$dispositions[$i->id] = array('notes' => null);
							$chg_disposition = true;
						}

					if (!$document->docDisposition()->sync($dispositions))
						throw new RollbackException('failed to save');
				}

				$document = DocumentModel::find($id);
				if(!$document->saveWithAction("DOC", $ori, null , null, $chg_disposition))
					throw new RollbackException('failed to save');

				//email notif intern
				$document->send_notif_intern($ori);

				// email notification
				switch ("{$ori}:{$stage}") {
				case 'PRDLV:PRASG':
					$document->send_notif();
					break;
				case 'PRANS:PRFNS':
					$document->send_notif();
					break;
				}
			});
		} catch (AuthorizationException $e) {
			if ($token)
				return Redirect::action('DocumentController@mobileStageChangeFail', array($document->id));

			return Redirect::action('DocumentController@showDetail', array($document->id))
				->with('UPDATE.FAIL', true);
		} catch (DataRequiredException $e) {
			if ($token)
				return Redirect::action('DocumentController@mobileStageChangeFail', array($document->id))
					->withErrors($document->validator());

			return Redirect::action('DocumentController@showDetail', array($document->id))
				->with('UPDATE.FAIL', true)
				->withErrors($document->validator());
		} catch (RollbackException $e) {
			if ($token)
				return Redirect::action('DocumentController@mobileStageChangeFail', array($document->id));

			return Redirect::action('DocumentController@showDetail', array($document->id))
				->with('UPDATE.FAIL', true);
		}

		if ($token)
			return Redirect::action('DocumentController@mobileStageChangeSuccess', array($document->id));

		return Redirect::action('DocumentController@showDetail', array($document->id))
			->with('UPDATE.OK', true);
	}

	public function saveUserTrack($id)
	{
		$user = Auth::user();
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		if (!$document->user->contains($user->id))
			$document->user()->attach($user->id);
		else
			return Redirect::action('HomeController@showHome')
				->with('UPDATE.FAIL', true);

		return Redirect::action('HomeController@showHome')
			->with('UPDATE.OK', true);
	}

	public function apiListClient()
	{
		$clients = ClientModel::orderBy('name')->get();

		$res = [];

		foreach ($clients as $c) {
			$res[] = [
				'id' => $c->id,
				'name' => $c->name,
			];
		}

		return Response::json($res);
	}

	public function printDisposition($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		$directive = array();
		$departments = DepartmentModel::get();
		foreach ($document->directive as $key => $i) {
			$directive[] = $i->transformToArray(array('id', 'directive'));
		}

		return View::make('print_disposition')
			->with('document', $document)
			->with('departments', $departments)
			->with('directive', $directive);
	}

	public function showPublic($id, $hash)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		if ($document->getPublicKey() != $hash)
			App::abort('404');

		return View::make('detail_document_public')
			->with('document', $document);
	}

	public function downloadPublicFile($id, $file_id, $hash)
	{
		$actfile = DocumentActionFileModel::find($file_id);
		if (!$actfile)
			App::abort('404');

		if ($actfile->getPublicKey() != $hash)
			App::abort('404');

		if ($actfile->type != 'PRO')
			App::abort('404');

		$resp = Response::download($actfile->getFullPath())
			->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $actfile->originalname);

		$resp->headers->set('X-Content-Type-Options', 'nosniff');
    	return $resp;
	}

	public function printLog($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		return View::make('print_log')
			->with('document', $document);
	}

	public function apiDocumentDetail($id)
	{
		$document = DocumentModel::find($id);

		if (!$document)
			App::abort('404');

		$to_client = array();
		foreach ($document->toClient as $i) {
			$to_client[] = $i->name;
		}

		$disposition = array();
		foreach ($document->docDisposition()->get() as $dept) {
			$disposition[] = $dept->name;
		}

		$token = Input::get('_token');
		$action = array();
		foreach ($document->action as $act) {
			$x = array();
			foreach ($act->file as $file) {
				if ($file->type == 'INB')
					$type = 'Surat Masuk';
				elseif ($file->type == 'PRO')
					$type = 'Produk';
				elseif ($file->type == 'OTH')
					$type ='Lain-Lain';
				else
					$type = $file->type;

				$hash = $file->generateHash($token);
		 		$x[] = array(
		 			'original_name' => $file->originalname,
		 			'type' => $type,
		 			'url' => action('DocumentController@downloadFile', array($file->id)),
		 			'app_url' => action('DocumentController@apiDownloadFile', array($file->id, $hash, '_token' => $token))
		 		);
			}

			$i = array(
				'employee_name' => $act->employee_name,
				'employee_depname' => $act->employee_depname,
				'created_at' => $act->created_at->format('c'),
				'notes' => $act->notes,
				'file' => $x
			);
			$action[] = $i;
		}

		$actionbar_html = View::make('mobile.content_wrapper')
			->with('content', $document->renderActionBarWithToken($token))
			->render();

		$doc = array(
			'id' => $document->id,
			'agenda_number' => $document->agenda_number,
			'about' => $document->about,
			'received_at' => $document->received_at->format('c'),
			'is_copy' => $document->is_copy,
			'percent_deadline' => $document->calcPercentageDeadline(),
			'document_number' => $document->document_number,
			'document_date' => $document->document_date->format('c'),
			'received_from' => $document->received_from_id ? $document->fromClient->name : $document->received_from,
			'to_client' => $to_client,
			'enclosure' => $document->enclosure,
			'urgency' => $document->defineUrgency(),
			'clasification' => $document->defineClasification(),
			'assigned_dep' => $document->assignedDept ? $document->assignedDept->name : null,
			'disposition' => $disposition,
			'target_date' => $document->target_date ? $document->target_date->format('c') : null,
			'assigned_emp' => $document->assigned_emp ? $document->assigned->name : "-",
			'assigned_emp_dept' => $document->assigned_emp && $document->assigned->dept ? $document->assigned->dept->name : null,
			'subtitute_emp' => $document->substitute_emp ? $document->substitute->name : "-",
			'substitute_emp_dept' => $document->substitute_emp && $document->substitute->dept ? $document->substitute->dept->name : null,
			'official_memo' => $document->official_memo ,
		    'is_product' => $document->is_product,
			'memo_date' => $document->memo_date ? $document->memo_date->format('c') : null ,
			'action_log' => $action,
			'stage_desc' => $document->stageDesc,
			'actionbar_html' => $actionbar_html
		);

		return Response::json($doc);
	}

	public function apiNotification()
	{
		$token = Input::get('_token');
		if (strlen($token) == 0) {
			return Response::json([
				'status' => 'FAIL',
				'error' => 'REQUEST_ERROR',
				'reason' => 'token is not specified'
			]);
		}

		$user = Auth::user();
		$notifications = $user->notif()->orderBy('created_at', 'desc')->paginate(10);

		$ret = $notifications->map(function ($i) {
			$action = $i->action;
			return [
				'id' => $i->id,
				'document_id' => $action->doc_id,
				'agenda_number' => $action->document->agenda_number,
				'about' => $action->document->about,
				'employee_name' => $action->employee->name,
				'employee_dep' => $action->employee->dept ? $action->employee->dept->name : null,
				'notes' => $action->notes,
				'read' => $i->read,
				'created_at' => $action->created_at->format('c'),
			];
		});

		return Response::json($ret);
	}

	public function apiReadNotification()
	{
		$token = Input::get('_token');
		if (strlen($token) == 0) {
			return Response::json([
				'status' => 'FAIL',
				'error' => 'REQUEST_ERROR',
				'reason' => 'token is not specified'
			]);
		}

		$notif_id = Input::get('notif_id');
		if (is_array($notif_id) && count($notif_id) > 0) {
			if (!NotificationModel::whereIn('id', $notif_id)->update(['read' => true]))
				return Response::json([
					'status' => 'FAIL',
					'error' => 'INTERNAL_ERROR',
					'reason' => 'Internal error, please try again'
				]);
		}

		return Response::json([
			'status' => 'OK',
			'error' => 'NO_ERR',
			'reason' => 'All OK',
		]);
	}

	public function apiDocument()
	{
		$list = $this->showList()->getData();
		$documents = $list['documents']->toArray();

		//add percentage deadline
		foreach ($documents['data'] as &$document) {
			$i = DocumentModel::find($document['id']);
			$document['percent_deadline'] = $i->calcPercentageDeadline();
			$document['target_date'] = $i->target_date ? $i->target_date->format('c') : null;
			$document['received_from'] = $i->received_from_id ? $i->fromClient->name : $i->received_from;
		}

		return Response::json($documents);
	}

	public function showMonitorDocument()
	{
		$documents = DocumentModel::where('is_done', '=', 0)
			->where(function($query) {
				$query->where('is_product', '=', 1)->orWhere(function ($query) {
					$query->where('is_product', '=', 0)->whereNotNull('target_date');
				});
			})->orderBy('target_date');

		if (!(Auth::user()->dept && Auth::user()->dept->disposition_product) && Auth::user()->level == JobLevelModel::KASUBAG) {
			$documents = $documents->where('assigned_dep', '=', Auth::user()->department)
				->whereIn('stage', ['PRDLV', 'PRASG', 'PRPRO', 'PRDAP', 'NPDLV', 'NPASG', 'NPPRO', 'NPDAP']);
		}

		$documents = $documents->paginate(40);

		return View::make('monitor_document')
			->with('documents', $documents);
	}

	public function apiDownloadFile($id, $hash)
	{
		$token = Input::get('_token');
		$check_hash = base64_encode(hash_hmac('sha256', $id  . '|' . $token, Config::get('app.hmac_key'), true));
		$check_hash = strtr($check_hash, ['/' => '_', '+' => '-', '=' => '~']);

		if ($hash != $check_hash)
			App::abort('404');

		$actfile = DocumentActionFileModel::find($id);
		if (!$actfile)
			App::abort('404');

		$resp = Response::download($actfile->getFullPath())
			->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $actfile->originalname);

		$resp->headers->set('X-Content-Type-Options', 'nosniff');
    	return $resp;
	}

	public function apiComment($id)
	{
		$document = DocumentModel::find($id);
		if (!$document)
			App::abort('404');

		$comment = Input::get('comment');
		$ori = $document->getOriginal('stage');

		if (!$document->saveWithAction('CMT', $ori ,$comment))
			return Response::json([
				'status' => 'FAIL',
				'error' => 'INTERNAL_ERROR',
				'reason' => 'Internal error, please try again'
			]);

		return Response::json([
			'status' => 'OK',
			'error' => 'NO_ERROR',
			'reason' => 'All OK'
		]);
	}

	public function mobileStageChangeFail($id)
	{
		return View::make('mobile.stage_change_fail');
	}

	public function mobileStageChangeSuccess($id)
	{
		return View::make('mobile.stage_change_success');
	}

	public function displayMonitor()
	{
		$list_dep = DepartmentModel::select('id', 'name') -> where('display_monitor','=','1') -> take(4)-> get();
		$jumbotrons = JumbotronModel::where('state', 'PUB')->orderBy('show_order')->take(3)->get();
		//var_dump($jumbotrons);die;
		return View::make('monitor.display')
			->with('list_dep', $list_dep)
			->with('jumbotrons', $jumbotrons);
	}

	public function displayMonitorAjax($id)
	{
		$model = DepartmentModel::find($id);
		if ($model)
			$doc_list = $model->getDisplayDocumentList();
		else
			$doc_list = [];

		return View::make('monitor.document')
			->with('doc_list', $doc_list);
	}
}
