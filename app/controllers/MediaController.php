<?php

use Carbon\Carbon;

class MediaController extends BaseController {

	public function showNewForm()
	{
		return View::make('new_media');
	}

	public function createNew()
	{
		$media = new MediaModel();
		$file = Input::file('media');
		$file_name = null;
		$destinationPath = 'media/' . Carbon::now()->year;
		
		$try = 250;
		do {
			if ($try <= 0)
				throw Exception("Storage::random fails to produce randomized filename");
			
			$hash = Carbon::now()->year."-".Carbon::now()->month."-".str_random(6).".".$file->getClientOriginalExtension();

			$file_name = $hash;
			$file_check = $destinationPath . '/' . $hash;
			$try -= 1;
		} while (file_exists($file_check));

		$uploadSuccess = $file->move($destinationPath, $file_name);

		$media->fill([
			'name' => $file->getClientOriginalName(),
			'filename' => $file_name,
			'upload_at' => Carbon::now()
		]);

		if (!$media->save()){
			@unlink($destinationPath . '/' . $file_name);
			return Redirect::action('MediaController@showList')
				->withErrors($media->validator())
				->withInput()
				->with('UPDATE.FAIL', true);
		}
		
		return Redirect::action('MediaController@showList')
			->with('UPDATE.OK', true);
	}

	public function showList()
	{
		$media = MediaModel::all();

		return View::make('list_media')->with("media", $media);
	}

	public function showEditForm($id)
	{
		$dep = DepartmentModel::find($id);
		if (!$dep)
			App::abort(404);

		return View::make('edit_department')->with("dep",$dep);
	}

	public function updateRow($id)
	{
		$media = MediaModel::findOrFail($id);
		$media->name = $_POST['name'];
		
		if (!$media->save())
			return Redirect::action('MediaController@showEditForm', array($id))
				->withErrors($media->validator())
				->withInput()
				->with('UPDATE.FAIL', true);

		return Redirect::action('MediaController@showList')
			->with('UPDATE.OK', true);
	}

	public function deleteRow($id)
	{
		$media = MediaModel::findOrFail($id);
		$path = 'media/' . $media->upload_at->year . '/' . $media->filename;

		if ($media->delete())
			@unlink($path);

		return Redirect::action('MediaController@showList')
			->with('DELETE.OK', true);
	}
}
