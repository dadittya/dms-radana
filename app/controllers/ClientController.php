<?php

class ClientController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showNewForm()
	{
		return View::make('new_client');
	}

	public function createNew()
	{
		$client = new ClientModel();
		$input = array (
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'email' => Input::get('email'),
			'phone' => Input::get('phone')
		);

		$client->fill($input);
		if(!$client->save())
			return Redirect::action('ClientController@showNewForm')
				->withInput()
				->withErrors($client->validator())
				->with('UPDATE.FAIL', true);

		return Redirect::action('ClientController@showList')
			->with('UPDATE.OK', true);
	}

	public function showList()
	{
		$view = View::make('list_client');

		if ($q = Input::get('q')) {
			$clients = ClientModel::where('name', 'like', "%{$q}%")
				->orderBy('name')
				->paginate(25);
			$view->with('q', $q);
		} else {
			$clients = ClientModel::orderBy('name')->paginate(25);
		}

		return $view->with('clients', $clients);
	}

	public function showEditForm($id)
	{
		$client = ClientModel::find($id);
		if (!$client)
			App::abort('404');

		return View::make('edit_client')
			->with('client', $client);
	}

	public function updateRow($id)
	{
		$client = ClientModel::find($id);
		if (!$client)
			App::abort('404');

		$input = array (
			'name' => Input::get('name'),
			'address' => Input::get('address'),
			'email' => Input::get('email'),
			'phone' => Input::get('phone')
		);

		$client->fill($input);
		if(!$client->save())
			return Redirect::action('ClientController@showEditForm', array($id))
				->withInput()
				->withErrors($client->validator())
				->with('UPDATE.FAIL', true);

		return Redirect::action('ClientController@showList')
			->with('UPDATE.OK', true);
	}
}