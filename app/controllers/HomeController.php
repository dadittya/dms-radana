<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showHome()
	{
		$user = Auth::user();
		$docs = static::getDocuments($user);		

		return View::make('home')
			->with('documents', $docs)
			->with('now', \Carbon\Carbon::now());
	}

	static function getDocuments($user)
	{
		$docs = DocumentModel::where('is_done', '=', 0)
			->orderBy(DB::raw('-target_date'), 'DESC')
			->orderBy('is_product', 'DESC');

		if ($user->level == JobLevelModel::KASUBAG) {
			$docs->where(function ($query) use ($user) {
				$query->where('assigned_dep', '=', $user->department);
				$query->orWhereRaw(
					"EXISTS (SELECT * FROM docdisposition_department AS disp WHERE documents.id = disp.doc_id AND disp.dept_id = {$user->department})"
				);
			})->whereNotIn('stage', array('__NEW', 'PRAPP', 'NPAPP'));
			
		} elseif ($user->level == JobLevelModel::STAF) {
			$user_id = $user->id;
			$docs = $docs->where(function ($query) use ($user_id) {
                $query->where('assigned_emp', '=', $user_id)
                      ->orWhere('substitute_emp', '=', $user_id)
                      ->orWhereRaw(
            			"EXISTS (SELECT * FROM doccarboncopy_employee AS cc WHERE documents.id = cc.doc_id AND cc.emp_id = {$user_id})"
            	);
            })->whereNotIn('stage', array('__NEW', 'PRAPP', 'PRDLV','NPAPP', 'NPDLV'));
		}

		$docs = $docs->get();
		$docs = DocumentModel::sortByPercentage($docs);

		return $docs;
	}

	public function apiDashboard()
	{
		$token = Input::get('_token');
		if (strlen($token) == 0) {
			return Response::json([
				'status' => 'FAIL',
				'error' => 'REQUEST_ERROR',
				'reason' => 'token is not specified'
			]);
		}

		$user = Auth::user();
		$docs = static::getDocuments($user);

		$data = array();
		foreach ($docs as $doc) {
			$x = array();
			foreach ($doc->action as $action) {
			 	foreach ($action->file as $file) {
			 		if ($file->type == 'INB')
						$type = 'Surat Masuk';
					elseif ($file->type == 'PRO')
						$type = 'Produk';
					elseif ($file->type == 'OTH')
						$type ='Lain-Lain';
					else
						$type = $file->type;

			 		$x[] = array(
			 			'original_name' => $file->originalname,
			 			'type' => $type,
			 			'url' => action('DocumentController@downloadFile', array($file->id))
			 		);
			 	}
			}

			$i = array(
				'id' => $doc->id,
				'agenda_number' => $doc->agenda_number,
				'document_number' => $doc->document_number,
				'about' => $doc->about,
				'is_copy' => $doc->is_copy,
				'received_from' => $doc->received_from_id ? $doc->fromClient->name : $doc->received_from,
				'last_action_at' => $doc->last_action()->created_at->format('c'),
				'last_action_by' => $doc->last_action()->employee->name,
				'target_date' => $doc->target_date ? $doc->target_date->format('c') : null,
				'percent_deadline' => $doc->calcPercentageDeadline(),
				'diff_target' => $doc->target_date < \Carbon\Carbon::now() ? 0 : $doc->target_date->diffInDays(\Carbon\Carbon::now()) + 1,
				'is_product' => $doc->is_product,
				'file' => $x,
			);

			$data[] = $i;
		}

		return Response::json($data);
	}

	public function showAbout()
	{
		return View::make('about');
	}
}
