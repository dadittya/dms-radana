<?php

class JobTitleController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showNewForm()
	{
		$job_level = JobLevelModel::lists('name', 'id');
		return View::make('new_jobtitle')
			->with('job_level', $job_level);
	}

	public function createNew()
	{
		$jobtitle = new JobTitleModel();
		$jobtitle->name = Input::get('name');
		$jobtitle->level = Input::get('level');
		
		if (!$jobtitle->save())
			return Redirect::action('JobTitleController@showNewForm')
				->withErrors($jobtitle->validator())
				->withInput()
				->with('UPDATE.FAIL', true);
		
		return Redirect::action('JobTitleController@showList')
			->with('UPDATE.OK', true);
	}

	public function showList()
	{
		$jobtitles = JobTitleModel::paginate(25);

		return View::make('list_jobtitle')->with("jobtitles",$jobtitles);
	}

	public function showEditForm($id)
	{
		$job_level = JobLevelModel::lists('name', 'id');
		$jobtitle = JobTitleModel::find($id);
		if (!$jobtitle)
			App::abort(404);

		return View::make('edit_jobtitle')
			->with('jobtitle', $jobtitle)
			->with('job_level', $job_level);
	}

	public function updateRow($id)
	{
		$jobtitle = JobTitleModel::find($id);
		if (!$jobtitle)
			App::abort(404);

		$jobtitle->name = Input::get('name');
		$jobtitle->level = Input::get('level');
		
		if (!$jobtitle->save())
			return Redirect::action('JobTitleController@showEditForm', array($id))
				->withErrors($jobtitle->validator())
				->withInput()
				->with('UPDATE.FAIL', true);

		return Redirect::action('JobTitleController@showList')
			->with('UPDATE.OK', true);
	}

	public function deleteRow($id)
	{
		$jobtitle = JobTitleModel::find($id);
		if (!$jobtitle)
			App::abort(404);

		if (count($jobtitle->employee) > 0 || $jobtitle->system_jobtitle == 1 || !$jobtitle->delete())
			return Redirect::action('JobTitleController@showList')
				->with('DELETE.FAIL', true);

		return Redirect::action('JobTitleController@showList')
			->with('DELETE.OK', true);
	}
}
