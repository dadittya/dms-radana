<?php

class EmployeeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showNewForm()
	{
		$jobtitle = JobTitleModel::orderBy('name')->lists('name', 'id');
		$department = DepartmentModel::orderBy('name')->lists('name', 'id');
		$department[0] = null; 
		ksort($department);

		return View::make('new_employee')
			->with('department', $department)
			->with('jobtitle', $jobtitle);
	}

	public function createNew()
	{

		$emp = new EmployeeModel();

		$rules = $emp->getCreateRules();
		$rules += array(
			'password_confirmation' => 'required|same:password',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
			return Redirect::action('EmployeeController@showNewForm')
				->withErrors($validator)
				->withInput()
				->with('UPDATE.FAIL', true);
		
		$emp->name = Input::get('name');
		$emp->email = Input::get('email');
		$emp->nip = Input::get('nip');
		$emp->password = Hash::make(Input::get('password'));
		$emp->department = Input::get('department');
		$emp->job_title = Input::get('job_title');
		$emp->active = Input::get('active');
		$emp->email_notif = Input::get('email_notif');
		
		if (!$emp->save())
			return Redirect::action('EmployeeController@showNewForm')
				->withErrors($emp->validator())
				->withInput()
				->with('UPDATE.FAIL', true);

		return Redirect::action('EmployeeController@showList')
			->with('UPDATE.OK', true);
	}

	public function showList()
	{
		$view = View::make('list_employees');
		$emps = EmployeeModel::orderBy('active', 'desc')->orderBy('name');

		if ($q = Input::get('q')) {
			$emps = $emps->where('name', 'like', "%{$q}%");
			$view = $view->with('q', $q);
		}
		
		$emps = $emps->paginate(25);

		return $view->with("emps",$emps);
	}

	public function showEditForm($id)
	{
		$emp = EmployeeModel::find($id);
		if (!$emp) 
			App::abort(404);
		
		$jobtitle = JobTitleModel::orderBy('name')->lists('name', 'id');
		$department = DepartmentModel::orderBy('name')->lists('name', 'id');
		$department[0] = null; 
		ksort($department);

		return View::make('edit_employee')
			->with("emp",$emp)
			->with('department', $department)
			->with('jobtitle', $jobtitle);
	}

	public function updateRow($id)
	{
		$emp = EmployeeModel::find($id);
		if (!$emp) 
			App::abort(404);

		//change password
		$check_chgpass = Input::get('chg_pass');
		if ($check_chgpass) {
			$rules = $emp->getCreateRules();
			$rules += array(
				'password_confirmation' => 'required|same:password',
			);

			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails())
				return Redirect::action('EmployeeController@showEditForm', array($id))
					->withErrors($validator)
					->withInput()
					->with('UPDATE.FAIL', true);

			$emp->password = Hash::make(Input::get('password'));
		}

		$emp->name = Input::get('name');
		$emp->email = Input::get('email');
		$emp->nip = Input::get('nip');
		$emp->department = Input::get('department');
		$emp->job_title = Input::get('job_title');
		$emp->active = Input::get('active');
		$emp->email_notif = Input::get('email_notif');
			
		if(!$emp->save())
			return Redirect::action('EmployeeController@showEditForm', array($id))
				->withErrors($emp->validator())
				->withInput()
				->with('UPDATE.FAIL', true);

		return Redirect::action('EmployeeController@showList')
			->with('UPDATE.OK', true);
	}

	public function filterEmployee()
	{
		$q = Input::get('q');
		$dept = Input::get('dept');
		$emp = array();

		$employees = EmployeeModel::where('name', 'like', "%{$q}%")->paginate(5);
		if ($dept != null) {
			if ($dept == 0)
				$dept = null;

			$employees = EmployeeModel::where('department', '=', $dept)
				->where('name', 'like', "%{$q}%")->paginate(5);
		}

		foreach ($employees as $employee) {
			$emp[] = array(
				'id' => $employee->id,
				'name' => $employee->name,
				'dept_id' => $employee->department,
				'department' => $employee->dept ? $employee->dept->name : '-',
				'job_title' => $employee->jobtitle->name
			);
		}

		return Response::json(array(
			'emp' => $emp, 
			'employees' => $employees->appends(array('q' => $q))->links()->render(),
		));
	}
}
