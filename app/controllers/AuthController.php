<?php

class AuthController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLoginForm()
	{
		$next_url = Input::get('next_url');

		if (strlen($next_url) == 0)
			$next_url = action('HomeController@showHome');

		if (Auth::check())
			return Redirect::to($next_url);
		
		return View::make('login_form')
			->with('next_url', $next_url);
	}

	public function authLogin()
	{	
		$next_url = Input::get('next_url');
		
		if (strlen($next_url) == 0)
			$next_url = action('HomeController@showHome');

		if (Auth::attempt(array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'active' => 1
		))) {

			$user = Auth::user();

			return Redirect::to($next_url);
		}

		return Redirect::action('AuthController@showLoginForm');
	}

	public function authLogout()
	{
		Auth::logout();
		return Redirect::action('AuthController@showLoginForm');
	}

	public function apiLogin()
	{
		if (strlen(Input::get('client_id')) == 0 ||
			strlen(Input::get('client_name')) == 0) {
			return Response::json([
				'status' => 'FAIL',
				'error' => 'REQUEST_ERROR',
				'reason' => 'client_id and client_name is not specified'
			]);
		}

		if (Auth::attempt(array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'active' => 1
		))) {

			$user = Auth::user();

			EmployeeTokenModel::where('employee_id', '=', $user->id)
				->where('client_id', '=', Input::get('client_id'))
				->where('client_name', '=', Input::get('client_name'))
				->delete();

			$token = new EmployeeTokenModel();
			$token->fill([
				'employee_id' => $user->id,
				'client_id' => Input::get('client_id'),
				'client_name' => Input::get('client_name'),
				'token' => Str::random(64),
				'gcm_token' => Input::get('gcm_token')
			]);

			if (!$token->save()) {
				Auth::logout();

				return Response::json([
					'status' => 'FAIL',
					'error' => 'INTERNAL_ERROR',
					'reason' => 'Internal error, please try again'
				]);
			}

			return Response::json([
				'status' => 'OK',
				'error' => 'NO_ERR',
				'reason' => 'All OK',
				'_token' => $token->token
			]);
		}

		return Response::json([
			'status' => 'FAIL',
			'error' => 'AUTH_FAIL',
			'reason' => 'Authentication Failed'
		]);
	}

	public function apiLogout()
	{
		$token = Input::get('_token');
		if (strlen($token) == 0) {
			return Response::json([
				'status' => 'FAIL',
				'error' => 'REQUEST_ERROR',
				'reason' => 'token is not specified'
			]);
		}

		if (!EmployeeTokenModel::where('token', '=', $token)->delete())
			return Response::json([
				'status' => 'FAIL',
				'error' => 'INTERNAL_ERROR',
				'reason' => 'Internal error, please try again'
			]);

		return Response::json([
			'status' => 'OK',
			'error' => 'NO_ERROR',
			'reason' => 'All OK'
		]);
	}

	public function apiUpdateGcm()
	{
		$update = EmployeeTokenModel::where('token', '=', Input::get('_token'))
			->update(['gcm_token' => Input::get('gcm_token')]);

		if (!$update)
			return Response::json([
				'status' => 'FAIL',
				'error' => 'INTERNAL_ERROR',
				'reason' => 'Internal error, please try again'
			]);

		return Response::json([
			'status' => 'OK',
			'error' => 'NO_ERROR',
			'reason' => 'All OK'
		]);
	}
}
