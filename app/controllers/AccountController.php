<?php

class AccountController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showChangePassword()
	{
		return View::make('account_password');
	}

	public function doChangePassword()
	{
		$acc = Auth::user();

		$rules = array(
			'old_password' => 'required|utf8',
			'password' => 'required|utf8',
			'password_confirmation' => 'required|same:password',
		);

		$v = Validator::make(Input::all(), $rules);
		if (!$v->passes())
			return Redirect::action('AccountController@showChangePassword')
				->withErrors($v)
				->with('UPDATE.FAIL', true);

		if (!$acc->checkPassword(Input::get('old_password'))) {
			$messages = new \Illuminate\Support\MessageBag;
			$messages->add('old_password', 'Password salah');
			return Redirect::action('AccountController@showChangePassword')
				->withErrors($messages)
				->with('PASSWORD.WRONG', true);
		}

		$acc->password = Hash::make(Input::get('password'));

		if (!$acc->save())
			return Redirect::action('AccountController@showChangePassword')
				->with('UPDATE.FAIL', true);

		return Redirect::action('AccountController@showChangePassword')
				->with('UPDATE.OK', true);	
	}
}