<?php

class DepartmentTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (App::environment('dev') || App::runningUnitTests()) {
			$department = new DepartmentModel();
			$department->name = 'Umum';
			$department->display_monitor = 1;
			$department->save();

			$department = new DepartmentModel();
			$department->name = 'Piutang';
			$department->display_monitor = 1;
			$department->save();

			$department = new DepartmentModel();
			$department->name = 'Hukum dan Informasi';
			$department->display_monitor = 1;
			$department->save();
			
			$department = new DepartmentModel();
			$department->name = 'Pelayanan Lelang';
			$department->display_monitor = 1;
			$department->save();
		}
	}

}