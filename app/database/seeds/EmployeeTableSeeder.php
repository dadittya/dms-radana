<?php

class EmployeeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$dep = DepartmentModel::orderBy('id')->first();
		$title = JobTitleModel::orderBy('id')->first();

		$employee = new EmployeeModel();
		$employee->name = 'Admin';
		$employee->email = 'admin@example.com';
		$employee->nip = Str::random(10);
		$employee->password = Hash::make('admin123');
		$employee->job_title = $title->id;
		$employee->active = 1;
		$employee->save();

		if (App::environment('local')) {
			$employee = new EmployeeModel();
			$employee->name = 'Gianeka';
			$employee->email = 'ravenwing793@gmail.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('LtVrf3vP');
			$employee->job_title = $title->id+1;
			$employee->active = 1;
			$employee->save();
		}

		if (App::environment('dev') || App::runningUnitTests()) {
			$employee = new EmployeeModel();
			$employee->name = 'Gianeka';
			$employee->email = 'gianeka@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->job_title = $title->id+1;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Deka';
			$employee->email = 'deka@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Ningsih';
			$employee->email = 'ningsih@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Paimin';
			$employee->email = 'paimin@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Paijoh';
			$employee->email = 'paijoh@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id;
			$employee->job_title = $title->id+2;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Jacky';
			$employee->email = 'jacky@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Yashmine';
			$employee->email = 'yashmine@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+1;
			$employee->job_title = $title->id+4;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Lupus';
			$employee->email = 'lupus@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+1;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Serena';
			$employee->email = 'serena@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Jimmy Dash';
			$employee->email = 'jimmy@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Zach';
			$employee->email = 'zach@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+2;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Arina';
			$employee->email = 'arina@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+2;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Durenworks';
			$employee->email = 'duren@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->job_title = $title->id+5;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Auditor';
			$employee->email = 'auditor@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->job_title = $title->id+8;
			$employee->active = 1;
			$employee->save();
			
			// NEW DEPS - ID 3
			$employee = new EmployeeModel();
			$employee->name = 'Zacher';
			$employee->email = 'zacher@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+3;
			$employee->job_title = $title->id+2;
			$employee->active = 1;
			$employee->save();

			$employee = new EmployeeModel();
			$employee->name = 'Arina Grande';
			$employee->email = 'arina.grande@example.com';
			$employee->nip = Str::random(10);
			$employee->password = Hash::make('admin123');
			$employee->department = $dep->id+3;
			$employee->job_title = $title->id+3;
			$employee->active = 1;
			$employee->save();
			
		}
	}

}