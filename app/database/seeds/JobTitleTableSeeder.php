<?php

class JobTitleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{	
		$jobtitle = new JobTitleModel();
		$jobtitle->id = 1;
		$jobtitle->name = 'Kepala Kantor';
		$jobtitle->system_jobtitle = 1;
		$jobtitle->level = 1;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 2;
		$jobtitle->name = 'Sekretaris';
		$jobtitle->system_jobtitle = 1;
		$jobtitle->level = 2;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 3;
		$jobtitle->name = 'Kepala SubBagian/Seksi';
		$jobtitle->system_jobtitle = 1;
		$jobtitle->level = 3;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 4;
		$jobtitle->name = 'Pelaksana';
		$jobtitle->system_jobtitle = 1;
		$jobtitle->level = 4;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 5;
		$jobtitle->name = 'Kepala Seksi';
		$jobtitle->system_jobtitle = 0;
		$jobtitle->level = 3;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 6;
		$jobtitle->name = 'Durenworks Engineer';
		$jobtitle->system_jobtitle = 1;
		$jobtitle->level = 5;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 7;
		$jobtitle->name = 'Bendahara Pengeluaran';
		$jobtitle->system_jobtitle = 0;
		$jobtitle->level = 4;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 8;
		$jobtitle->name = 'Bendahara Penerimaan';
		$jobtitle->system_jobtitle = 0;
		$jobtitle->level = 4;
		$jobtitle->save();

		$jobtitle = new JobTitleModel();
		$jobtitle->id = 9;
		$jobtitle->name = 'Auditor';
		$jobtitle->system_jobtitle = 0;
		$jobtitle->level = 6;
		$jobtitle->save();		
	}

}