<?php

class ClientTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{	
		if (!App::environment('dev') && !App::runningUnitTests())
			return;

		$client = new ClientModel();
		$client->name = 'Bank BNI';
		$client->address = 'Jl Jambu 22';
		$client->email = 'bni@example.com';
		$client->phone = '0222321';
		$client->save();

		$client = new ClientModel();
		$client->name = 'Bank BRI';
		$client->address = 'Jl Semangka 10';
		$client->email = 'bri@example.com';
		$client->phone = '0252162';
		$client->save();

		$client = new ClientModel();
		$client->name = 'Dinas Pajak';
		$client->address = 'Jl Rambutan 23';
		$client->email = 'pajak@example.com';
		$client->phone = '02200281';
		$client->save();

		// SEED FOR TESTING PERFORMANCE
		// DEV ENV ONLY
		if (App::environment('dev'))
			$this->seedDummy(25);
	}

	public function seedDummy($rows)
	{
		DB::transaction(function () use ($rows) {
			$f = Faker\Factory::create();
			$used_email = [];
			for($i = 0; $i < $rows; $i++) {
				$c = new ClientModel();
				do {
					$email = $f->email;
				} while (isset($used_email[$email]));
				$used_email[$email] = true;
				$c->fill([
					'name' => $f->name,
					'address' => $f->address,
					'email' => $email,
					'phone' => $f->phoneNumber
				]);
				$c->save();
			}
		});
	}
}