<?php

use \Carbon\Carbon;

class DocumentTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (!App::environment('dev') && !App::runningUnitTests())
			return;

		$first = EmployeeModel::orderBy('id')->first();
		//DOCUMENT-1
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 01, 30),
			'document_number' => Str::random(6),
			'is_copy' => 0,
			'document_date' => Carbon::createFromDate(2015, 01, 29),
			'received_from' => 'Yasmin',
			'about' => 'Lelang Tanah',
			'enclosure' => 'Lorem Ipsum Bla Bla',
			'clasification' => 'RHS',
			'urgency' => 'SGR',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 03),
			'is_product' => 1,
			'clients' => '[{"id":"0","name":"Bank Mandiri","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = $first;
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-2
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 01, 30),
			'document_number' => Str::random(6),
			'is_copy' => 0,
			'document_date' => Carbon::createFromDate(2015, 01, 29),
			'received_from' => 'Takashimura',
			'about' => 'Upacara Pagi',
			'clasification' => 'BIA',
			'urgency' => 'BIA',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 01),
			'is_product' => 1,
			'clients' => '[{"id":"0","name":"Jokowow","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = EmployeeModel::find($first->id+1);
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-3
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 02, 05),
			'document_number' => Str::random(6),
			'is_copy' => 1,
			'document_date' => Carbon::createFromDate(2015, 02, 04),
			'received_from' => 'Pandawa',
			'about' => 'Pertemuan Mingguan',
			'clasification' => 'RHS',
			'urgency' => 'BIA',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 10),
			'is_product' => 0,
			'clients' => '[{"id":"0","name":"Setya","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = EmployeeModel::find($first->id+1);
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-4
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 02, 05),
			'document_number' => Str::random(6),
			'is_copy' => 1,
			'document_date' => Carbon::createFromDate(2015, 02, 04),
			'received_from' => 'Floresta',
			'about' => 'Olahraga Jumat Pagi',
			'clasification' => 'KFD',
			'urgency' => 'SSG',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 06),
			'is_product' => 0,
			'clients' => '[{"id":"0","name":"Universitas Negeri XY","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = $first;
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-5
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 02, 05),
			'document_number' => Str::random(6),
			'is_copy' => 1,
			'document_date' => Carbon::createFromDate(2015, 02, 05),
			'received_from' => 'Pemerintah Pusat',
			'about' => 'Penyambutan Presiden',
			'clasification' => 'BIA',
			'urgency' => 'KLT',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 15),
			'is_product' => 0,
			'clients' => '[{"id":"0","name":"Toko Samudera","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = EmployeeModel::find($first->id+1);
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-6
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 02, 05),
			'document_number' => Str::random(6),
			'is_copy' => 1,
			'document_date' => Carbon::createFromDate(2015, 02, 05),
			'received_from' => 'Kepala Pusat',
			'about' => 'Piknik Keluarga Besar',
			'clasification' => 'BIA',
			'urgency' => 'KLT',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 02, 07),
			'is_product' => 1,
			'clients' => '[{"id":"0","name":"PT. Yamin","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = EmployeeModel::find($first->id+1);
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		//DOCUMENT-7
		$doc = new DocumentModel();
		$input = array (
			'agenda_number' => Str::random(6),
			'received_at' => Carbon::createFromDate(2015, 2, 6),
			'document_number' => Str::random(6),
			'is_copy' => 1,
			'document_date' => Carbon::createFromDate(2015, 2, 5),
			'received_from' => 'Calon Bupati',
			'about' => 'Sosialisasi Kesehatan',
			'clasification' => 'BIA',
			'urgency' => 'SSG',
			'stage' => '__NEW',
			'target_date' => Carbon::createFromDate(2015, 2, 8),
			'is_product' => 1,
			'clients' => '[{"id":"0","name":"Bank BCA","_id":"0"}]'
		);
		$doc->fill($input);
		$doc->save();

		$emp = $first;
		$employee = $emp->name . " (" . $emp->jobtitle->name . ")";
		$actnew = new DocumentActionModel();
		$input = array(
			'doc_id' => $doc->id,
			'employee_id' => $emp->id,
			'employee_name' => $emp->name,
			'stage' => $doc->stage,
			'action_type' => 'DOC',
			'notes' => '<b>' . $employee . '</b>' . ' membuat surat baru',
		);
		$actnew->fill($input);
		$actnew->save();

		// SEED FOR TESTING PERFORMANCE
		// DEV ENV ONLY
		if (App::environment('dev'))
			$this->seedDummy(10, 25);
		
		if (App::environment('testing'))
			$this->seedDummy(10, 25);
	}

	public function seedDummy($closed_rows, $open_rows)
	{
		DB::transaction(function () use ($closed_rows, $open_rows) {
			// get initial data
			$clients = ClientModel::lists('name', 'id');
			$clients[] = 0;
			$departments = DepartmentModel::lists('id');
			$employees = [];
			foreach($departments as $d) {
				$employees[$d] = EmployeeModel::where('department', '=', $d)->lists('id');
			}
			//
			$f = Faker\Factory::create();
			for($i = 0; $i < $closed_rows; $i++) {
				$doc = new DocumentModel();
				$received_at = $f->dateTimeBetween('-1 year', 'now')->setTime(0, 0, 0);
				$received_from_id = $f->randomElement($clients);
				$assigned_dep = $f->randomElement($departments);
				$random_id = $f->randomElement(array_keys($clients));
				$cl = [[
					'id' => $random_id,
					'name' => $clients[$random_id]
				]];
				$input = array (
					'agenda_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
					'received_at' => $received_at,
					'document_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
					'is_copy' => $f->randomElement([0, 1]),
					'document_date' => $f->dateTimeBetween(Carbon::instance($received_at)->copy()->subDays(14), $received_at)->setTime(0, 0, 0),
					'received_from_id' => $received_from_id,
					'received_from' => ($received_from_id == 0 ? $f->name : null),
					'about' => $f->sentence(11),
					'enclosure' => $f->paragraph(4),
					'clasification' => $f->randomElement(['SRS','RHS','KFD','BIA']),
					'urgency' => $f->randomElement(['KLT','SSG','SGR','BIA']),
					'stage' => $f->randomElement(['PRFNS','PRFNS','PRFNS','PRFNS','PRDSC']),
					'assigned_dep' => $assigned_dep,
					'assigned_emp' => $f->randomElement($employees[$assigned_dep]),
					'target_date' => $f->dateTimeBetween($received_at, Carbon::instance($received_at)->copy()->addDays(14))->setTime(0, 0, 0),
					'is_product' => 1,
					'clients' => json_encode($cl),
					'is_done' => 1
				);
				$doc->fill($input);
				$doc->save();

				$actnew = new DocumentActionModel();
				$input = array(
					'doc_id' => $doc->id,
					'employee_id' => $f->randomElement($employees[$assigned_dep]),
					'employee_name' => 'System',
					'stage' => '__NEW',
					'action_type' => 'DOC',
					'notes' => '<i>System</i> membuat surat baru',
				);
				$actnew->fill($input);
				$actnew->save();

				$dispositions = [];
				$num = $f->randomElement([0, 1, 2, 3]);
				for($j = 0; $j < $num; $j++) {
					do {
						$dep = $f->randomElement($departments);
					} while (isset($dispositions[$dep]));

					$dispositions[$dep] = array('notes' => $f->sentence(11));
				}

				if ($num)
					$doc->docDisposition()->sync($dispositions);
			}
			for($i = 0; $i < $open_rows; $i++) {
				$doc = new DocumentModel();
				$received_at = $f->dateTimeBetween('-14 days', 'now')->setTime(0, 0, 0);
				$received_from_id = $f->randomElement($clients);
				$assigned_dep = $f->randomElement($departments);
				$random_id = $f->randomElement(array_keys($clients));
				$cl = [[
					'id' => $random_id,
					'name' => $clients[$random_id]
				]];
				$input = array (
					'agenda_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
					'received_at' => $received_at,
					'document_number' => $f->randomNumber(4, true) . '/' . $f->randomNumber(6, true),
					'is_copy' => $f->randomElement([0, 1]),
					'document_date' => $f->dateTimeBetween(Carbon::instance($received_at)->copy()->subDays(14), $received_at)->setTime(0, 0, 0),
					'received_from_id' => $received_from_id,
					'received_from' => ($received_from_id == 0 ? $f->name : null),
					'about' => $f->sentence(11),
					'enclosure' => $f->paragraph(4),
					'clasification' => $f->randomElement(['SRS','RHS','KFD','BIA']),
					'urgency' => $f->randomElement(['KLT','SSG','SGR','BIA']),
					'stage' => $f->randomElement(['__NEW','PRAPP','PRASG','PRDLV','PRDAP','PRPRO','PRANS']),
					'assigned_dep' => $assigned_dep,
					'assigned_emp' => $f->randomElement($employees[$assigned_dep]),
					'target_date' => $f->dateTimeBetween('now', '+14 days')->setTime(0, 0, 0),
					'is_product' => 1,
					'clients' => json_encode($cl)
				);
				$doc->fill($input);
				$doc->save();

				$actnew = new DocumentActionModel();
				$input = array(
					'doc_id' => $doc->id,
					'employee_id' => $f->randomElement($employees[$assigned_dep]),
					'employee_name' => 'System',
					'stage' => '__NEW',
					'action_type' => 'DOC',
					'notes' => '<i>System</i> membuat surat baru',
				);
				$actnew->fill($input);
				$actnew->save();

				$dispositions = [];
				$num = $f->randomElement([2, 3]);
				for($j = 0; $j < $num; $j++) {
					do {
						$dep = $f->randomElement($departments);
					} while (isset($dispositions[$dep]));

					$dispositions[$dep] = array('notes' => $f->sentence(11));
				}

				if ($num)
					$doc->docDisposition()->sync($dispositions);
			}
		});
	}
}