<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Eloquent::unguard();
		DB::table('password_reminders')->delete();

		DB::table('doccarboncopy_employee')->delete();
		DB::table('docdisposition_department')->delete();
		DB::table('document_action_file')->delete();
		DB::table('document_action')->delete();
		DB::table('document_directive')->delete();
		DB::table('document_addressedto')->delete();
		DB::table('documents')->delete();

		DB::table('employees')->delete();
		DB::table('departments')->delete();
		DB::table('job_title')->delete();
		DB::table('job_level')->delete();
		DB::table('clients')->delete();
		
		$this->call('DepartmentTableSeeder');
		$this->call('JobLevelTableSeeder');
		$this->call('JobTitleTableSeeder');
		$this->call('EmployeeTableSeeder');
		$this->call('ClientTableSeeder');
		$this->call('DocumentTableSeeder');
	}

}
