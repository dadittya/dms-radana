<?php

class JobLevelTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{	
		$level = new JobLevelModel();
		$level->id = 1;
		$level->name = "Kepala Kantor";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 2;
		$level->name = "Sekretaris";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 3;
		$level->name = "Kepala SubBagian/Seksi";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 4;
		$level->name = "Pelaksana";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 5;
		$level->name = "Engineer";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 6;
		$level->name = "Auditor";
		$level->save();
	}

}