<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployee extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string("name", 255);
			$table->string("email", 255)->unique();
			$table->string("password", 127);
			$table->string('nip', 255)->nullable();
			$table->integer('department')->unsigned()->nullable();
			$table->integer("job_title")->unsigned()->index();
			$table->boolean("active")->default("0");
			$table->rememberToken();
			$table->timestamps();

			$table->foreign('department')
				->references('id')
				->on('departments');

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
