<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocumentActionNotesType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_action', function(Blueprint $table)
		{
			$table->dropColumn('notes');
		});
		Schema::table('document_action', function(Blueprint $table)
		{
			$table->mediumText('notes')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_action', function(Blueprint $table)
		{
			$table->dropColumn('notes');
		});
		Schema::table('document_action', function(Blueprint $table)
		{
			$table->text('notes')->nullable();
		});
	}

}
