<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeAtDocActionFile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_action_file', function(Blueprint $table)
		{
			$table->string('type', 3)->index()->nullable()->default('OTR');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_action_file', function(Blueprint $table)
		{
			$table->dropColumn('type');
		});
	}

}
