<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableFieldClient extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `clients` MODIFY `email` VARCHAR(255) NULL;');
		DB::statement('ALTER TABLE `clients` MODIFY `phone` VARCHAR(255) NULL;');
		DB::statement('ALTER TABLE `clients` MODIFY `address` VARCHAR(255) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `clients` MODIFY `email` VARCHAR(255);');
		DB::statement('ALTER TABLE `clients` MODIFY `phone` VARCHAR(255);');
		DB::statement('ALTER TABLE `clients` MODIFY `address` VARCHAR(255);');
	}

}
