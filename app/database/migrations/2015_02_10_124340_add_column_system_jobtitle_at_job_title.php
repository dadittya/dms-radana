<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSystemJobtitleAtJobTitle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_title', function(Blueprint $table) 
		{
			$table->boolean('system_jobtitle')->default(0)->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_title', function(Blueprint $table) 
		{
			$table->dropColumn('system_jobtitle');
		});
	}

}
