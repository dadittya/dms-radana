<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJobTitle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_title', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->timestamps();

			$table->engine = "InnoDB";
		});

		Schema::table('employees', function(Blueprint $table)
		{
			$table->foreign('job_title')
				->references('id')
				->on('job_title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employees', function(Blueprint $table)
		{
			$table->dropForeign('employees_job_title_foreign');
		});

		Schema::drop('job_title');
	}

}
