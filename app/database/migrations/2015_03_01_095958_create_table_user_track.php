<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserTrack extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_track', function(Blueprint $table) 
		{
			$table->increments('id');
			$table->integer('doc_id')->unsigned()->index();
			$table->integer('emp_id')->unsigned()->index();
			$table->timestamps();

			$table->foreign('doc_id')
				->references('id')
				->on('documents');

			$table->foreign('emp_id')
				->references('id')
				->on('employees');

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_track');
	}

}
