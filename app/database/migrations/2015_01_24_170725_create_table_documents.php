<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocuments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('agenda_number', 255)->index();
			$table->datetime('received_at');
			$table->string('kk_number', 255)->index();
			$table->string('document_number', 255)->index();
			$table->boolean('is_copy');
			$table->datetime('document_date');
			$table->string('received_from', 255)->nullable();
			$table->string('about', 255);
			$table->text('enclosure')->nullable();
			$table->string('clasification', 3)->index();
			$table->string('urgency', 3)->index();
			$table->integer('assigned_emp')->unsigned()->nullable();
			$table->integer('assigned_dep')->unsigned()->nullable();
			$table->string('stage', 5)->index();
			$table->integer('referenced_doc')->unsigned()->nullable();
			$table->datetime('target_date')->nullable();
			$table->boolean('is_product');
			$table->boolean('is_done')->default(0);
			$table->timestamps();

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}
}