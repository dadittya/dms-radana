<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocdispositionDepartment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('docdisposition_department', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('doc_id')->unsigned()->index();
			$table->integer('dept_id')->unsigned()->index();
			$table->string('notes', 255)->nullable();
			$table->timestamps();

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('docdisposition_department');
	}

}
