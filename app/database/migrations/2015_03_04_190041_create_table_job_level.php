<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJobLevel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_level', function(Blueprint $table)
		{
			$table->increments('id')->index();
			$table->string('name', 64);
			$table->timestamps();

			$table->engine = "InnoDB";
		});

		$level = new JobLevelModel();
		$level->id = 1;
		$level->name = "Kepala Kantor";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 2;
		$level->name = "Sekretaris";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 3;
		$level->name = "Kepala SubBagian/Seksi";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 4;
		$level->name = "Pelaksana";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 5;
		$level->name = "Engineer";
		$level->save();

		$level = new JobLevelModel();
		$level->id = 6;
		$level->name = "Auditor";
		$level->save();
	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_level');
	}

}
