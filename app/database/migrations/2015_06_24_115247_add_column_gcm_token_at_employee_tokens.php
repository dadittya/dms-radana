<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGcmTokenAtEmployeeTokens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_tokens', function (Blueprint $table) 
		{
			$table->string('gcm_token', 255)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_tokens', function (Blueprint $table) 
		{
			$table->dropColumn('gcm_token');
		});
	}

}
