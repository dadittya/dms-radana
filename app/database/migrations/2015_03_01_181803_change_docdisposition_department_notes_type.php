<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocdispositionDepartmentNotesType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('docdisposition_department', function(Blueprint $table)
		{
			$table->dropColumn('notes');
		});
		Schema::table('docdisposition_department', function(Blueprint $table)
		{
			$table->mediumText('notes')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('docdisposition_department', function(Blueprint $table)
		{
			$table->dropColumn('notes');
		});
		Schema::table('docdisposition_department', function(Blueprint $table)
		{
			$table->string('notes', 255)->nullable();
		});
	}

}
