<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJumbotronTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jumbotrons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('link');
            $table->string('image', 127);
            $table->string('mimetype', 127);
            $table->string('state', 3)->index();
            $table->integer('show_order');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jumbotrons');
	}

}
