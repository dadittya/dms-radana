<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnClientAtDocuments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->integer('received_from_id')->unsigned()->nullable();
			$table->string('addressed_to', 255)->nullable();
			$table->integer('addressed_to_id')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropColumn(
				'received_from_id',
				'addressed_to',
				'addressed_to_id'
			);
		});
	}

}
