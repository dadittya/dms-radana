<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentAction extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_action', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('doc_id')->unsigned()->index();
			$table->integer('employee_id')->unsigned()->index();
			$table->string('employee_name', 255);
			$table->string('employee_depname', 255)->nullable();
			$table->text('notes');
			$table->string('stage', 5)->index();
			$table->string('action_type', 3)->index();
			$table->timestamps();

			$table->foreign('doc_id')
				->references('id')
				->on('documents');

			$table->foreign('employee_id')
				->references('id')
				->on('employees');

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_action');
	}

}
