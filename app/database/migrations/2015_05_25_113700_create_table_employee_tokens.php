<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployeeTokens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_tokens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employee_id')->unsigned();
			$table->string('client_id', 255);
			$table->string('client_name', 255);
			$table->string('token', 255)->unique();
			$table->timestamps();

			$table->engine = "InnoDB";
		});

		Schema::table('employee_tokens', function(Blueprint $table)
		{
			$table->foreign('employee_id')
				->references('id')
				->on('employees');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_tokens', function(Blueprint $table)
		{
			$table->dropForeign('employee_tokens_employee_id_foreign');
		});

		Schema::drop('employee_tokens');
	}

}
