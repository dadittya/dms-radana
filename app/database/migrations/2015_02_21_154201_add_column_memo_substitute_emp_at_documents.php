<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMemoSubstituteEmpAtDocuments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->string('official_memo', 255)->index()->nullable();
			$table->datetime('memo_date')->nullable();
			$table->integer('substitute_emp')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropColumn(
				'official_memo',
				'memo_date',
				'substitute_emp'
			);
		});
	}

}
