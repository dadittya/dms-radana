<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLevelAtJobtitle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('job_title', function (Blueprint $table)
		{
			$table->integer('level')->unsigned()->default(4);

			$table->foreign('level')
				->references('id')
				->on('job_level');
		});

		//fill field level
		DB::table('job_title')->where('id', 1)->update(array('level' => 1));
		DB::table('job_title')->where('id', 2)->update(array('level' => 2));
		DB::table('job_title')->where('id', 3)->update(array('level' => 3));
		DB::table('job_title')->where('id', 4)->update(array('level' => 4));
		DB::table('job_title')->where('id', 5)->update(array('level' => 3));
		DB::table('job_title')->where('id', 6)->update(array('level' => 5));
		DB::table('job_title')->where('id', 7)->update(array('level' => 4));
		DB::table('job_title')->where('id', 8)->update(array('level' => 4));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('job_title', function (Blueprint $table)
		{
			$table->dropForeign('job_title_level_foreign');
			$table->dropColumn('level');
		});
	}

}
