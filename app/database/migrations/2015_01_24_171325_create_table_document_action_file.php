<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentActionFile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_action_file', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('docaction_id')->unsigned()->index();
			$table->string('originalname', 255);
			$table->string('filename', 255);
			$table->string('extension', 255);
			$table->integer('size');
			$table->timestamps();

			$table->foreign('docaction_id')
				->references('id')
				->on('document_action');

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_action_file');
	}

}
