<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocumentsAboutType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropColumn('about');
		});
		Schema::table('documents', function(Blueprint $table)
		{
			$table->mediumText('about')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropColumn('about');
		});
		Schema::table('documents', function(Blueprint $table)
		{
			$table->string('about', 255)->nullable();
		});
	}

}
