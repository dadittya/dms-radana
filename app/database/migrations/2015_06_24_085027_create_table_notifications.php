<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function (Blueprint $table) 
		{
			$table->increments('id');
			$table->integer('employee_id')->unsigned()->index();
			$table->integer('doc_action_id')->unsigned()->index();
			$table->boolean('read')->nullable()->default(0);
			$table->timestamps();

			$table->foreign('employee_id')
				->references('id')
				->on('employees');

			$table->foreign('doc_action_id')
				->references('id')
				->on('document_action');

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
