<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentDirective extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_directive', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('doc_id')->unsigned()->index();
			$table->string('directive', 255);
			$table->string('notes', 255)->nullable;
			$table->timestamps();

			$table->foreign('doc_id')
				->references('id')
				->on('documents');

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_directive');
	}

}
