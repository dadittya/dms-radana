<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2HoursDatetime extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('UPDATE clients SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE departments SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE docdisposition_department SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE document_action SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE document_action_file SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE document_addressedto SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE document_directive SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE documents SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE employees SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE job_level SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE job_title SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL 2 HOUR)');
		DB::statement('UPDATE password_reminders SET created_at = DATE_ADD(created_at, INTERVAL 2 HOUR)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('UPDATE clients SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE departments SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE docdisposition_department SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE document_action SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE document_action_file SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE document_addressedto SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE document_directive SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE documents SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE employees SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE job_level SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE job_title SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR), updated_at = DATE_ADD(updated_at	, INTERVAL -2 HOUR)');
		DB::statement('UPDATE password_reminders SET created_at = DATE_ADD(created_at, INTERVAL -2 HOUR)');
	}

}
