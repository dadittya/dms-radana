<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentAddressedto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_addressedto', function(Blueprint $table)
		{
			$table->increments('id')->index();
			$table->integer('doc_id')->unsigned()->index();
			$table->integer('client_id')->unsigned()->index();
			$table->string('name', 255);
			$table->string('email', 255)->nullable();
			$table->timestamps();

			$table->engine = "InnoDB";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_addressedto');
	}

}
