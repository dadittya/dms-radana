<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMarkDownAtDocAction extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_action', function(Blueprint $table) 
		{
			$table->text('mark_down');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_action', function(Blueprint $table) 
		{
			$table->dropColumn('mark_down');
		});
	}

}
