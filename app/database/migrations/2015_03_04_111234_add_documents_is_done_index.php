<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentsIsDoneIndex extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->index('is_done');
			$table->index('is_product');
			$table->index('assigned_emp');
			$table->index('substitute_emp');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropIndex('documents_is_done_index');
			$table->dropIndex('documents_is_product_index');
			$table->dropIndex('documents_assigned_emp_index');
			$table->dropIndex('documents_substitute_emp_index');
		});
	}
}