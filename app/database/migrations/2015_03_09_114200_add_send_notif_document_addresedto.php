<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendNotifDocumentAddresedto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_addressedto', function(Blueprint $table)
		{
			$table->boolean('notify_email')->default('0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_addressedto', function(Blueprint $table)
		{
			$table->dropColumn('notify_email');
		});
	}
}