(function($){
 $.fn.time_idr = function(e, is_ajax, days, months) {
        var d = this;
        var i = 0;
        var c = 0;

        function h(k, j) {
            var l = "" + k;
            while (l.length < j) {
                l = "0" + l
            }
            return l
        }

        function g() {
            var j = new Date(c * 1000 + (new Date().getTime() - i));
            d.children(".date").text(days[j.getDay()] + ", " + j.getDate() + " " + months[j.getMonth()] + " " + (j.getYear() + 1900));
            d.children(".time").text(h(j.getHours(), 2) + ":" + h(j.getMinutes(), 2) + ":" + h(j.getSeconds(), 2))
        }
        var f;
		var startTime = function(j){
			c = j;
			i = new Date().getTime();
			g();
			f = window.setInterval(function() {
				g()
			}, 1000)
		}
		if(is_ajax){
			$.ajax({
				url: e,
				success: function(j) {
					startTime(j);
				}
			});
		}else{
			startTime(e);
		}
    };
})(jQuery);