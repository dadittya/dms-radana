var dept = $('#dept').val();
var result_emp, result_subs_emp, result_disp_emp, result_doc;

function show_notes(id) {
	if ($('#department_' + id).prop('checked'))
		$('#notes_' + id).removeClass('hidden');
	else 
		$('#notes_' + id).addClass('hidden');
}

function choose_emp(i) {
	if (result_emp)
		var choose = result_emp[i];
	else
		var choose = emp[dept][i];
		
	$('#inputEmployee').val(choose.name);
	$('#sentEmployee').val(choose.id);
	$('#employee').modal('hide');
}

function choose_subs_emp(i) {
	var choose = result_subs_emp[i];
		
	$('#inputSubsEmployee').val(choose.name);
	$('#sentSubsEmployee').val(choose.id);
	$('#subsEmployee').modal('hide');
}

function render_carboncopy() {
	for (idx in carboncopy) {
		carboncopy[idx]['_id'] = idx;
	}

	$('#sentDispEmployee').val(JSON.stringify(carboncopy));
	var tmpl = $('#tmplCarbonCopyTable').html();
	Mustache.parse(tmpl);
	var html = Mustache.render(tmpl, { 'data': carboncopy });
	$('#carbonCopyTable').html(html);
	check_carboncopy();
}

function check_carboncopy() {
	for (var i in carboncopy) {
		if (user_dept && carboncopy[i].dept_id != user_dept)
			$('.btn_delete_cc_' + i).addClass('hidden');
	}
}

function choose_disp_emp(i) {
	if (result_disp_emp)
		var choose = result_disp_emp[i];
	else
		var choose = emp[user_dept][i];

	var i = choose.id;
	var n = choose.name;
	var p = choose.dept_id;
	var d = choose.department;

	var input = {
		'id' : i,
		'name' : n,
		'dept_id' : p,
		'department' : d
	};

	for (var y in carboncopy)
		if (i == carboncopy[y].id) {
			input = null;
			break;
		}

	if (input)
		carboncopy.push(input);
	render_carboncopy();
	$('#dispEmployee').modal('hide');
}

function delete_carboncopy(i) 
{
	carboncopy.splice(i, 1);
	render_carboncopy();
}

function choose_doc(i){
	if (result_doc)
		var choose = result_doc[i];
	else
		var choose = doc[i];

	$('#inputDocument').val(choose.document_number);
	$('#sentDocument').val(choose.id);
	$('#reference').modal('hide');
}

//addressed to
function render_addressedto()
{
	for (idx in clients) {
		clients[idx]['_id'] = idx;
	}

	$('#sentClient').val(JSON.stringify(clients));
	var tmpl = $('#tmplAddressedToTable').html();
	Mustache.parse(tmpl);
	var html = Mustache.render(tmpl, { 'data': clients });
	$('#addressedToTable').html(html);

	$("#inputClient").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$("#AddClientButton").click();
		}
	});
}

function add_client()
{
	var d, found, e;
	var id = parseInt($('#selectClient').val(), 10);

	if (id == 0) {
		var n = $('#inputClient').val();
		d = '(Lain-Lain)';
		e = false;
	} else {
		var n = $('#selectClient option:selected').html();
		e = true;
	}

	for (var i in clients) {
		if (clients[i].name == n) {
			found = clients[i];
			break;
		}
	}

	if (!found && n) {
		var input = {
			'id' : id,
			'name' : n,
			'label' : d,
			'notify_email': e
		};
		clients.push(input);
	}

	render_addressedto();
	$('#inputClient').val('');
	$('selectClient').focus();
}

function set_client_notify_email(i)
{
	clients[i].notify_email = !clients[i].notify_email;
	render_addressedto();
}

function delete_client(i)
{
	clients.splice(i, 1);
	render_addressedto();
}

$(function (){
	function check_dept() {
		var key = Object.keys(deptlist);
		for (var i = 0 in key)
			show_notes(key[i]);
	}

	function render_employee(filter) {
		for (idx in filter) {
			filter[idx]['_id'] = idx;
		}

		var tmpl = $('#tmplEmployeeTable').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data': filter });
		$('#employeeTable').html(html);
	}

	function render_subs_employee(filter) {
		for (idx in filter) {
			filter[idx]['_id'] = idx;
		}

		var tmpl = $('#tmplSubsEmployeeTable').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data': filter });
		$('#subsEmployeeTable').html(html);
	}

	function render_disp_employee(filter) {
		for (idx in filter) {
			filter[idx]['_id'] = idx;
		}

		var tmpl = $('#tmplDispEmployeeTable').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data': filter });
		$('#dispEmployeeTable').html(html);
	}

	function render_doc(filter){
		for (idx in filter) {
			filter[idx]['_id'] = idx;

			if(filter[idx]['received_from'] == '')
				filter[idx]['received_from'] = '-';
			if(filter[idx]['agenda_number'] == '')
				filter[idx]['agenda_number'] = '-';
			if(filter[idx]['document_number'] == '')
				filter[idx]['document_number'] = '-';
			if(filter[idx]['about'] == '')
				filter[idx]['about'] = '-';

		}

		var tmpl = $('#tmplDocumentTable').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { 'data': filter });
		$('#documentTable').html(html);
	}

	function filter_employee() {
		var q = $('#inputSearch').val();
		var asg_dep = $('#dept').val();
		$('#emp_modal').removeClass('hidden');

		$.ajax ({
			url: '/employee/filter' + '?q=' + q + '&dept=' + asg_dep,
			type: 'GET',
			success: function(result){
				$('#emp_modal').addClass('hidden');
				result_emp = result.emp;
				render_employee(result_emp);
				$('#emp_page').html(result.employees);
			}
		});
	}

	function filter_subs_employee() {
		var q = $('#inputSearchSubsEmp').val();
		$('#subs_emp_modal').removeClass('hidden');

		$.ajax ({
			url: '/employee/filter' + '?q=' + q,
			type: 'GET',
			success: function(result){
				$('#subs_emp_modal').addClass('hidden');
				result_subs_emp = result.emp;
				render_subs_employee(result_subs_emp);
				$('#subs_emp_page').html(result.employees);
			}
		});
	}

	function filter_disp_employee() {
		var q = $('#inputSearchDispEmp').val();
		$('#disp_emp_modal').removeClass('hidden');
		if (!user_dept)
			user_dept = '';

		$.ajax ({
			url: '/employee/filter' + '?q=' + q + '&dept=' + user_dept,
			type: 'GET',
			success: function(result){
				$('#disp_emp_modal').addClass('hidden');
				result_disp_emp = result.emp;
				render_disp_employee(result_disp_emp);
				$('#disp_emp_page').html(result.employees);
			}
		});
	}

	function filter_doc() {
		var q = $('#inputSearchDoc').val();
		$('#doc_modal').removeClass('hidden');

		$.ajax ({
			url: '/document/filter' + '?q=' + q,
			type: 'GET',
			success: function(result){
				$('#doc_modal').addClass('hidden');
				result_doc = result.doc;
				render_doc(result_doc);
				page_url = '/document/filter';
				$('#doc_page').html(result.documents);
			}
		});
	}

	function input_old() {
		for (var i = 0 in doc) {
			if (doc[i].id == $('#sentDocument').val()) {
				$('#inputDocument').val(doc[i].document_number);
				break;
			}
		}

		for (var i = 0 in emp[dept]) {
			if (emp[dept][i].id == $('#sentEmployee').val()) {
				$('#inputEmployee').val(emp[dept][i].name);
				break;
			}
		}
	}

	function check_fromClient()
	{
		if ($('#fromClient_id').val() == 0)
			$('#fromClient').removeClass('hidden');
		else {
			$('#fromClient').val('');
			$('#fromClient').addClass('hidden');
		}
	}

	function check_toClient()
	{
		if ($('#selectClient').val() == 0) {
			$('#inputClient').removeClass('hidden');
		} else {
			$('#inputClient').val('');
			$('#inputClient').addClass('hidden');
		}
	}

	$('#dept').on('change', function() {
		$('#inputSearch').val('');
		filter_employee();
		$('#inputEmployee').val('');
		$('#sentEmployee').val('');
	});

	$('#search_emp').on('click', filter_employee);

	$('#search_subs_emp').on('click', filter_subs_employee);

	$('#search_disp_emp').on('click', filter_disp_employee);

	$('#search_doc').on('click', filter_doc);

	//pagination search document
	$(document).on('click', '.document .pagination a', function (e) {
        var page = $(this).attr('href').split('page=')[1];
        var q = $('#inputSearchDoc').val();
        $('#doc_modal').removeClass('hidden');

        $.ajax ({
        	url: page_url + '?page=' + page + '&q=' + q,
        	type: 'GET',
        	dataType: 'json',
        	success: function(data){
        		$('#doc_modal').addClass('hidden');
        		result_doc = data.doc;
        		render_doc(result_doc);
        		$('#doc_page').html(data.documents);
        	}
        });
        e.preventDefault();
    });

	//pagination search employee
	$(document).on('click', '.employee .pagination a', function (e) {
        var page = $(this).attr('href').split('page=')[1];
        var q = $('#inputSearch').val();
        var asg_dep = $('#dept').val();
        $('#emp_modal').removeClass('hidden');

        $.ajax ({
        	url: '/employee/filter' + '?page=' + page + '&q=' + q + '&dept=' + asg_dep,
        	type: 'GET',
        	dataType: 'json',
        	success: function(data){
        		$('#emp_modal').addClass('hidden');
        		result_emp = data.emp;
        		render_employee(result_emp);
        		$('#emp_page').html(data.employees);
        	}
        });
        e.preventDefault();
    });

    //pagination search substitute employee
	$(document).on('click', '.subs_employee .pagination a', function (e) {
        var page = $(this).attr('href').split('page=')[1];
        var q = $('#inputSearchSubsEmp').val();
        $('#subs_emp_modal').removeClass('hidden');

        $.ajax ({
        	url: '/employee/filter' + '?page=' + page + '&q=' + q,
        	type: 'GET',
        	dataType: 'json',
        	success: function(data){
        		$('#subs_emp_modal').addClass('hidden');
        		result_subs_emp = data.emp;
        		render_subs_employee(result_subs_emp);
        		$('#subs_emp_page').html(data.employees);
        	}
        });
        e.preventDefault();
    });

    //pagination search disposition employee
	$(document).on('click', '.disp_employee .pagination a', function (e) {
        var page = $(this).attr('href').split('page=')[1];
        var q = $('#inputSearchDispEmp').val();
        $('#disp_emp_modal').removeClass('hidden');

        $.ajax ({
        	url: '/employee/filter' + '?page=' + page + '&q=' + q,
        	type: 'GET',
        	dataType: 'json',
        	success: function(data){
        		$('#disp_emp_modal').addClass('hidden');
        		result_disp_emp = data.emp;
        		render_disp_employee(result_disp_emp);
        		$('#disp_emp_page').html(data.employees);
        	}
        });
        e.preventDefault();
    });

    $('#employee').on('shown.bs.modal', function () {
		$('#inputEmployee').val('');
		$('#sentEmployee').val('');
	});

	$('#reference').on('shown.bs.modal', function () {
		$('#inputDocument').val('');
		$('#sentDocument').val('');
	});

	$('#fromClient_id').on('change', check_fromClient);
	$('#selectClient').on('change', check_toClient);

	if (!user_dept) 
		filter_disp_employee();
	else
		render_disp_employee(emp[user_dept]);

	render_employee(emp[dept]);
	render_doc(doc);
	filter_subs_employee();
	render_addressedto();
	render_carboncopy();
	input_old();
	check_dept();
	check_fromClient();
	check_toClient();
});

// Aditional script for client list refresh
$(function() {
	$('#RefreshFromListButton').on('click', function() {
		$.ajax ({
			url: "/api/v1/client/list",
			type: 'GET',
			dataType: 'json',
			success: function(data){
				data.push({
					'id': 0,
					'name': 'Lain-Lain',
				});
				var selected_id = parseInt($("#fromClient_id").val(), 10);
				if (isNaN(selected_id)) {
					selected_id = 0;
				}
				var length = data.length;
				for (var i = 0; i < length; i++) {
					if (data[i].id == selected_id) {
						data[i].selected = true;
						break;
					}
				}
				var tmpl = $('#tmplListClient').html();
				Mustache.parse(tmpl);
				var html = Mustache.render(tmpl, { 'data': data });
				$("#fromClient_id").html(html);
			}
		});
	});
});