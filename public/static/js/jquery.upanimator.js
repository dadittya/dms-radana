(function(){
	$.fn.upAnimator = function(new_request){
		this.each(function(){
			var fthis = $(this);
			// cloning div, dua div berurutan.
			var div = fthis.children();
			var div2 = div.clone();
			var height = fthis.height();
			var resize = function(){
				if(div.position().top < div2.position().top)
				positioningDivAfter(div, div2, false);
				else
				positioningDivAfter(div2, div, false);
			}
			var positioningDivAfter = function(div, div2, is_ajax){
				// swap here
				var changePosition = function(){
					var heightElement = div.height();
					var posDiv2;
					if(heightElement<height){
						posDiv2 = div.position().top+height;
					} else {
						posDiv2 = div.position().top+heightElement;
					}
					div2.css({top: posDiv2});
				};
				if(is_ajax&&!in_ajax){
				in_ajax = true;
				changePosition();
				$.ajax({url:new_request+'/'+fthis.attr('data-id'),
					success: function(data){
						div2.get(0).innerHTML = data;
						in_ajax = false;
					}
				});
				}else{
					changePosition();
				}
			};
			resize();
			$(window).resize(function(){
				resize();
			});
			div.css({top: height});
			div2.insertAfter(div);
			var heightElement = div.height();
			var in_ajax;
			positioningDivAfter(div, div2, true);

			window.setInterval(function(){
				var top1 = div.position().top;
				div.css({top: top1-1});
				var top2 = div2.position().top;
				div2.css({top: top2-1});
				var change_pos = false;
				if(div2.position().top*-1 >= div2.height()){
					var div_1 = div;
					var div_2 = div2;
					var change_pos = true;
				}
				if(div.position().top*-1 >= div.height()){
					var div_1 = div2;
					var div_2 = div;
					var change_pos = true;
				}
				if(change_pos)
				positioningDivAfter(div_1, div_2, true);
			}, 60);
		});
	};
})(jQuery);
