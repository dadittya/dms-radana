//file upload
var dropbox = $("#dropbox");
var list = [], files = [], temp = [], count=0;
dropbox[0].addEventListener("dragover", dragover, false);
dropbox[0].addEventListener("drop", drop, false);
dropbox[0].addEventListener("dragend", dragend, false);
dropbox[0].addEventListener("dragleave", dragend, false);

function render_list(list)
{
	for (idx in  list)
		list[idx]['_id'] = idx;

	var tmpl = $('#tmplFileListTable').html();
	Mustache.parse(tmpl);
	var html = Mustache.render(tmpl, { 'data' : list });
	$('#fileListTable').html(html);

	check_state();
	$('#inputFile').val(JSON.stringify(temp));
}

function check_state() {
	upload = true;
	for (var i = 0; i < list.length; i++) {
		if (list[i].state == "SUKSES") {
			$("#state_" + i).addClass("text-success");
			$("#btn_remove_" + i).removeClass("hidden");
			$('#filetype_' + i).removeAttr('disabled');
		}
		else if (list[i].state == "GAGAL")
			$("#state_" + i).addClass("text-danger");
		else if (list[i].state == "DALAM PROSES") {
			$("#state_" + i).addClass("text-muted");
			upload = false;
		}

		if (list[i].type) {
			$("#filetype_" + i).val(list[i].type);
			temp[i].type = list[i].type;
		}
	}
}

function dragenter(e) {
	e.stopPropagation();
	e.preventDefault();
}

function dragover(e) {
	e.stopPropagation();
	e.preventDefault();
	dropbox.addClass('box-upload-hover');
}

function dragend(e) {
	dropbox.removeClass('box-upload-hover');
}

function drop(e) {
	e.stopPropagation();
	e.preventDefault();
	dropbox.removeClass('box-upload-hover');
	files = e.dataTransfer.files;

	addfile(files);
}

function addfile(files) {
	for (var i = 0; i < files.length; i++) {
		list.push({"file" : files[i], "file_name" : files[i].name, "state" : "MENUNGGU", "type" : ""});
	}	

	render_list(list);
	if (upload) 
		upload_file(list[count].file);
}

function upload_file(file) {
	if (list[count].state == "MENUNGGU") {
		var data = new FormData();
		var type = $('#filetype_' + count).val();
		data.append('file_name', file);
		data.append('type', type);

		$.ajax ({
			url: '../document/upload',
			type: 'POST',
			xhr: function() {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload)
					myXhr.upload.addEventListener("progress", progressHandler, false);
				return myXhr;
			},
			//beforeSend: beforeSendHandler,
			success: successHandler,
			error: errorHandler,
			data: data,
			cache: false,
			dataType: 'json',
			contentType: false,
			processData: false,
		});
	}
}

function errorHandler() {
	list[count].state = "GAGAL";
	render_list(list);
	$('#btn_submit').removeAttr('disabled');
	count++;
	if (count < list.length)
		upload_file(list[count].file);
}

function successHandler(data) {
	if (!data) 
		list[count].state = "GAGAL";
	else {
		temp.push(data);
		list[count].state = "SUKSES";
	}
	
	$('#btn_submit').removeAttr('disabled');
	render_list(list);
	count++;
	if (count < list.length)
		upload_file(list[count].file);
}

function progressHandler() {
	list[count].state = "DALAM PROSES";
	render_list(list);
	$('#btn_submit').attr('disabled', 'true');
}

function remove_file(i) {
	var data = new FormData();
	file_name = temp[i].temp;
	data.append('file_name', file_name);

	$.ajax ({
		url: '../document/remove',
		type: 'POST',
		data: data,
		contentType: false,
		processData: false,
		success: function(){
			list.splice(i, 1);
			temp.splice(i, 1);
			render_list(list);
			count --;
		}
	});
}

function update_type(i)
{
	list[i].type = $('#filetype_' + i).val();
	render_list(list);
}

$(function (){
	$('#one_file').on('change', function (e) {
		var files = e.target.files;
		addfile(files);
	});
});